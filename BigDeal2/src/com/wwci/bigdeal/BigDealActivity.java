package com.wwci.bigdeal;



import com.wwci.bigdeal.activity.TabView;
import com.wwci.bigdeal.db.DbAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.WindowManager;

public class BigDealActivity extends Activity {
	  private Intent i;
	  private DbAdapter mDbHelper;

	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.main);
	        mDbHelper = new DbAdapter(this);
	        mDbHelper.open();

	        boolean eula = checkEULA();
	        if (eula == false){
				AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BigDealActivity.this);
				  dialogBuilder.setTitle("End User License Agreement");
	            dialogBuilder.setMessage(R.string.eula);
	            dialogBuilder.setPositiveButton("Accept",  new
	          		  DialogInterface.OnClickListener() {
	                  public void onClick(DialogInterface dialog, int whichButton) {
	                      // User clicked Accept so set that they've agreed to the eula.
//	                  	    DB_Eula eulaDB = new DB_Eula(MyPOS.this);
	                	    mDbHelper.setEULA();
	                		i = new Intent(BigDealActivity.this, TabView.class);
	             	        startActivity(i);
	                  }
	              });
	            dialogBuilder.setNegativeButton("Decline", new
	          		  DialogInterface.OnClickListener() {
	                  public void onClick(DialogInterface dialog, int whichButton) {
	                      finish();  //goodbye!
	                  }
	              });
	            dialogBuilder.setCancelable(false);
	           dialogBuilder.create().show();	
	        }else{
	        	i = new Intent(this, TabView.class);
	 	        startActivity(i);
	        }        
	    }
	    
		
		@Override
		public boolean onKeyDown(int keyCode, KeyEvent event)  {  
	        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME ){
			  AlertDialog.Builder builder = new AlertDialog.Builder(BigDealActivity.this);		    	        
		    	  builder.setMessage("Quit Big Deal?");
		    	  builder.setCancelable(false);		    	        
		    	  builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		    	       	public void onClick(DialogInterface dialog, int id) {
		    	       		System.exit(0);
		    	       	}
		    	  });
		    	        
		    	  builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
		    	       	public void onClick(DialogInterface dialog, int id) {
		    	       		i = new Intent(BigDealActivity.this, TabView.class);
		    	 	        startActivity(i);
		    	       	}
		    	  });   
		    	        
		    	  AlertDialog alert = builder.create();
		    	  alert.show();	
		}

	        return super.onKeyDown(keyCode, event); 

		}
		
//		@Override
//		 public void onAttachedToWindow()
//
//		 { // TODO Auto-generated method stub
//
//		    this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
//
//		    super.onAttachedToWindow();
//
//		 }     

	    private boolean checkEULA(){
//	    	DB_Eula eulaDB = new DB_Eula(this);
	        boolean sEULA = mDbHelper.checkEULA();        
	    	return sEULA;
	    	
	    }
	    
		@Override
		protected void onDestroy() {
			mDbHelper.close();
			super.onDestroy();
		}
}