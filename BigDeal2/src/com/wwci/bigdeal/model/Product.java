package com.wwci.bigdeal.model;

public class Product {
	private String id;
	private String name;
	private String desc;
	private String unit;
	private String saleCost;
	private String salePrice;
	private String tag;
	private String picPath;
	private String status;
	
    private String alltags;
	


	public Product() {
		super();
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getSaleCost() {
		return saleCost;
	}
	public void setSaleCost(String saleCost) {
		this.saleCost = saleCost;
	}
	public String getSalePrice() {
		return salePrice;
	}
	public void setSalePrice(String salePrice) {
		this.salePrice = salePrice;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getPicPath() {
		return picPath;
	}
	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getAlltags() {
		return alltags;
	}

	public void setAlltags(String alltags) {
		this.alltags = alltags;
	}

	
}
