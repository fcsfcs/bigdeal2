package com.wwci.bigdeal.io.output;


import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.wwci.bigdeal.R;

import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.widget.TextView;
import au.com.bytecode.opencsv.CSVWriter;

public class PurchaseView extends ExportCSVView {
	public static final String TAG = SalesView.class.getSimpleName();
	ArrayList<String[]> datas = new ArrayList<String[]>();	   

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState,"BigDeal_Pro_Purchase", "csv");
		m_header = (TextView) findViewById(R.id.header);
		m_header.setText(R.string.management_export_purchase);
		m_title = (TextView) findViewById(R.id.title);
		m_title.setText(R.string.csv_file);

		super.m_exporter = new Runnable() {
			public void run() {
				
				try {
					CSVWriter csv = new CSVWriter(new FileWriter(Environment.getExternalStorageDirectory() + "/" + getFilename()));			
//					CSVWriter csv = new CSVWriter( new BufferedWriter(new OutputStreamWriter(new FileOutputStream(Environment.getExternalStorageDirectory() + "/" + getFilename()), "UTF-8")));
					setExportDataList();
								
		            
					for (int i = 0; i < datas.size(); i++) {
						csv.writeNext(datas.get(i));
				    } 
					
					csv.close(); 
				
					m_handler.post(new Runnable() {
						public void run() {
							Bundle data = new Bundle();
							data.putString(MESSAGE, getString(R.string.export_finished_msg) + "\n" + getFilename());
							data.putString(TITLE, getString(R.string.success));
							data.putBoolean(SUCCESS, true);

							Message msg = new Message();
							msg.setData(data);
							m_handler.handleMessage(msg);
						}
					});
				} catch (final IOException e) {
					e.printStackTrace();
					m_handler.post(new Runnable() {
						public void run() {
							Bundle data = new Bundle();
							data.putString(MESSAGE, e.getMessage());
							data.putString(TITLE, getString(R.string.error));
							data.putBoolean(SUCCESS, false);

							Message msg = new Message();
							msg.setData(data);
							m_handler.handleMessage(msg);
						}
					});
					return;
				}
			}
		};
	}
	
	private void setExportDataList(){
		ExportDao exportDao = new ExportDao(this);
		this.datas = exportDao.getPurchases();
	}
}
