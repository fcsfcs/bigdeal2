package com.wwci.bigdeal.io.output;

import java.io.File;
import java.io.FilenameFilter;

import com.wwci.bigdeal.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;



public abstract class ExportCSVView extends Activity{
	protected final static String MESSAGE = "msg";
	protected final static String HEADER = "header";
	protected final static String TITLE = "title";
	protected final static String SUCCESS = "success";

	protected final static int DIALOG_FINISHED = 1;
	protected final static int DIALOG_EXPORTING = 2;

	protected static String s_title = "";
	protected static String s_message = "";

	protected TextView m_header;
	protected TextView m_title;
	protected Button m_startBtn;
	protected EditText E_filename;
	protected String m_ext = "";
    protected String m_filename = "";
	public void onCreate(Bundle savedInstanceState, String filename, String ext) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.export_csv_db);
		m_ext = ext;
		m_filename = filename;
	}

	public void onResume() {
		super.onResume();
		initUI();
	}

	protected void initUI() {
		m_header = (TextView) findViewById(R.id.header);
		m_title = (TextView) findViewById(R.id.title);
		m_startBtn = (Button) findViewById(R.id.start);
		E_filename = (EditText) findViewById(R.id.filename);
		E_filename.setText(findFilename());

		m_startBtn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				export();
			}
		});
	}

	protected String findFilename() {
		File directory = Environment.getExternalStorageDirectory();
		String[] files = directory.list(m_filter);

		String name;
		int n = 0;
		while (true) {
			// search for the file
			name = buildName(n);
			boolean found = false;
			for (int i = 0; i < files.length; i++) {
				if (files[i].equalsIgnoreCase(name)) {
					found = true;
					break;
				}
			}
			if (!found) {
				break;
			}
			n++;
		}
		return name;
	}
    
	protected String getFilename() {
		return E_filename.getText().toString().trim();
	}

	protected String buildName(int num) {
		return m_filename + (num > 0 ? "." + String.valueOf(num) : "") + "." + m_ext;
	}

	protected void export() {
		showDialog(DIALOG_EXPORTING);
		new Thread(m_exporter).start();
	}

	@Override
	protected Dialog onCreateDialog(int which) {
		switch (which) {
			case DIALOG_EXPORTING:
				ProgressDialog dlg = new ProgressDialog(this);
				dlg.setTitle(R.string.exporting_title);
				dlg.setMessage(getString(R.string.exporting));
				return dlg;
			case DIALOG_FINISHED:
				return new AlertDialog.Builder(ExportCSVView.this).setTitle(s_title).setMessage(s_message).setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dismissDialog(DIALOG_FINISHED);
						finish();
					}
				}).create();
		}
		return null;
	}

	protected Runnable m_exporter = null;

	protected FilenameFilter m_filter = new FilenameFilter() {
		public boolean accept(File dir, String filename) {
			// get the extension
			int dot_loc = filename.lastIndexOf(".");
			if (dot_loc >= 0) {
				String ext = filename.substring(dot_loc + 1);
				return ext.equalsIgnoreCase(m_ext);
			}
			return false;
		}
	};

	protected Handler m_handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissDialog(DIALOG_EXPORTING);

			Bundle data = msg.getData();

			s_title = data.getString(TITLE);
			s_message = data.getString(MESSAGE);

			showDialog(DIALOG_FINISHED);
		}
	};
}
