package com.wwci.bigdeal.io.output;


import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.wwci.bigdeal.R;
import com.wwci.bigdeal.uitl.Constants;

import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.widget.TextView;
import au.com.bytecode.opencsv.CSVWriter;

public class ProductView extends ExportCSVView {
	public static final String TAG = ProductView.class.getSimpleName();
	ArrayList<String[]> datas = new ArrayList<String[]>();	   

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState,"BigDeal_Pro_Product", "csv");
		m_title = (TextView) findViewById(R.id.title);
		m_title.setText(R.string.csv_file);
		m_header = (TextView) findViewById(R.id.header);
		m_header.setText(R.string.management_export_product);
		final Bundle b = this.getIntent().getExtras();
		if (b.getString("type").equals("example")) m_header.setText(R.string.management_export_product_sample);
			
		super.m_exporter = new Runnable() {
			public void run() {
				
				try {
					CSVWriter csv = new CSVWriter(new FileWriter(Environment.getExternalStorageDirectory() + "/" + getFilename()));			
					
					if (b.getString("type").equals("example")){
						datas.add(new String[]{Constants.PLAINTEXT.get("vName"),Constants.PLAINTEXT.get("vDesc"),Constants.PLAINTEXT.get("vUnit"),Constants.PLAINTEXT.get("nSaleCost"),Constants.PLAINTEXT.get("nSalePrice"),Constants.PLAINTEXT.get("vTag"),Constants.PLAINTEXT.get("vPicPath"),Constants.PLAINTEXT.get("iStatus")});
						datas.add(new String[]{"Beanie Baby", "yellow, blue", "pcs","15.5","20.5","Toys","","1"});
						datas.add(new String[]{"6 Asst. Doggie Bags", "black", "pcs","25", "31.5","Toys","","1"});
						datas.add(new String[] {"Big Eyed Turtle Peek A Boo Pillow", "black, read","pcs", "19.9","34.5","Toys","","1"});
					}else{
						setExportDataList();
					}				
		            
					for (int i = 0; i < datas.size(); i++) {
						csv.writeNext(datas.get(i));
				    } 
					
					csv.close(); 
				
					m_handler.post(new Runnable() {
						public void run() {
							Bundle data = new Bundle();
							data.putString(MESSAGE, getString(R.string.export_finished_msg) + "\n" + getFilename());
							data.putString(TITLE, getString(R.string.success));
							data.putBoolean(SUCCESS, true);

							Message msg = new Message();
							msg.setData(data);
							m_handler.handleMessage(msg);
						}
					});
				} catch (final IOException e) {
					e.printStackTrace();
					m_handler.post(new Runnable() {
						public void run() {
							Bundle data = new Bundle();
							data.putString(MESSAGE, e.getMessage());
							data.putString(TITLE, getString(R.string.error));
							data.putBoolean(SUCCESS, false);

							Message msg = new Message();
							msg.setData(data);
							m_handler.handleMessage(msg);
						}
					});
					return;
				}
			}
		};
	}
	
	private void setExportDataList(){
		ExportDao exportDao = new ExportDao(this);
		this.datas = exportDao.getProductCSVDatas();
	}
}
