package com.wwci.bigdeal.io.output;

import java.util.ArrayList;

import com.wwci.bigdeal.db.DbAdapter;
import com.wwci.bigdeal.uitl.Constants;
import android.content.Context;
import android.database.Cursor;


public class ExportDao {

	private DbAdapter mDbHelper;

	public ExportDao(Context context){
		mDbHelper = new DbAdapter(context);

	}

	
	public ArrayList<String[]> getProductCSVDatas(){
		mDbHelper.open();
		StringBuffer sql = new StringBuffer();
		sql.append("select ")
		.append(DbAdapter.Mst_Product.PRODUCTNAME).append(",")
		.append(DbAdapter.Mst_Product.PRODUCTDESC).append(",")
		.append(DbAdapter.Mst_Product.PRODUCTUNIT).append(",")
	    .append(DbAdapter.Mst_Product.PRODUCTCOST).append(",")
		.append(DbAdapter.Mst_Product.PRODUCTPRICE).append(",")
		.append(DbAdapter.Mst_Product.PRODUCTTAG).append(",")
		.append(DbAdapter.Mst_Product.PRODUCTPIC).append(",")
		.append(DbAdapter.Mst_Product.STATUS)	
		.append(" from ")
		.append(DbAdapter.Mst_Product.TABLE_NAME)
		.append(" order by ").append(DbAdapter.Mst_Product.PRODUCTNAME);
		ArrayList<String[]> result = new ArrayList<String[]>();
		result.add(new String[]{Constants.PLAINTEXT.get("vName"),Constants.PLAINTEXT.get("vDesc"),Constants.PLAINTEXT.get("vUnit"),Constants.PLAINTEXT.get("nSaleCost"),Constants.PLAINTEXT.get("nSalePrice"),Constants.PLAINTEXT.get("vTag"),Constants.PLAINTEXT.get("vPicPath"),Constants.PLAINTEXT.get("iStatus")});
		Cursor cur = mDbHelper.excuteSQL(sql.toString());
		if(cur.moveToFirst()){
			do{
				result.add(new String[]{
					cur.getString(0),
					cur.getString(1),
					cur.getString(2),
					cur.getString(3),
					cur.getString(4),
					cur.getString(5),
					cur.getString(6),
					cur.getString(7)
				});
			}while(cur.moveToNext());
		}
		cur.close();
		mDbHelper.close();
		return result;
	}
	public ArrayList<String[]> getSales(){
		mDbHelper.open();
		StringBuffer sql = new StringBuffer();
		sql.append("select ")
		.append("s.").append(DbAdapter.orderHeader.PURCHASENO).append(",")
		.append("s.").append(DbAdapter.orderHeader.PURCHASEDATE).append(",")
		.append("s.").append(DbAdapter.orderHeader.DUEDATE).append(",")
		.append("s.").append(DbAdapter.orderHeader.CLOSEDATE).append(",")
		.append("i.").append(DbAdapter.orderProduct.RECEIVEDATE).append(",")		
		.append("s.").append(DbAdapter.orderHeader.AMOUNT).append(",")
		.append("s.").append(DbAdapter.orderHeader.PAID).append(",")
		.append("s.").append(DbAdapter.orderHeader.STATUS).append(",")
		.append("s.").append(DbAdapter.orderHeader.CLIENT).append(",")
		.append("s.").append(DbAdapter.orderHeader.CLIENTPHONE).append(",")
		.append("p.").append(DbAdapter.Mst_Product.PRODUCTNAME).append(",")
		.append("i.").append(DbAdapter.orderProduct.QTY).append(",")
	    .append("p.").append(DbAdapter.Mst_Product.PRODUCTUNIT).append(",")
		.append("i.").append(DbAdapter.orderProduct.PRICE).append(",")
		.append("i.").append(DbAdapter.orderProduct.QTY).append(",")
        .append("i.").append(DbAdapter.orderProduct.SUBTOTAL).append(",")
		.append("p.").append(DbAdapter.Mst_Product.PRODUCTDESC).append(",")
		.append("p.").append(DbAdapter.Mst_Product.PRODUCTTAG)
		.append(" from ")
		.append(DbAdapter.orderProduct.TABLE_NAME).append(" i, ")
		.append(DbAdapter.orderHeader.TABLE_NAME).append(" s, ")
		.append(DbAdapter.Mst_Product.TABLE_NAME).append(" p ")
		.append(" where ")
		.append(" i.").append(DbAdapter.orderProduct.PURCHASENO).append("=s.").append(DbAdapter.orderHeader.PURCHASENO).append(" and ")
		.append(" i.").append(DbAdapter.orderProduct.PRODUCTID).append("=p.").append(DbAdapter.Mst_Product.PRODUCTID).append(" ")
		.append(" order by ")
		.append(" i.").append(DbAdapter.orderProduct.PURCHASENO).append(",p.").append(DbAdapter.Mst_Product.PRODUCTNAME);
		
		ArrayList<String[]> result = new ArrayList<String[]>();
		result.add(Constants.SALES_HEADER);		           
		Cursor cur = mDbHelper.excuteSQL(sql.toString());
		if(cur.moveToFirst()){
			do{
				result.add(new String[]{
					cur.getString(0),
					cur.getString(1),
					cur.getString(2),
					cur.getString(3),
					cur.getString(4),
					cur.getString(5),
					cur.getString(6),
					cur.getString(7),
					cur.getString(8),
					cur.getString(9),
					cur.getString(10),
					cur.getString(11),
					cur.getString(12),
					cur.getString(13),
					cur.getString(14),
					cur.getString(15),
					cur.getString(16),
					cur.getString(17)
				});
			}while(cur.moveToNext());
		}
		cur.close();
		mDbHelper.close();
		return result;
	}
	
	public ArrayList<String[]> getPurchases(){
		mDbHelper.open();
		StringBuffer sql = new StringBuffer();
		sql.append("select ")
		.append("s.").append(DbAdapter.purchaseHeader.PURCHASENO).append(",")
		.append("s.").append(DbAdapter.purchaseHeader.PURCHASEDATE).append(",")
		.append("s.").append(DbAdapter.purchaseHeader.DUEDATE).append(",")
		.append("s.").append(DbAdapter.purchaseHeader.CLOSEDATE).append(",")
		.append("i.").append(DbAdapter.purchaseProduct.RECEIVEDATE).append(",")		
		.append("s.").append(DbAdapter.purchaseHeader.AMOUNT).append(",")
		.append("s.").append(DbAdapter.purchaseHeader.PAID).append(",")
		.append("s.").append(DbAdapter.purchaseHeader.STATUS).append(",")
		.append("s.").append(DbAdapter.purchaseHeader.CLIENT).append(",")
		.append("s.").append(DbAdapter.purchaseHeader.CLIENTPHONE).append(",")
		.append("p.").append(DbAdapter.Mst_Product.PRODUCTNAME).append(",")
		.append("i.").append(DbAdapter.purchaseProduct.QTY).append(",")
	    .append("p.").append(DbAdapter.Mst_Product.PRODUCTUNIT).append(",")
		.append("i.").append(DbAdapter.purchaseProduct.PRICE).append(",")
		.append("i.").append(DbAdapter.purchaseProduct.QTY).append(",")
        .append("i.").append(DbAdapter.purchaseProduct.SUBTOTAL).append(",")
		.append("p.").append(DbAdapter.Mst_Product.PRODUCTDESC).append(",")
		.append("p.").append(DbAdapter.Mst_Product.PRODUCTTAG)
		.append(" from ")
		.append(DbAdapter.purchaseProduct.TABLE_NAME).append(" i, ")
		.append(DbAdapter.purchaseHeader.TABLE_NAME).append(" s, ")
		.append(DbAdapter.Mst_Product.TABLE_NAME).append(" p ")
		.append(" where ")
		.append(" i.").append(DbAdapter.purchaseProduct.PURCHASENO).append("=s.").append(DbAdapter.purchaseHeader.PURCHASENO).append(" and ")
		.append(" i.").append(DbAdapter.purchaseProduct.PRODUCTID).append("=p.").append(DbAdapter.Mst_Product.PRODUCTID).append(" ")
		.append(" order by ")
		.append(" i.").append(DbAdapter.purchaseProduct.PURCHASENO).append(",p.").append(DbAdapter.Mst_Product.PRODUCTNAME);
		
		ArrayList<String[]> result = new ArrayList<String[]>();
		result.add(Constants.PURCHASE_HEADER);		           
		Cursor cur = mDbHelper.excuteSQL(sql.toString());
		if(cur.moveToFirst()){
			do{
				result.add(new String[]{
					cur.getString(0),
					cur.getString(1),
					cur.getString(2),
					cur.getString(3),
					cur.getString(4),
					cur.getString(5),
					cur.getString(6),
					cur.getString(7),
					cur.getString(8),
					cur.getString(9),
					cur.getString(10),
					cur.getString(11),
					cur.getString(12),
					cur.getString(13),
					cur.getString(14),
					cur.getString(15),
					cur.getString(16),
					cur.getString(17)
				});
			}while(cur.moveToNext());
		}
		cur.close();
		mDbHelper.close();
		return result;
	}
}
