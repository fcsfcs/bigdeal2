package com.wwci.bigdeal.io.input;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.wwci.bigdeal.R;
import com.wwci.bigdeal.db.DbAdapter;
import com.wwci.bigdeal.uitl.Constants;

import android.app.Dialog;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import android.widget.TextView;
import au.com.bytecode.opencsv.CSVReader;

public class CSVView extends ImportView {
	private List<Map<String, String>> items_data = new ArrayList<Map<String, String>>();
	private List<String[]> m_csvData;
	private Cursor cursor;
	private ArrayList<String> loadDataArray;
	private StringBuffer alltags = new StringBuffer();
	private boolean updateTag=true;
	

//	public static final String INTENT_EXTRA = "intent_extra";

	private static final int DIALOG_CSV_IMPORT = 3;
	
	private DbAdapter mDbHelper;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, "csv");

		setContentView(R.layout.import_csv_db);
        mDbHelper = new DbAdapter(this);
        mDbHelper.open();
		m_title = (TextView) findViewById(R.id.title);
		m_title.setText(getString(R.string.csv_file));
		
	    cursor = mDbHelper.fetchData(" where vParamType='Application' and vParamCode='TAG'");
	    startManagingCursor(cursor);
	    loadDataArray = new ArrayList<String>();
	    loadDataArray.add(Constants.ALL);
	    if (cursor!=null && cursor.getCount()>0){
            cursor.moveToFirst();
            alltags.append(cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Parameter.PARAMVALUE)));
	    	String[] arr = alltags.toString().split(",");	    	
		    Arrays.sort(arr,String.CASE_INSENSITIVE_ORDER);	    	
		    for(String s:arr)
		    	loadDataArray.add(s);    	
	    }else{
	    	updateTag=false;
	    }

		m_importer = new Runnable() {
			@SuppressWarnings("unchecked")
			public void run() {
				final String filename = getInput();
				CSVReader csv;
				try {
					csv = new CSVReader(new FileReader(filename));
					m_csvData = csv.readAll();
					csv.close();

					String[] columns = m_csvData.get(0);
					m_csvData.remove(0);


					int price_column=-1;
					int saleprice_column=-1;					
					int status_column=-1;
					int picPath_column=-1;
					int tag_column = -1;
					

					for (int i = 0; i < columns.length; i++) {
						columns[i] = columns[i].trim();

						for (String key : Constants.PLAINTEXT.keySet()) {
							String val = Constants.PLAINTEXT.get(key).trim();
							if (val.equalsIgnoreCase(columns[i])) {
								columns[i] = key;
							}
						}
                        if (columns[i].equalsIgnoreCase(DbAdapter.Mst_Product.PRODUCTCOST)) {
							price_column = i;
						} else if (columns[i].equalsIgnoreCase(DbAdapter.Mst_Product.PRODUCTPRICE)) {							
							saleprice_column = i;			
						} else if(columns[i].equalsIgnoreCase(DbAdapter.Mst_Product.PRODUCTPIC)){
							status_column = i;	
						} else if(columns[i].equalsIgnoreCase(DbAdapter.Mst_Product.STATUS)){
							status_column = i;		
						}else if (columns[i].equalsIgnoreCase(DbAdapter.Mst_Product.PRODUCTTAG)){
							tag_column = i;
						}
					}

                    // list data for item table
					for (String[] row : m_csvData) {
						HashMap<String, String> rowData = new HashMap<String, String>();
						for (int i = 0; i < row.length; i++) {
							String info = row[i];
                            if (i == price_column) {
								try {
									info = String.valueOf(doubleFormatter(Double.parseDouble(info.replaceAll(",", ""))));
								} catch (NumberFormatException ae) {
									info="0.0";
								}
							} else if (i == saleprice_column) {
								try {
									info = String.valueOf(doubleFormatter(Double.parseDouble(info.replaceAll(",", ""))));
								} catch (NumberFormatException ae) {
									info="0.0";
								}								
							} else if (i == status_column){
								try {
									if (Integer.parseInt(info.replaceAll(",", ""))>1)
										info = "1";
								} catch (NumberFormatException ae) {
									info="1";
								}
							} else if (i == picPath_column){
								info="";
							} else if (i==tag_column){
								String[] arr = info.split(",");
								for (String s:arr){
									if (!loadDataArray.contains(s)){
									    loadDataArray.add(s);
									    alltags.append(s).append(",");
									}
								}
								
							}
								
                            rowData.put(columns[i], info);
                            
																					
						}
						if (rowData.keySet().size() > 0) {
							items_data.add(rowData);
						}
					

					}
					
					
					m_handler.post(new Runnable() {
						public void run() {
							Bundle data = new Bundle();
							data.putString(MESSAGE, getString(R.string.data_parsed));
							data.putBoolean(SUCCESS, true);

							Message msg = new Message();
							msg.setData(data);
							m_handler.handleMessage(msg);
						}
					});
				} catch (final FileNotFoundException e) {
					m_handler.post(new Runnable() {
						public void run() {
							Bundle data = new Bundle();
							data.putString(MESSAGE, e.getMessage());
							data.putBoolean(SUCCESS, false);

							Message msg = new Message();
							msg.setData(data);
							m_handler.handleMessage(msg);
						}
					});
				} catch (final IOException e) {
					m_handler.post(new Runnable() {
						public void run() {
							Bundle data = new Bundle();
							data.putString(MESSAGE, e.getMessage());
							data.putBoolean(SUCCESS, false);

							Message msg = new Message();
							msg.setData(data);
							m_handler.handleMessage(msg);
						}
					});
				}
			}
		};

		m_handler = new Handler() {
			public void handleMessage(Message msg) {
				Bundle data = msg.getData();
				if (data.getBoolean(SUCCESS, false)) {
					dismissDialog(DIALOG_IMPORTING);
					showDialog(DIALOG_CSV_IMPORT);
				}
			}
		};
	}

	@Override
	protected Dialog onCreateDialog(int which) {
		switch (which) {
			case DIALOG_CSV_IMPORT:
				return new CSVViewConfirm(this);
		}
		return super.onCreateDialog(which);
	}

	public List<Map<String, String>> getData() {
		return items_data;
	}	
	
	public StringBuffer getAlltags() {
		return alltags;
	}		

	public boolean isUpdateTag() {
		return updateTag;
	}

	public DbAdapter getOpen(){
		return mDbHelper;
	}
	
    private String doubleFormatter(Double val){
    	DecimalFormat  df =  new DecimalFormat( "##.00" ); 
    	return df.format(val); 
    }
	@Override
	protected void onDestroy() {
		mDbHelper.close();
		super.onDestroy();
	}

	
}
