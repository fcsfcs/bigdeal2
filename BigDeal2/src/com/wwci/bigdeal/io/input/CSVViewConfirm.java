package com.wwci.bigdeal.io.input;


import java.text.DecimalFormat;
import java.util.ArrayList;

import java.util.List;
import java.util.Map;

import com.wwci.bigdeal.R;
import com.wwci.bigdeal.db.DbAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;


public class CSVViewConfirm extends Dialog {
	private Activity m_context;
	private ProgressDialog m_progress;
	private List<Map<String, String>> item_data;
	private StringBuffer alltags;
	private boolean updateTag;
	
	private List<String> m_sqls;

	private Button m_importBtn;
	private DbAdapter mDbHelper;
	
	public CSVViewConfirm(Activity context) {
		super(context);
		m_context = context;
	}

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		item_data = new ArrayList<Map<String, String>>();
		m_sqls = new ArrayList<String>();
		alltags = new StringBuffer();
		updateTag = false;

		if (m_context instanceof CSVView) {
			CSVView c = (CSVView) m_context;
			item_data = c.getData();
			alltags = c.getAlltags();
			updateTag = c.isUpdateTag();
			mDbHelper = c.getOpen();
		}

		setContentView(R.layout.import_csv_confirm);
		initUI();
        
		setTitle(R.string.import_options);

	}

	private void initUI() {
		// parse the date
//		Map<String, String> row = item_data.get(0);
		m_importBtn = (Button) findViewById(R.id.start);
		m_importBtn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				doImport();
				dismiss();
			}
		});
	}

	private void doImport() {
		m_progress = new ProgressDialog(m_context);
		m_progress.setTitle(R.string.importing_title);
		m_progress.setIndeterminate(false);
		m_progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		m_progress.setProgress(0);
		m_progress.setMax((item_data.size()) * 2 );
		m_progress.show();
		Thread t = new Thread(new Runnable() {
			public void run() {		
				
				// update tag
				if (updateTag)
					mDbHelper.updateData(alltags.toString(),"Application","TAG");
				else
					mDbHelper.insertData(alltags.toString(),"Application","TAG");
					
				// create SQL from the parsed data
				m_sqls = new ArrayList<String>();
				for (Map<String, String> row : item_data) {
					StringBuilder sb = new StringBuilder();
					sb.append("INSERT INTO ").append(DbAdapter.Mst_Product.TABLE_NAME).append(" (");

					String[] row_data = row.keySet().toArray(new String[row.keySet().size()]);
					for (int i = 0; i < row_data.length; i++) {
						sb.append(row_data[i]);
					   	if (i < row_data.length - 1) {
						   sb.append(", ");
						}

					}

					sb.append(") VALUES (");
					for (int i = 0; i < row_data.length; i++) {
						sb.append("?");
						if (i < row_data.length - 1) {
							sb.append(", ");
						}

					}
					sb.append(")");

					m_sqls.add(sb.toString());
					m_progressHandler.post(new Runnable() {
						public void run() {
							m_progressHandler.sendEmptyMessage(1);
						}
					});
				}

				for (int i = 0; i < m_sqls.size(); i++) {
					Map<String, String> row = item_data.get(i);
					String[] params = new String[row.keySet().size()];
					String[] keys = row.keySet().toArray(new String[params.length]);
					for (int j = 0; j < keys.length; j++) {
						params[j] = row.get(keys[j]);						
					}
					mDbHelper.excuteSQL(m_sqls.get(i), params);
					m_progressHandler.post(new Runnable() {
						public void run() {
							m_progressHandler.sendEmptyMessage(1);
						}
					});
				}
//				db.close();
				m_handler.post(new Runnable() {
					public void run() {
						Bundle data = new Bundle();
						data.putString(CSVView.MESSAGE, m_context.getString(R.string.import_done_msg));
						data.putBoolean(CSVView.SUCCESS, true);

						Message msg = new Message();
						msg.setData(data);
						m_importHandler.handleMessage(msg);
					}
				});
			}
		});
		t.start();
	}

	protected Handler m_handler = new Handler() {
		public void handleMessage(Message msg) {
			dismiss();
		}
	};

	private Handler m_progressHandler = new Handler() {
		public void handleMessage(Message msg) {
			m_progress.setProgress(m_progress.getProgress() + 1);
		}
	};

	private Handler m_importHandler = new Handler() {
		public void handleMessage(Message msg) {
			if (m_progress != null) {
				m_progress.dismiss();
			}

			final Bundle data = msg.getData();

			AlertDialog dlg = new AlertDialog.Builder(m_context).create();
			final boolean success = data.getBoolean(CSVView.SUCCESS, false);
			if (success) {
				dlg.setTitle(R.string.success);
			} else {
				dlg.setTitle(R.string.error);
			}
			dlg.setMessage(data.getString(CSVView.MESSAGE) + ((CSVView) m_context).getInput());
			dlg.setButton(m_context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
			dlg.show();
		}
	};
    private String doubleFormatter(Double val){
    	DecimalFormat  df =  new DecimalFormat( "##.00" ); 
    	return df.format(val); 
    }
    
}
