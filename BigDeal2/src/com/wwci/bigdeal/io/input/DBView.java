package com.wwci.bigdeal.io.input;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import com.wwci.bigdeal.R;
import com.wwci.bigdeal.db.DbAdapter;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.widget.TextView;



public class DBView extends ImportView {
	private DbAdapter mDbHelper;
	private ProgressDialog m_progress;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, "db");
		m_title = (TextView) findViewById(R.id.title);
		m_title.setText(R.string.management_sqlite);
		
        mDbHelper = new DbAdapter(this);
        mDbHelper.open();

		super.m_importer = new Runnable() {
			public void run() {
				final String filename = getInput();
				FileInputStream in = null;
				FileOutputStream out = null;
				try {
					in = new FileInputStream(filename);
					out = new FileOutputStream("/data/data/" + DbAdapter.PACKAGE + "/databases/" +DbAdapter.DATABASE_NAME);

					FileChannel inChannel = in.getChannel();
					FileChannel outChannel = out.getChannel();

					outChannel.transferFrom(inChannel, 0, inChannel.size());

					inChannel.close();
					outChannel.close();
					in.close();
					out.close();
					
					mDbHelper.upgradeDatabase();

				} catch (final IOException ioe) {
					m_handler.post(new Runnable() {
						public void run() {
							Bundle data = new Bundle();
							data.putString(MESSAGE, ioe.getMessage());
							data.putBoolean(SUCCESS, false);

							Message msg = new Message();
							msg.setData(data);
							m_handler.handleMessage(msg);
						}
					});
					return;
				}
				m_handler.post(new Runnable() {
					public void run() {
						Bundle data = new Bundle();
						data.putString(MESSAGE, filename+" " +getString(R.string.import_done_msg));
						data.putBoolean(SUCCESS, true);

						Message msg = new Message();
						msg.setData(data);
						m_handler.handleMessage(msg);
						
					}
				});
				
			}
		};
		

	}
	
	@Override
	protected void onDestroy() {
		mDbHelper.close();
		super.onDestroy();
        Intent mIntent = new Intent();	  
        setResult(RESULT_OK, mIntent);
        finish();
	}
}
