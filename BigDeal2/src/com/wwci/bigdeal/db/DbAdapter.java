package com.wwci.bigdeal.db;

import java.util.ArrayList;

import com.wwci.bigdeal.model.Product;
import com.wwci.bigdeal.model.Purchase;
import com.wwci.bigdeal.model.PurchaseItem;
import com.wwci.bigdeal.uitl.FileUtil;
import com.wwci.bigdeal.uitl.StringTools;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;
public class DbAdapter {
	private static final String TAG = "DbAdapter";
	public static final String DATABASE_NAME="bigdeal.db";
	public static final String PACKAGE = "com.wwci.bigdeal2";   
	private static final int DATABASE_VERSION =1;
	private final Context mCtx;
	
	private DatabaseHelper mDBHelper; 
	private SQLiteDatabase mDb;
	protected Cursor cursor;
	
	
	public DbAdapter(Context ctx){
		this.mCtx = ctx;
		mDBHelper = new DatabaseHelper(mCtx);
	}
	
	public DbAdapter open() throws SQLException{				
		mDb = mDBHelper.getWritableDatabase();
		return this;
	}
	
	public void close(){
		mDb.close();
		mDBHelper.close();
	}
	
	
	//Home
	public Cursor homeReport(String table, String where){
		return mDb.rawQuery("Select dDueDate,nAmount,nPaid,vStatus from "+table+"  where substr(dDueDate,1,4)||substr(dDueDate,6,2)= '"+where+"'", null);
	}
	
	//report - provider report
	public Cursor supplierClientReport(String table, String where){
		return mDb.rawQuery("Select vClient as client,vClientPhone as clientPhone, count(*) as total, sum(nAmount) as amt,sum(nPaid) as amtinpaid from "+table+" where "+where+" group by vClient,vClientPhone order by sum(nPaid) desc", null);
	}
	//report - top product report
	public Cursor productReport(String where){
		return mDb.rawQuery("select c._id,c.vName,sum(a.iQty) as qty, sum(a.nsubtotal) as amt from orderProduct as a, orderheader as b,mst_product c where a.vPurchaseNo= b.vPurchaseNo and a.iProductID=c._id and b.vStatus<>'Canceled' "+ where +" group by c._id,c.vname order by sum(a.nsubtotal) desc",null);
	}
	//report - top client with product report
	public Cursor clientProductReport(String where){
		return mDb.rawQuery("select b.vClient as client,b.vClientPhone as clientPhone, c._id,c.vName,sum(a.iQty) as qty, sum(a.nsubtotal) as amt from orderProduct as a, orderheader as b,mst_product c where a.vPurchaseNo= b.vPurchaseNo and a.iProductID=c._id and b.vStatus<>'Canceled'  "+ where +"  group by b.vClient,b.vClientPhone,c._id order by b.vClient asc,sum(a.nsubtotal) desc",null);
	}

	
	public boolean checkEULA(){
		boolean returnValue=false;
		cursor = mDb.query(true,Eula.TABLE_NAME, new String[]{ Eula.READ},Eula.ID + "=0", null,null,null,null,null);		
		if (cursor !=null && cursor.getCount() > 0){
			cursor.moveToFirst();
			returnValue = cursor.getShort(cursor.getColumnIndex(Eula.READ)) != 0;
		}
		cursor.close();
		return returnValue;
	}
	public void setEULA() {
		ContentValues values = new ContentValues();
		values.put(Eula.ID, 0);
		values.put(Eula.READ, 1); //set that they've read the EULA
		mDb.insert(Eula.TABLE_NAME, null, values);		
	}
	
    public void excuteSQL(String sqlString, String[] paramString){
    	mDb.execSQL(sqlString, paramString);
    }
	public Cursor excuteSQL(String sql) {
		return mDb.rawQuery(sql, null);

	}
	
    public Cursor fetchData(String where){    	
    	return mDb.rawQuery("select * from "+Mst_Parameter.TABLE_NAME+" " +where,null);    	
    }
    
    public Cursor fetchAllData(String tableName, String where){    	
    	return mDb.rawQuery("select * from "+tableName+" " + where,null);
    }
	public void updateData(String value, String type, String code){
		ContentValues values = new ContentValues();
		values.put(Mst_Parameter.PARAMVALUE, value);
		mDb.update(Mst_Parameter.TABLE_NAME, values,Mst_Parameter.PARAMTYPE+"=? and " +Mst_Parameter.PARAMCODE+"=?",new String[]{type,code});
	}
	public boolean insertData(String value, String type, String code){
		ContentValues values = new ContentValues();
		values.put(Mst_Parameter.PARAMVALUE, value);
		values.put(Mst_Parameter.PARAMTYPE, type);
		values.put(Mst_Parameter.PARAMCODE, code);
		return mDb.insert(Mst_Parameter.TABLE_NAME, null,values)>0;
	}
	public boolean checkProduct(String name, String id){	
		String sqlStatement;
		boolean returnvalue=false;
		if(StringTools.isNullOrBlank(id))
			sqlStatement ="SELECT COUNT(*) FROM " + Mst_Product.TABLE_NAME + " WHERE " + Mst_Product.PRODUCTNAME+"='"+name+"' ";
		else
			sqlStatement ="SELECT COUNT(*) FROM " + Mst_Product.TABLE_NAME + " WHERE " + Mst_Product.PRODUCTNAME+"='"+name+"' and " + Mst_Product.PRODUCTID+"<>"+id;
		cursor = mDb.rawQuery(sqlStatement,null);
		cursor.moveToFirst();
        if (cursor.getInt(0)>0) 
        	returnvalue= true; 
        cursor.close();
        return returnvalue;
	}

    public boolean insertProduct(Product product,String alltags){
		ContentValues values = new ContentValues();
		String filename;
		try{
			mDb.beginTransaction();
			FileUtil.getExternalFilesDirAllApiLevels();
		    cursor = fetchData("where vParamType='Application' and vParamCode='TAG'");
		    if (cursor!=null && cursor.getCount()>0){
		    	updateData(alltags,"Application","TAG");
		    }else{
		    	insertData(alltags,"Application","TAG");
		    }
		    
//			values.put(Mst_Product.PRODUCTID,product.getId());
			values.put(Mst_Product.PRODUCTNAME,product.getName());
			values.put(Mst_Product.PRODUCTDESC,product.getDesc());
			values.put(Mst_Product.PRODUCTUNIT,product.getUnit());
			values.put(Mst_Product.PRODUCTCOST,product.getSaleCost());
			values.put(Mst_Product.PRODUCTPRICE,product.getSalePrice());
			values.put(Mst_Product.PRODUCTTAG,product.getTag());
			values.put(Mst_Product.STATUS, 1);
				
			if (!StringTools.isNullOrBlank(product.getPicPath())){
				filename = fetchMaxSeqNo("orderProduct")+".jpg";
				FileUtil.copyFile(product.getPicPath(),FileUtil.getExternalFilePath()+filename);
				values.put(Mst_Product.PRODUCTPIC,filename);
			}

			mDb.insert(Mst_Product.TABLE_NAME, null, values);	
		
			mDb.setTransactionSuccessful();			
		} catch (SQLException e) {
			return false;
		} finally {
			mDb.endTransaction();			
		}
		return true;
	}	
    
	public void updateProduct(String id, String suspended){
		ContentValues values = new ContentValues();
		values.put(Mst_Product.STATUS, suspended);
		mDb.update(Mst_Product.TABLE_NAME, values,Mst_Product.PRODUCTID+"=?",new String[]{id});
	}
	public boolean updateProduct(Product product, String alltags){
		String filename;
		ContentValues values = new ContentValues();
		try{
			mDb.beginTransaction();
			cursor = fetchData("where vParamType='Application' and vParamCode='TAG'");
			if (cursor!=null && cursor.getCount()>0){
			  	updateData(alltags,"Application","TAG");
			}else{
			  	insertData(alltags,"Application","TAG");
			}	
		    
			values.put(Mst_Product.PRODUCTNAME,product.getName());
		    values.put(Mst_Product.PRODUCTDESC,product.getDesc());
		    values.put(Mst_Product.PRODUCTUNIT,product.getUnit());
		    values.put(Mst_Product.PRODUCTCOST,product.getSaleCost());
		    values.put(Mst_Product.PRODUCTPRICE,product.getSalePrice());
		    values.put(Mst_Product.PRODUCTTAG,product.getTag());
		    values.put(Mst_Product.STATUS, product.getStatus());
		    filename = product.getId()+".jpg";
		    FileUtil.deleteFile(FileUtil.getExternalFilePath()+filename);
		    if (!StringTools.isNullOrBlank(product.getPicPath())){			
			   FileUtil.copyFile(product.getPicPath(),FileUtil.getExternalFilePath()+filename);
			   values.put(Mst_Product.PRODUCTPIC,FileUtil.getExternalFilePath()+filename);
		    }

		   mDb.update(Mst_Product.TABLE_NAME, values, Mst_Product.PRODUCTID+"=?",new String[]{product.getId()});		
		   
		   mDb.setTransactionSuccessful();			
		} catch (SQLException e) {
			return false;
		} finally {
			mDb.endTransaction();			
		}
		return true;
	}
	
	public Cursor fetchPurchases(String where){
		return mDb.query(purchaseHeader.TABLE_NAME, new String[]{purchaseHeader.PURCHASEID,purchaseHeader.PURCHASENO, purchaseHeader.PURCHASEDATE,purchaseHeader.DUEDATE, purchaseHeader.STATUS,purchaseHeader.AMOUNT,purchaseHeader.PAID,purchaseHeader.CLIENT,purchaseHeader.CLIENTPHONE}, where, null, null, null,purchaseHeader.DUEDATE);
	}

	public Cursor fetchOrders(String where){
		return mDb.query(orderHeader.TABLE_NAME, new String[]{orderHeader.PURCHASEID,orderHeader.PURCHASENO, orderHeader.PURCHASEDATE,orderHeader.DUEDATE, orderHeader.STATUS,orderHeader.AMOUNT,orderHeader.PAID,orderHeader.CLIENT,orderHeader.CLIENTPHONE}, where, null, null, null,orderHeader.DUEDATE);
	}	
	
	public Cursor fetchNotYetReceivedProduct(String purchaseno){
		return mDb.rawQuery("select a._id,b.vname,a.iQty,b.vdesc,b.vunit,b.vpicpath from purchaseProduct as a, Mst_product as b where a.iProductID=b._id and a.vPurchaseNo='"+purchaseno+"' and (a.dReceiveDate is null or a.dReceiveDate='') order by b.vname",null);
	}
	
	public Cursor fetchNotYetDeliverProduct(String purchaseno){
		return mDb.rawQuery("select a._id,b.vname,a.iQty,b.vdesc,b.vunit,b.vpicpath from orderProduct as a, Mst_product as b where a.iProductID=b._id and a.vPurchaseNo='"+purchaseno+"' and (a.dReceiveDate is null or a.dReceiveDate='') order by b.vname",null);
	}
	
	public void updateReceiveDate(String receiveDate, String selectedItems, boolean receiveAll, String status,String purchaseNo){
		mDb.beginTransaction();
	    try {
	        ContentValues contentValues = new ContentValues();
	        contentValues.put(purchaseProduct.RECEIVEDATE, receiveDate);
	        mDb.update(purchaseProduct.TABLE_NAME, contentValues, purchaseProduct.PURCHASEPID + " IN (" + selectedItems + ")", null);

	        if (receiveAll){
		    	if (status.equals("Paid"))
		    		status = "Completed";
		    	else
		    		status="Received";
		    	cancelResumePurchase(purchaseNo,status);
		    }
	        mDb.setTransactionSuccessful();
	    } catch (Exception e) {

	        Log.i("Error in transaction", e.toString());
	    } finally {

	    	mDb.endTransaction();
	    }


	}
	
	public void updateDeliverDate(String receiveDate, String selectedItems, boolean receiveAll, String status,String purchaseNo){
		mDb.beginTransaction();
	    try {
	        ContentValues contentValues = new ContentValues();
	        contentValues.put(orderProduct.RECEIVEDATE, receiveDate);
	        mDb.update(orderProduct.TABLE_NAME, contentValues, orderProduct.PURCHASEPID + " IN (" + selectedItems + ")", null);

	        if (receiveAll){
		    	if (status.equals("Settled"))
		    		status = "Completed";
		    	else
		    		status="Delivered";
		    	cancelResumeOrder(purchaseNo,status);
		    }
	        mDb.setTransactionSuccessful();
	    } catch (Exception e) {

	        Log.i("Error in transaction", e.toString());
	    } finally {

	    	mDb.endTransaction();
	    }


	}
	
	public Cursor fetchPurchaseProduct(String purchaseno){
		return mDb.rawQuery("select b._id,a.vPurchaseNo,a.iQty,a.nPrice,a.nSubtotal,a.dReceiveDate,b.vname,b.vdesc,b.vunit,b.vpicpath from purchaseProduct as a, Mst_product as b where a.iProductID=b._id and a.vPurchaseNo='"+purchaseno+"'",null);
	}
	
	public Cursor fetchOrderProduct(String purchaseno){
		return mDb.rawQuery("select b._id,a.vPurchaseNo,a.iQty,a.nPrice,a.nSubtotal,a.dReceiveDate,b.vname,b.vdesc,b.vunit,b.vpicpath from orderProduct as a, Mst_product as b where a.iProductID=b._id and a.vPurchaseNo='"+purchaseno+"'",null);
	}
	
	public int fetchMaxSeqNo(String table){		
	    int returnValue =1;	
		cursor = mDb.rawQuery("SELECT COALESCE(MAX(_id)+1, 1) AS max_id FROM "+table,null);
		if (cursor!=null && cursor.getCount()>0){
			cursor.moveToFirst();
			returnValue = cursor.getInt(0);
		}
			
        cursor.close();
        return returnValue;
	}
	
	public void updatePaidAmount(String id, Double paid, String status){
		ContentValues values = new ContentValues();
		values.put(purchaseHeader.PAID, paid);
	    values.put(purchaseHeader.STATUS, status);
	    mDb.update(purchaseHeader.TABLE_NAME, values,purchaseHeader.PURCHASEID+"=?",new String[]{id});
    }

	public void updateSettleAmount(String id, Double paid, String status){
		ContentValues values = new ContentValues();
		values.put(orderHeader.PAID, paid);
	    values.put(orderHeader.STATUS, status);
	    mDb.update(orderHeader.TABLE_NAME, values,purchaseHeader.PURCHASEID+"=?",new String[]{id});
    }
	
	public void updatePurchaseStatus(String id, String status){
		ContentValues values = new ContentValues();
	    values.put(purchaseHeader.STATUS, status);
	    mDb.update(purchaseHeader.TABLE_NAME, values,purchaseHeader.PURCHASEID+"=?",new String[]{id});
    }
	
	public void cancelResumePurchase(String purchaseno, String status){
		ContentValues values = new ContentValues();
		values.put(purchaseHeader.STATUS, status);
		mDb.update(purchaseHeader.TABLE_NAME, values,purchaseHeader.PURCHASENO+"=?",new String[]{purchaseno});		
	}
	
	public void cancelResumeOrder(String purchaseno, String status){
		ContentValues values = new ContentValues();
		values.put(orderHeader.STATUS, status);
		mDb.update(orderHeader.TABLE_NAME, values,orderHeader.PURCHASENO+"=?",new String[]{purchaseno});		
	}
	
	public boolean updatePurchase(Purchase purchase){
		ContentValues values = new ContentValues();
		ArrayList<PurchaseItem> items = new ArrayList<PurchaseItem>();
		try{
			mDb.beginTransaction();			
			values.put(purchaseHeader.PURCHASENO,purchase.getPurchaseNo());
			values.put(purchaseHeader.PURCHASEDATE,purchase.getPurchaseDate());
			values.put(purchaseHeader.DUEDATE,purchase.getDueDate());
			values.put(purchaseHeader.CLOSEDATE,purchase.getCloseDate());
			values.put(purchaseHeader.AMOUNT,purchase.getAmount());
			values.put(purchaseHeader.PAID,purchase.getPaid());
			values.put(purchaseHeader.STATUS,purchase.getStatus());
			values.put(purchaseHeader.CLIENT,purchase.getClient());
			values.put(purchaseHeader.CLIENTPHONE,purchase.getClientPhone());
						
			mDb.update(purchaseHeader.TABLE_NAME, values, purchaseHeader.PURCHASENO+"=?",new String[]{purchase.getPurchaseNo()});
			mDb.delete(purchaseProduct.TABLE_NAME, purchaseProduct.PURCHASENO + "='" + purchase.getPurchaseNo()+"'", null);
			
			items = purchase.getPurchaseItem();
			values = new ContentValues();
			for (PurchaseItem purchaseItem : items){
				values.put(purchaseProduct.PURCHASENO,purchaseItem.getPurchaseNo());
				values.put(purchaseProduct.PRODUCTID,purchaseItem.getProductID());
				values.put(purchaseProduct.QTY,purchaseItem.getQty());
				values.put(purchaseProduct.PRICE,purchaseItem.getPrice());
				values.put(purchaseProduct.SUBTOTAL,purchaseItem.getSubtotal());
				values.put(purchaseProduct.RECEIVEDATE,purchaseItem.getReceivedDate());
				mDb.insert(purchaseProduct.TABLE_NAME, null, values);
			}
		
		    mDb.setTransactionSuccessful();			
		} catch (SQLException e) {
			return false;
		} finally {
			mDb.endTransaction();			
		}
		return true;	
		
	}
	public boolean insertPurchase(Purchase purchase){
		ContentValues values = new ContentValues();
		ArrayList<PurchaseItem> items = new ArrayList<PurchaseItem>();
		try{
			mDb.beginTransaction();
			values.put(purchaseHeader.PURCHASENO,purchase.getPurchaseNo());
			values.put(purchaseHeader.PURCHASEDATE,purchase.getPurchaseDate());
			values.put(purchaseHeader.DUEDATE,purchase.getDueDate());
			values.put(purchaseHeader.CLOSEDATE,purchase.getCloseDate());
			values.put(purchaseHeader.AMOUNT,purchase.getAmount());
			values.put(purchaseHeader.PAID,purchase.getPaid());
			values.put(purchaseHeader.STATUS,purchase.getStatus());
			values.put(purchaseHeader.CLIENT,purchase.getClient());
			values.put(purchaseHeader.CLIENTPHONE,purchase.getClientPhone());
			
			mDb.insert(purchaseHeader.TABLE_NAME, null, values);	
						
			items = purchase.getPurchaseItem();
			values = new ContentValues();
			for (PurchaseItem purchaseItem : items){
				values.put(purchaseProduct.PURCHASENO,purchaseItem.getPurchaseNo());
				values.put(purchaseProduct.PRODUCTID,purchaseItem.getProductID());
				values.put(purchaseProduct.QTY,purchaseItem.getQty());
				values.put(purchaseProduct.PRICE,purchaseItem.getPrice());
				values.put(purchaseProduct.SUBTOTAL,purchaseItem.getSubtotal());
				values.put(purchaseProduct.RECEIVEDATE,purchaseItem.getReceivedDate());
				mDb.insert(purchaseProduct.TABLE_NAME, null, values);
			}
		
		    mDb.setTransactionSuccessful();			
		} catch (SQLException e) {
			return false;
		} finally {
			mDb.endTransaction();			
		}
		return true;		
	}
	
	public boolean insertOrder(Purchase purchase){
		ContentValues values = new ContentValues();
		ArrayList<PurchaseItem> items = new ArrayList<PurchaseItem>();
		try{
			mDb.beginTransaction();
			values.put(orderHeader.PURCHASENO,purchase.getPurchaseNo());
			values.put(orderHeader.PURCHASEDATE,purchase.getPurchaseDate());
			values.put(orderHeader.DUEDATE,purchase.getDueDate());
			values.put(orderHeader.CLOSEDATE,purchase.getCloseDate());
			values.put(orderHeader.AMOUNT,purchase.getAmount());
			values.put(orderHeader.PAID,purchase.getPaid());
			values.put(orderHeader.STATUS,purchase.getStatus());
			values.put(orderHeader.CLIENT,purchase.getClient());
			values.put(orderHeader.CLIENTPHONE,purchase.getClientPhone());
			
			mDb.insert(orderHeader.TABLE_NAME, null, values);	
						
			items = purchase.getPurchaseItem();
			values = new ContentValues();
			for (PurchaseItem purchaseItem : items){
				values.put(orderProduct.PURCHASENO,purchaseItem.getPurchaseNo());
				values.put(orderProduct.PRODUCTID,purchaseItem.getProductID());
				values.put(orderProduct.QTY,purchaseItem.getQty());
				values.put(orderProduct.PRICE,purchaseItem.getPrice());
				values.put(orderProduct.SUBTOTAL,purchaseItem.getSubtotal());
				values.put(orderProduct.RECEIVEDATE,purchaseItem.getReceivedDate());
				mDb.insert(orderProduct.TABLE_NAME, null, values);
			}
		
		    mDb.setTransactionSuccessful();			
		} catch (SQLException e) {
			return false;
		} finally {
			mDb.endTransaction();			
		}
		return true;		
	}
	
	public boolean updateOrder(Purchase purchase){
		ContentValues values = new ContentValues();
		ArrayList<PurchaseItem> items = new ArrayList<PurchaseItem>();
		try{
			mDb.beginTransaction();			
			values.put(orderHeader.PURCHASENO,purchase.getPurchaseNo());
			values.put(orderHeader.PURCHASEDATE,purchase.getPurchaseDate());
			values.put(orderHeader.DUEDATE,purchase.getDueDate());
			values.put(orderHeader.CLOSEDATE,purchase.getCloseDate());
			values.put(orderHeader.AMOUNT,purchase.getAmount());
			values.put(orderHeader.PAID,purchase.getPaid());
			values.put(orderHeader.STATUS,purchase.getStatus());
			values.put(orderHeader.CLIENT,purchase.getClient());
			values.put(orderHeader.CLIENTPHONE,purchase.getClientPhone());
						
			mDb.update(orderHeader.TABLE_NAME, values, orderHeader.PURCHASENO+"=?",new String[]{purchase.getPurchaseNo()});
			mDb.delete(orderProduct.TABLE_NAME, orderProduct.PURCHASENO + "='" + purchase.getPurchaseNo()+"'", null);
			
			items = purchase.getPurchaseItem();
			values = new ContentValues();
			for (PurchaseItem purchaseItem : items){
				values.put(orderProduct.PURCHASENO,purchaseItem.getPurchaseNo());
				values.put(orderProduct.PRODUCTID,purchaseItem.getProductID());
				values.put(orderProduct.QTY,purchaseItem.getQty());
				values.put(orderProduct.PRICE,purchaseItem.getPrice());
				values.put(orderProduct.SUBTOTAL,purchaseItem.getSubtotal());
				values.put(orderProduct.RECEIVEDATE,purchaseItem.getReceivedDate());
				mDb.insert(orderProduct.TABLE_NAME, null, values);
			}
		
		    mDb.setTransactionSuccessful();			
		} catch (SQLException e) {
			return false;
		} finally {
			mDb.endTransaction();			
		}
		return true;	
		
	}
	
	private static class DatabaseHelper extends SQLiteOpenHelper{

		public DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			createDatabase(db);	
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			upgradeDatabase(db, oldVersion, newVersion);
		}
		
	}
	
	private static void createDatabase(SQLiteDatabase db){
		db.execSQL("CREATE TABLE  if not exists " + Eula.TABLE_NAME + " (" + Eula.ID
				+ " INTEGER PRIMARY KEY autoincrement," + Eula.READ + " INTEGER not null);");

		db.execSQL("CREATE TABLE  if not exists " + Mst_Parameter.TABLE_NAME + " (" + Mst_Parameter.PARAMID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT," + Mst_Parameter.PARAMTYPE + " text ,"
			    + Mst_Parameter.PARAMCODE+ " text ," + Mst_Parameter.PARAMVALUE+" TEXT );");
		
		
		db.execSQL("CREATE TABLE  if not exists " + Mst_Product.TABLE_NAME + " (" + Mst_Product.PRODUCTID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT," +  Mst_Product.PRODUCTNAME + " text ," 
				+ Mst_Product.PRODUCTDESC + " text ," +Mst_Product.PRODUCTUNIT+ " text ," +
				Mst_Product.PRODUCTCOST + " DOUBLE,"+ Mst_Product.PRODUCTPRICE+" DOUBLE,"  +
				Mst_Product.PRODUCTTAG + " text ," + Mst_Product.PRODUCTPIC+" text ," + Mst_Product.STATUS+ " INTEGER DEFAULT '1' );");
		
		db.execSQL("CREATE TABLE  if not exists " + purchaseHeader .TABLE_NAME + " (" + purchaseHeader.PURCHASEID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT,"  +purchaseHeader.PURCHASENO + " text ," + purchaseHeader.PURCHASEDATE + " Date,"
				+ purchaseHeader.DUEDATE+" Date ," + purchaseHeader.CLOSEDATE+" Date ," +
				purchaseHeader.AMOUNT + " DOUBLE ," + purchaseHeader.PAID + " DOUBLE ,"  + " INTEGER DEFAULT '0',"+ 
				purchaseHeader.STATUS+" text," + purchaseHeader.CLIENT + " text, " + purchaseHeader.CLIENTPHONE + " text);");
		
		db.execSQL("CREATE TABLE  if not exists " + purchaseProduct.TABLE_NAME + " (" + purchaseProduct.PURCHASEPID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT,"  +purchaseProduct.PURCHASENO + " text," + purchaseProduct.PRODUCTID + " integer, " +
				purchaseProduct.QTY + " integer,"+purchaseProduct.PRICE + " float,"+purchaseProduct.SUBTOTAL+" double," + purchaseProduct.RECEIVEDATE+" date);");
		
		db.execSQL("CREATE TABLE  if not exists " + orderHeader.TABLE_NAME + " (" + orderHeader.PURCHASEID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT,"  +orderHeader.PURCHASENO + " text ," + orderHeader.PURCHASEDATE + " Date,"
				+ orderHeader.DUEDATE+" Date ," + orderHeader.CLOSEDATE+" Date ," +
				orderHeader.AMOUNT + " DOUBLE ," + orderHeader.PAID + " DOUBLE ,"  + " INTEGER DEFAULT '0',"+ 
				orderHeader.STATUS+" text," + orderHeader.CLIENT + " text, " + orderHeader.CLIENTPHONE + " text);");
		
		db.execSQL("CREATE TABLE  if not exists " + orderProduct.TABLE_NAME + " (" + orderProduct.PURCHASEPID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT,"  +orderProduct.PURCHASENO + " text," + orderProduct.PRODUCTID + " integer, " +
				orderProduct.QTY + " integer,"+orderProduct.PRICE + " float,"+orderProduct.SUBTOTAL+" double," + orderProduct.RECEIVEDATE+" date);");
		
	}
	
	public  boolean upgradeDatabase() {
        int dbVersion = mDb.getVersion();
		if (dbVersion > 0 && dbVersion != DATABASE_VERSION) {
			upgradeDatabase(mDb, dbVersion, DATABASE_VERSION);
			return true;
		}
		return false;
	}
	private static void upgradeDatabase(SQLiteDatabase db, final int oldVersion, final int newVersion) {
		if (oldVersion >=newVersion)
			return;

		Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
				+ newVersion + ", which will destroy all old data");
		 
	}
	
	public boolean clearData(boolean clearPurchase,boolean clearSales, boolean clearProduct){
		try{
			mDb.beginTransaction();
		    if (clearPurchase){
		    	mDb.execSQL("DELETE FROM " + purchaseHeader.TABLE_NAME);
			    mDb.execSQL("DELETE FROM " + purchaseProduct.TABLE_NAME);
		    }
		
		    if (clearSales){
			    mDb.execSQL("DELETE FROM " + orderHeader.TABLE_NAME);
			    mDb.execSQL("DELETE FROM " + orderProduct.TABLE_NAME);
		    }
		
		    if (clearProduct){
			    mDb.execSQL("DELETE FROM " + Mst_Product.TABLE_NAME);
			    mDb.execSQL("DELETE FROM " + Mst_Product.TABLE_NAME + "  where vParamType='Application' and vParamCode='TAG'");
		    }
		    mDb.setTransactionSuccessful();		
		
	    } catch (SQLException e) {
		  return false;
	    } finally {
		  mDb.endTransaction();			
	    }			
		return true;
	}
	
	/**
	 * eula table
	 */
	public static final class Eula implements BaseColumns{
		private Eula(){}
		public static final String TABLE_NAME = "eula";
		public static final String ID = "id";
		public static final String READ = "read";
	}
	
    /**
     * system parameter table
     */
	public static final class Mst_Parameter implements BaseColumns{
		private Mst_Parameter(){}
		public static final String TABLE_NAME = "Mst_Parameter";
		public static final String PARAMID = "_id";
		public static final String PARAMTYPE = "vParamType";
		public static final String PARAMCODE = "vParamCode";
		public static final String PARAMVALUE = "vParamValue";
	}
	/**
	 * Product table
	 */
	public static final class Mst_Product implements BaseColumns{
		private Mst_Product(){}
		public static final String TABLE_NAME = "Mst_Product";
		public static final String PRODUCTID = "_id";
		public static final String PRODUCTNAME = "vName";
		public static final String PRODUCTDESC = "vDesc";
		public static final String PRODUCTUNIT = "vUnit";
		public static final String PRODUCTCOST = "nSaleCost";
		public static final String PRODUCTPRICE = "nSalePrice";
		public static final String PRODUCTTAG = "vTag";
		public static final String PRODUCTPIC =	"vPicPath";
		public static final String STATUS = "iStatus";
		
	}
	
	/**
	 * purchase header table
	 */
	public static final class purchaseHeader implements BaseColumns{
		private purchaseHeader(){}
		public static final String TABLE_NAME = "purchaseHeader";
		public static final String PURCHASEID = "_id";
		public static final String PURCHASENO = "vPurchaseNo";
		public static final String PURCHASEDATE = "dPurchaseDate";
		public static final String DUEDATE = "dDueDate";
		public static final String CLOSEDATE = "dCloseDate";
		public static final String AMOUNT = "nAmount";
		public static final String PAID = "nPaid";
		// 1 - in progress; 2 - Received; 3 - Paid; 4 - completed (received & paid); 5 - cancelled 
		public static final String STATUS = "vStatus"; 
		
		public static final String CLIENT = "vClient";
		public static final String CLIENTPHONE = "vClientPhone";
		
	}
	
	public static final class purchaseProduct implements BaseColumns{
		private purchaseProduct(){}
		public static final String TABLE_NAME = "purchaseProduct";
		public static final String PURCHASEPID = "_id";
		public static final String PURCHASENO = "vPurchaseNo";
		public static final String PRODUCTID = "iProductID";
		public static final String QTY = "iQty";
		public static final String PRICE = "nPrice";
		public static final String SUBTOTAL = "nSubtotal";
		public static final String RECEIVEDATE = "dReceiveDate";		
	}

	/**
	 * purchase header table
	 */
	public static final class orderHeader implements BaseColumns{
		private orderHeader(){}
		public static final String TABLE_NAME = "orderHeader";
		public static final String PURCHASEID = "_id";
		public static final String PURCHASENO = "vPurchaseNo";
		public static final String PURCHASEDATE = "dPurchaseDate";
		public static final String DUEDATE = "dDueDate";
		public static final String CLOSEDATE = "dCloseDate";
		public static final String AMOUNT = "nAmount";
		public static final String PAID = "nPaid";
		// 1 - in progress; 2 - Received; 3 - Paid; 4 - completed (received & paid); 5 - cancelled 
		public static final String STATUS = "vStatus"; 
		
		public static final String CLIENT = "vClient";
		public static final String CLIENTPHONE = "vClientPhone";
		
	}
	
	public static final class orderProduct implements BaseColumns{
		private orderProduct(){}
		public static final String TABLE_NAME = "orderProduct";
		public static final String PURCHASEPID = "_id";
		public static final String PURCHASENO = "vPurchaseNo";
		public static final String PRODUCTID = "iProductID";
		public static final String QTY = "iQty";
		public static final String PRICE = "nPrice";
		public static final String SUBTOTAL = "nSubtotal";
		public static final String RECEIVEDATE = "dReceiveDate";		
	}
	
		
}
