package com.wwci.bigdeal;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Application;

public class BigDealApplication extends Application {
	private ArrayList<HashMap<String, String>> list;
	private HashMap<String, String> purchase;
	private int position;
	private int status; //0-new ; 1-edit
	
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		list = new ArrayList<HashMap<String, String>>();
		purchase = new  HashMap<String, String>();
		status = 0;

	}

	public HashMap<String, String> getPurchase() {
		return purchase;
	}

	public void setPurchase(HashMap<String, String> purchase) {
		this.purchase = purchase;
	}

	public ArrayList<HashMap<String, String>> getList() {
		return list;
	}

	public void setList(ArrayList<HashMap<String, String>> list) {
		this.list = list;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	
	

}
