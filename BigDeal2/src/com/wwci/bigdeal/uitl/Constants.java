package com.wwci.bigdeal.uitl;

import java.util.HashMap;
import java.util.Map;

import android.graphics.Color;
import android.provider.BaseColumns;

public final class Constants implements BaseColumns{
	public static final String LOG_TAG = "com.wwci.bigdeal";
	
	public static final int[] TAG_COLORS= new int[]{Color.rgb(0, 0, 0x8b), Color.rgb(0, 0x8b, 0), Color.rgb(0x8b, 0, 0), Color.rgb(0x8b, 0, 0x8b), Color.rgb(0xee, 0x40, 0)};
	
	
	//Order status
	public static final String ALL = "All";
	public static final String STATUS_PENDING = "Pending";
	public static final String STATUS_RECEIVED = "Received";
	public static final String STATUS_INCOMPLETE = "Incomplete";
	public static final String STATUS_CLAIM = "Claim";
	public static final String STATUS_CLOSED = "Closed";
	public static final String STATUS_CANCELLED = "Canceled";
	
	public static final String STATUS_SUSPEND = "Suspended";
	public static final String STATUS_CANCEL = "Cancel";
	public static final String STATUS_RESUME = "Resume";
	public static final String WARNING = "Warning";
	
	public static final String WARNING_PRODUCT = "Please choose product";
	
	public static final Map<String, String> PLAINTEXT = new HashMap<String, String>();
	
	public static final String[] SALES_HEADER = new String[]{"Number","Order Date", "Due Date", "Close Date", "Deliver Date","Amount", "Invoiced","Status","Client","Phone","Name","Qty.", "Unit","Unit Price", "Subtotal", "Description","Tag"};
	public static final String[] PURCHASE_HEADER = new String[]{"Number","Order Date", "Due Date", "Close Date", "Receive Date","Amount", "Paid","Status","Client","Phone","Name","Qty.", "Unit","Unit Price", "Subtotal", "Description","Tag"};
	
	static {
		PLAINTEXT.put("vName", "Name");
		PLAINTEXT.put("vDesc", "Description");
		PLAINTEXT.put("vUnit", "Unit");
		PLAINTEXT.put("nSaleCost", "Unit Cost");
		PLAINTEXT.put("nSalePrice", "Unit Price");
		PLAINTEXT.put("vTag", "Tag (max 5 tags)");
		PLAINTEXT.put("vPicPath", "Picture Path (keep empty)");
		PLAINTEXT.put("iStatus", "Status (0 - inActive; 1 - Active)");
	}
	
}
