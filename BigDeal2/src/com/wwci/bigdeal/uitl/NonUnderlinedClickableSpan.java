package com.wwci.bigdeal.uitl;

import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;

public class NonUnderlinedClickableSpan extends ClickableSpan {
	@Override
    public void updateDrawState(TextPaint ds) {
//       ds.setColor(ds.linkColor);
       ds.setUnderlineText(false); // set to false to remove underline
    }
	@Override
	public void onClick(View widget) {
		// TODO Auto-generated method stub
		Log.i("ClickableSpan", "Confirm OnClick: "+widget.toString());


	}

}
