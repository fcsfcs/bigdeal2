package com.wwci.bigdeal.uitl;

import java.io.IOException;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import java.util.regex.Matcher;
import java.util.regex.Pattern;



/**
 * <p>
 * Title: WWCI Class Lib
 * </p>
 * <p>
 * Description: Design By CloudOcean Team.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2010
 * </p>
 * <p>
 * Company: WWCI
 * </p>
 * 
 * @author Hannah Yuan
 * @version 1.0
 */

public final class StringTools {

	

	public StringTools() {
	}

	public static String getUpperFirstString(String str) {
		return str.toUpperCase().substring(0, 1) + str.substring(1);

	}

	public static ArrayList clipSentenceBySpace(String str) {
		if (str == null)
			return null;
		if (str.length() < 1)
			return null;
		ArrayList aStr = new ArrayList();
		int wordStartIndex = 0, wordEndIndex;
		do {
			wordEndIndex = str.indexOf(" ", wordStartIndex);
			if (wordEndIndex < 1) {
				// Not find " " and str.length()>1
				aStr.add(str.substring(wordStartIndex, str.length()));
				break;
			} else if (wordEndIndex > 1 && wordEndIndex + 1 == str.length()) {
				// find " " and the position at last
				aStr.add(str.substring(wordStartIndex, wordEndIndex));
				break;
			} else if (wordEndIndex > 1 && wordEndIndex + 1 < str.length()) {
				// find " " but the position not last
				aStr.add(str.substring(wordStartIndex, wordEndIndex));
			}
			wordStartIndex = wordEndIndex + 1;
		} while (true);
		return aStr;
	}

	public static ArrayList dashSentenceBySpace(String str) {
		if (str == null)
			return null;
		if (str.length() < 1)
			return null;
		ArrayList aStr = new ArrayList();
		int wordStartIndex = 0, wordEndIndex;
		do {
			wordEndIndex = str.indexOf("-", wordStartIndex);
			if (wordEndIndex < 1) {
				// Not find " " and str.length()>1
				aStr.add(str.substring(wordStartIndex, str.length()));
				break;
			} else if (wordEndIndex > 1 && wordEndIndex + 1 == str.length()) {
				// find " " and the position at last
				aStr.add(str.substring(wordStartIndex, wordEndIndex));
				break;
			} else if (wordEndIndex > 1 && wordEndIndex + 1 < str.length()) {
				// find " " but the position not last
				aStr.add(str.substring(wordStartIndex, wordEndIndex));
			}
			wordStartIndex = wordEndIndex + 1;
		} while (true);
		return aStr;
	}

	public static String[] clipSentenceBySemicolon(String str) {
		if (str == null)
			return null;
		if (str.length() < 1)
			return null;
		ArrayList aLst = new ArrayList(10);
		int wordStartIndex = 0, wordEndIndex;
		do {
			wordEndIndex = str.indexOf(";", wordStartIndex);
			if (wordEndIndex < 1) {
				// Not find ; and str.length()>1
				aLst.add(str.substring(wordStartIndex, str.length()));
				break;
			} else if (wordEndIndex > 1 && wordEndIndex + 1 == str.length()) {
				// find ; and the position at last
				aLst.add(str.substring(wordStartIndex, wordEndIndex));
				break;
			} else if (wordEndIndex > 1 && wordEndIndex + 1 < str.length()) {
				// find ; but the position not last
				aLst.add(str.substring(wordStartIndex, wordEndIndex));
			}
			wordStartIndex = wordEndIndex + 1;
		} while (true);

		String[] aStr = new String[aLst.size()];
		for (int i = 0; i < aLst.size(); i++) {
			aStr[i] = (String) aLst.get(i);
		}
		if (aLst != null)
			aLst = null;
		return aStr;
	}

	public static String[] clipSentenceByComma(String str) {
		if (str == null)
			return null;
		if (str.length() < 1)
			return null;
		ArrayList aLst = new ArrayList();
		int wordStartIndex = 0, wordEndIndex;
		do {
			wordEndIndex = str.indexOf(",", wordStartIndex);
			if (wordEndIndex < 1) {
				// Not find "," and str.length()>1
				aLst.add(str.substring(wordStartIndex, str.length()));
				break;
			} else if (wordEndIndex > 1 && wordEndIndex + 1 == str.length()) {
				// find "," and the position at last
				aLst.add(str.substring(wordStartIndex, wordEndIndex));
				break;
			} else if (wordEndIndex > 1 && wordEndIndex + 1 < str.length()) {
				// find "," but the position not last
				aLst.add(str.substring(wordStartIndex, wordEndIndex));
			}
			wordStartIndex = wordEndIndex + 1;
		} while (true);
		String[] aStr = new String[aLst.size()];
		for (int i = 0; i < aLst.size(); i++) {
			aStr[i] = (String) aLst.get(i);
		}
		if (aLst != null)
			aLst = null;
		return aStr;
	}

	public static String ArrayToString(String[] str) {
		StringBuffer sb = new StringBuffer("");
		for (int i = 0; i < str.length; i++) {
			if (i > 0)
				sb.append(",");
			sb.append(str[i]);
		}
		return sb.toString();
	}

	public static String[] StringToArray(String str, String regex) {
		return str.split(regex);
	}
	public static boolean toFindStringinArray(String str, String ex){
		if (str == null || str.trim().length() < 1)
			return false;
		
		String [] array= StringToArray(str,",");
		
		for (int i = 0; i < array.length; i++) {
			if (array[i].equals(ex)){
				return true;
			}
			
		}
		return false;
	}
    public static String toReplaceString(String[] str, String oldStr,String newStr){
    	if (str.length < 1)
			return "";
		String results = "";
		for (int i = 0; i < str.length; i++) {
			if (str[i].equals(oldStr)){
				str[i]=newStr;
			}
			results = results + str[i] + (str.length-1>i?",":"");
			
		}
		return results;
    }
	public static String toQuotemarkString(String[] str) {

		if (str.length < 1)
			return null;
		if (str.length == 1)
			return quotemark(str[0]);
		String results = "";
		for (int i = 0; i < str.length - 1; i++) {
			results = results + quotemark(str[i]) + ",";
		}
		return results + quotemark(str[str.length - 1]);
	}
	
	private static String quotemark(String s) {
		return "'" + s + "'";
	}

	public static boolean isNullOrBlank(String str) {
		if (str == null || str.trim().length() < 1) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isInvaildValue(String str){
		if (str == null || str.trim().length() < 1 || str.trim().equals("0") || str.trim().equals("0.0")
		   || str.trim().equals(".") || str.trim().equals(".0") || str.trim().equals(".0")) {
			return true;
		} else {
			return false;
		}
	}

	public static String getNotNullString(String str) {
		if (isNullOrBlank(str))
			return "";
		else
			return str;
	}
	public static String getQtyOrAmt(String str) {
		if (isNullOrBlank(str))
			return "0";
		else
			return str.replace(",", "");
	}
	public static String toTimeDesc(String str) {
		if (str.trim().equalsIgnoreCase("a"))
			return "AM";
		if (str.trim().equalsIgnoreCase("p"))
			return "PM";
		if (str.trim().equalsIgnoreCase("f"))
			return "FULL";
		return str;
	}
	

	public static Date toDate(String strDate, String dateFormat) {
		DateFormat myDateFormat = new SimpleDateFormat(dateFormat);
		Date myDate = new Date();
		if (!isNullOrBlank(strDate)){
			try {
				myDate = myDateFormat.parse(strDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}	
		}
		
		return myDate;
	}
	public static String toString(Date strDate, String dateFormat) {
		DateFormat myDateFormat = new SimpleDateFormat(dateFormat);
		return myDateFormat.format(strDate);
	}

	public static String toConvertDateFormat(String strDate,String dateFormat){
		SimpleDateFormat toFormat =  new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyyy");
		String myDate = toString(new Date(),dateFormat);
		if (!isNullOrBlank(strDate)){
		try {
			myDate = toString(toFormat.parse(strDate),dateFormat);
		} catch (ParseException e) {
				// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		return myDate;

	}
	public static String toConvertDateFormat(String strDate,String fromDateFormat,String toDateFormat){
		SimpleDateFormat fromFormat = new SimpleDateFormat(fromDateFormat,Locale.US);
		SimpleDateFormat toFormat = new SimpleDateFormat(toDateFormat,Locale.US);
		String myDate = toString(new Date(),toDateFormat);
		
		try {
			myDate = toFormat.format(fromFormat.parse(strDate));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return myDate;

	}
	
	public static String toFormatDouble(String strDouble){
		DecimalFormat df = new DecimalFormat("###,###,###,##0.00");
		if (strDouble.equals(""))
			strDouble="0.00";
		return df.format(Double.valueOf(strDouble));
	}
	public static String toFormatInteger(String strInteger){
		DecimalFormat df = new DecimalFormat("###,###,###,##0");
		if (strInteger.equals(""))
			strInteger="0";
		return df.format(Integer.valueOf(strInteger));
	}
	public static int toIneger(String strInteger){
		if (strInteger.equals(""))
			strInteger="0";
		return (int)Double.parseDouble(toReplaceComma(strInteger));
	}
	public static Double toConvertString2Double(String strDouble){
		if (strDouble.equals(""))
			strDouble="0.00";
		return Double.parseDouble(strDouble.replaceAll(",", ""));
	}
	
	public static String toReplaceComma(String strDouble){
		return strDouble.replaceAll(",", "");
	}
	public static String toSQLQuote(String s) {
		String strFind0 = "\\";
		String strReplace0 = "\\\\";
		String strFind = "'";
		String strReplace = "\\'";
		String strFind1 = "&";
		String strReplace1 = "\\&";
		String strFind2 = "\"";
		String strReplace2 = "\\\"";
		String strFind3 = "<";
		String strReplace3 = "\\<";
		String strFind4 = ">";
		String strReplace4 = "\\>";
		String strFind5 = ".";
		String strReplace5 = "\\.";
		String strFind7 = "(";
		String strReplace7 = "\\(";
		String strFind8 = ")";
		String strReplace8 = "\\)";
        String strFind9 = ",";
        String strReplace9 = "\\,";

		int pos = 0;
		boolean done = false;
		int strFindLen = strFind0.length();
		int strReplaceLen = strReplace0.length();
		for (; !done;) {
			pos = s.indexOf(strFind0, pos);
			if (pos >= 0) {
				s = s.substring(0, pos) + strReplace0 + s.substring(pos + strFindLen);
				pos = pos + strReplaceLen;
			} else {
				done = true;
			}
		}
		
		pos = 0;
		done = false;
		strFindLen = strFind.length();
		strReplaceLen = strReplace.length();
		for (; !done;) {
			pos = s.indexOf(strFind, pos);
			if (pos >= 0) {
				s = s.substring(0, pos) + strReplace + s.substring(pos + strFindLen);
				pos = pos + strReplaceLen;
			} else {
				done = true;
			}
		}
		done = false;
		pos = 0;
	    strFindLen = strFind1.length();
		strReplaceLen = strReplace1.length();
		for (; !done;) {
			pos = s.indexOf(strFind1, pos);
			if (pos >= 0) {
				s = s.substring(0, pos) + strReplace1 + s.substring(pos + strFindLen);
				pos = pos + strReplaceLen;
			} else {
				done = true;
			}
		}
		done = false;
		pos = 0;
	    strFindLen = strFind2.length();
		strReplaceLen = strReplace2.length();
		for (; !done;) {
			pos = s.indexOf(strFind2, pos);
			if (pos >= 0) {
				s = s.substring(0, pos) + strReplace2 + s.substring(pos + strFindLen);
				pos = pos + strReplaceLen;
			} else {
				done = true;
			}
		}
		
		done = false;
		pos = 0;
	    strFindLen = strFind3.length();
		strReplaceLen = strReplace3.length();
		for (; !done;) {
			pos = s.indexOf(strFind3, pos);
			if (pos >= 0) {
				s = s.substring(0, pos) + strReplace3 + s.substring(pos + strFindLen);
				pos = pos + strReplaceLen;
			} else {
				done = true;
			}
		}
		
		done = false;
		pos = 0;
	    strFindLen = strFind4.length();
		strReplaceLen = strReplace4.length();
		for (; !done;) {
			pos = s.indexOf(strFind4, pos);
			if (pos >= 0) {
				s = s.substring(0, pos) + strReplace4 + s.substring(pos + strFindLen);
				pos = pos + strReplaceLen;
			} else {
				done = true;
			}
		}
		done = false;
		pos = 0;
	    strFindLen = strFind5.length();
		strReplaceLen = strReplace5.length();
		for (; !done;) {
			pos = s.indexOf(strFind5, pos);
			if (pos >= 0) {
				s = s.substring(0, pos) + strReplace5 + s.substring(pos + strFindLen);
				pos = pos + strReplaceLen;
			} else {
				done = true;
			}
		}
				
		done = false;
		pos = 0;
	    strFindLen = strFind7.length();
		strReplaceLen = strReplace7.length();
		for (; !done;) {
			pos = s.indexOf(strFind7, pos);
			if (pos >= 0) {
				s = s.substring(0, pos) + strReplace7 + s.substring(pos + strFindLen);
				pos = pos + strReplaceLen;
			} else {
				done = true;
			}
		}
		done = false;
		pos = 0;
	    strFindLen = strFind8.length();
		strReplaceLen = strReplace8.length();
		for (; !done;) {
			pos = s.indexOf(strFind8, pos);
			if (pos >= 0) {
				s = s.substring(0, pos) + strReplace8 + s.substring(pos + strFindLen);
				pos = pos + strReplaceLen;
			} else {
				done = true;
			}
		}

       	done = false;
		pos = 0;
	    strFindLen = strFind9.length();
		strReplaceLen = strReplace9.length();
		for (; !done;) {
			pos = s.indexOf(strFind9, pos);
			if (pos >= 0) {
				s = s.substring(0, pos) + strReplace9 + s.substring(pos + strFindLen);
				pos = pos + strReplaceLen;
			} else {
				done = true;
			}
		}
		return s;
		
		
	}

	public static String RCoFtn(String s) {
		String strFind = "'";
		String strReplace = "''";
		
		int strFindLen = strFind.length();
		int strReplaceLen = strReplace.length();
		int pos = 0;

		boolean done = false;
		for (; !done;) {
			pos = s.indexOf(strFind, pos);
			if (pos >= 0) {
				s = s.substring(0, pos) + strReplace + s.substring(pos + strFindLen);
				pos = pos + strReplaceLen;
			} else {
				done = true;
			}
		}
		return s;
	}
    public static String SingleQuote2Double(String s){
		String strFind = "'";
		String strReplace = "''";
		s.replaceAll(strFind, strReplace);
    	return s;
    }
    
    public static String DoubleQuote2Single(String s){
		String strFind = "''";
		String strReplace = "'";
		s.replaceAll(strFind, strReplace);
    	return s;
    }
    
   
	public static String nl2br(String s) {
        s.replaceAll("(\r\n|\r|\n|\n\r)", "<br>");
		return s;
	}


}