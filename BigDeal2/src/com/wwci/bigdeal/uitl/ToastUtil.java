package com.wwci.bigdeal.uitl;

import com.wwci.bigdeal.R;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class ToastUtil {

	public static void getCustomToast(Context context, String message){
		LayoutInflater inflater =  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.toast_layout,null);
		TextView text = (TextView) layout.findViewById(R.id.text);
		Toast toast = new Toast(context);
		text.setText(message);
		toast.setGravity(Gravity.CLIP_VERTICAL, 0, 0);
//		toast.setGravity(Gravity.FILL, 0, 0);
		toast.setDuration(Toast.LENGTH_SHORT);
		toast.setView(layout);
		toast.show();
	}
}
