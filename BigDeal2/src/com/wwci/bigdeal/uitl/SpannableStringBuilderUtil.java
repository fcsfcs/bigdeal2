package com.wwci.bigdeal.uitl;

import com.wwci.bigdeal.R;
import com.wwci.bigdeal.activity.ProductEditActivity;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public final class SpannableStringBuilderUtil {

	public SpannableStringBuilderUtil() {
		super();
		// TODO Auto-generated constructor stub
	}
	public static SpannableStringBuilder setForegroundColor(String text){
    	int start;
    	int end;
    	int space;
    	String[] arr = text.split(",");
    	SpannableStringBuilder word = new SpannableStringBuilder();
    	start=0;
    	end=0;
    	space=1;
    	
    	for (int i=0;i<arr.length;i++){
    		word.append((i>0?" ":"")).append(arr[i]);
    		start=end+(i>0?space:0);
    		end += arr[i].length()+(i>0?space:0);
    		word.setSpan(new ForegroundColorSpan(Constants.TAG_COLORS[i]), start, end, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
    		
    	}
    	return word;
    }
	
	public static SpannableStringBuilder setClickspans(String text, final EditText product_tag){
    	int start;
    	int end;
    	int space;

    	String[] arr = text.split(",");
    	SpannableStringBuilder word = new SpannableStringBuilder();
    	start=0;
    	end=0;
    	space=1;
    	
    	for (int i=0;i<arr.length;i++){
    		word.append((i>0?" ":"")).append(arr[i]);
    		final String s = arr[i];
    		start=end+(i>0?space:0);
    		end += arr[i].length()+(i>0?space:0);
    		
    		word.setSpan(new NonUnderlinedClickableSpan() {
    			@Override
    			public void onClick(View widget) {
    				Log.i("ExampleClickableLink", s);
    			    if (checkTagNotExist(s,product_tag))
    			    	product_tag.setText(product_tag.getText().toString()+(product_tag.getText().toString().equals("")?"":",")+s);
    			}
    			}, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    		word.setSpan(new ForegroundColorSpan(Constants.TAG_COLORS[i]), start, end, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
    		
    	}
    	return word;
    }
	
	private static boolean checkTagNotExist(String ss, EditText product_tag){
		if (StringTools.isNullOrBlank(product_tag.getText().toString().trim())) return true; 
		
		
		String[] arr = product_tag.getText().toString().trim().split(",");
		if (arr.length<5){
			for (String s:arr){
				if (s.equalsIgnoreCase(ss))  return false;
			}			
		}else return false;
			
		
		
		return true;
		
	}


}
