package com.wwci.bigdeal.uitl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;


import android.os.Environment;
import android.util.Log;

public final class FileUtil {

    // from the Android docs, these are the recommended paths
    private static final String EXT_STORAGE_PATH_PREFIX = "/bigdeal2/";
    private static final String EXT_STORAGE_FILES_PATH_SUFFIX = "files/";
    private static final String EXT_STORAGE_CACHE_PATH_SUFFIX = "cache/";
    

    // Object for intrinsic lock (per docs 0 length array "lighter" than a normal Object)
    public static final Object[] DATA_LOCK = new Object[0];

    private FileUtil() {
    }
    
    
    public static String getExternalFilePath(){
    	return Environment.getExternalStorageDirectory()+ EXT_STORAGE_PATH_PREFIX + EXT_STORAGE_FILES_PATH_SUFFIX;
    }
    
	/**
     * Use Environment to check if external storage is writable.
     * 
     * @return
     */
    public static boolean isExternalStorageWritable() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    /**
     * Use environment to check if external storage is readable.
     * 
     * @return
     */
    public static boolean isExternalStorageReadable() {
        if (isExternalStorageWritable()) {
            return true;
        }
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED_READ_ONLY);
    }

    /**
     * Return the recommended external files directory, whether using API level 8 or lower.
     * (Uses getExternalStorageDirectory and then appends the recommended path.)
     * 
     * @param packageName
     * @return
     */
    public static File getExternalFilesDirAllApiLevels() {
        return FileUtil.getExternalDirAllApiLevels(EXT_STORAGE_FILES_PATH_SUFFIX);
    }

    /**
     * Return the recommended external cache directory, whether using API level 8 or lower.
     * (Uses getExternalStorageDirectory and then appends the recommended path.)
     * 
     * @param packageName
     * @return
     */
    public static File getExternalCacheDirAllApiLevels() {
        return FileUtil.getExternalDirAllApiLevels(EXT_STORAGE_CACHE_PATH_SUFFIX);
    }

    private static File getExternalDirAllApiLevels(final String suffixType) {
        File dir = new File(Environment.getExternalStorageDirectory()+ EXT_STORAGE_PATH_PREFIX + suffixType);        
        synchronized (FileUtil.DATA_LOCK) {
            try {
            	if(!dir.exists()){
                    dir.mkdirs();
                    dir.createNewFile();
            	}

            } catch (IOException e) {
                Log.e(Constants.LOG_TAG, "Error creating file", e);
            }
        }
        return dir;
    }
    
    public static File createTemporaryFile(String part, String ext) throws Exception
    {
        File tempDir= Environment.getExternalStorageDirectory();
        tempDir=new File(tempDir.getAbsolutePath()+"/DCIM/Camera");
        if(!tempDir.exists())
        {
            tempDir.mkdir();
        }
        return File.createTempFile(part, ext, tempDir);
    }  

    /**
     * Copy file, return true on success, false on failure.
     * 
     * @param src
     * @param dst
     * @return
     */
    public static boolean copyFile(final String src, final String dst) {
        boolean result = false;
        File srcFile = new File(src);
        File dstFile = new File(dst);
        FileChannel inChannel = null;
        FileChannel outChannel = null;
        synchronized (FileUtil.DATA_LOCK) {
            try {
                inChannel = new FileInputStream(srcFile).getChannel();
                outChannel = new FileOutputStream(dstFile).getChannel();
                inChannel.transferTo(0, inChannel.size(), outChannel);
                result = true;
            } catch (IOException e) {

            } finally {
                if (inChannel != null && inChannel.isOpen()) {
                    try {
                        inChannel.close();
                    } catch (IOException e) {
                        // ignore
                    }
                }
                if (outChannel != null && outChannel.isOpen()) {
                    try {
                        outChannel.close();
                    } catch (IOException e) {
                        // ignore
                    }
                }
            }
        }
        return result;
    }
    
    /**
     * Copy file, return true on success, false on failure.
     * 
     * @param src
     * @param dst
     * @return
     */
    public static boolean copyFile(final File src, final File dst) {
        boolean result = false;
        FileChannel inChannel = null;
        FileChannel outChannel = null;
        synchronized (FileUtil.DATA_LOCK) {
            try {
                inChannel = new FileInputStream(src).getChannel();
                outChannel = new FileOutputStream(dst).getChannel();
                inChannel.transferTo(0, inChannel.size(), outChannel);
                result = true;
            } catch (IOException e) {

            } finally {
                if (inChannel != null && inChannel.isOpen()) {
                    try {
                        inChannel.close();
                    } catch (IOException e) {
                        // ignore
                    }
                }
                if (outChannel != null && outChannel.isOpen()) {
                    try {
                        outChannel.close();
                    } catch (IOException e) {
                        // ignore
                    }
                }
            }
        }
        return result;
    }
	 /**
     *  �?�移檔案
     *  
     *  @param sourceFilePath   來�?檔案的實際路徑(Ex: D:/source/sorce.xml)
     *  @param targetFilePath   目的檔案的實際路徑(Ex: D:/target/target.xml)
     *  
     *  @return         true:�?�移�?功 / false:�?�移失敗
     */
    public static boolean moveFile(String sourceFilePath,String targetFilePath){
        File sourceFile = new File(sourceFilePath);
        File targetFile = new File(targetFilePath);
        if (sourceFile.exists() == true) {
        	if (sourceFile.renameTo(targetFile) == true) {
        		return true;
            }else{
                return false;
            }
        }else{
        	return false;
        }
    }    
    
    /**
     * 删除目录
     * @param dir 目录路径
     * @return         true:�?功 / false:失敗
     */
    public static boolean deleteDirectory(File dir){
    	if (dir.isDirectory()) {
    		String[] children = dir.list();
    		for (int i = 0; i < children.length; i++) {
    			new File(dir, children[i]).delete();
    	    }
    		
    		return dir.delete();
    	
    	}
    	return false;
    }
    
    /**
     * 删除目录中所有的文件
     * @param dir 目录路径
     * @return         true:�?功 / false:失敗
     */
    public static boolean deleteFilesFromDirectory(String dirPath){
    	File dir = new File(dirPath);
    	if (dir.isDirectory()) {
    		String[] children = dir.list();
    		for (int i = 0; i < children.length; i++) {
    			new File(dir, children[i]).delete();
    	    }
    		
    		return true;
    	
    	}
    	return false;
    }
    public static boolean deleteFile(String filepath){
    	File file = new File(filepath);
    	if (file.exists() == true) 
    		return file.delete();   
    	else
    		return false;
    }
 
}
