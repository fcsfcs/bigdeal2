package com.wwci.bigdeal.uitl;

import java.util.ArrayList;
import java.util.HashMap;

import com.wwci.bigdeal.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class OrderAdapter extends ArrayAdapter<HashMap<String, String>> {
	protected ArrayList<HashMap<String, String>> orders;
	protected HashMap<String, String> order=new HashMap<String,String>();
	
	public OrderAdapter(Context context, int textViewResourceId,
			ArrayList<HashMap<String, String>> objects) {
		super(context, textViewResourceId, objects);
		// TODO Auto-generated constructor stub
		this.orders=objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.purchase_order_list_item, null);
        }
        HashMap<String,String> order = orders.get(position);
        if (order != null) {
        	TextView product_name = (TextView)v.findViewById(R.id.title);
        	TextView product_qty= (TextView)v.findViewById(R.id.qty);    

        	product_name.setText(order.get("product_name"));
        	product_qty.setText(order.get("product_qty")+" " + order.get("product_unit")+" X " + order.get("product_cost")+" = " + order.get("product_subtotal"));
        }
        return v;
	}

}
