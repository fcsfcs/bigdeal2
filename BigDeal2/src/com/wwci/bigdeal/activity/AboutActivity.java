package com.wwci.bigdeal.activity;


import java.util.Locale;

import com.wwci.bigdeal.R;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class AboutActivity extends Activity {
	private WebView webView;
	private String locale_language;
	
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.help);
		
		webView = (WebView) findViewById(R.id.webView1);
		webView.setWebViewClient(new WebViewClient());
		
		locale_language = Locale.getDefault().getISO3Country();

		if (locale_language.equals("TWN"))
			webView.loadUrl("file:///android_asset/BigDeal_About_ZH_TW.html");
		else if(locale_language.equals("CHN"))
			webView.loadUrl("file:///android_asset/BigDeal_About_ZH_CN.html");
		else
			webView.loadUrl("file:///android_asset/BigDeal_About_ENG.html");

	}
}
