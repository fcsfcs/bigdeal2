package com.wwci.bigdeal.activity;


import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.wwci.bigdeal.db.DbAdapter;
import com.wwci.bigdeal.uitl.StringTools;
import com.wwci.bigdeal.R;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.GestureDetector.OnGestureListener;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.ViewFlipper;



public class HomeActivity extends Activity implements OnGestureListener{
    /** Called when the activity is first created. */

	private TextView current_date;
	private TextView order_total;
	private TextView order_today;
	private TextView order_overdue;
	private TextView order_canceled;
	private TextView order_amt;

	private TextView purchase_total;
	private TextView purchase_today;
	private TextView purchase_overdue;
	private TextView purchase_canceled;
	private TextView purchase_amt;
	
	private int mYear;     
	private int mMonth;      
	private int mDay;
	private String currentMonthYear;
	private SimpleDateFormat format1;
	private DbAdapter mDbHelper;
	private int minMonth;
	private int maxMonth;
	
	private GestureDetector detector;
	private ViewFlipper flipper;
	
	
	protected Cursor cursor;
	protected List<HashMap<String, String>> productsList;
	
    @SuppressWarnings("deprecation")
	public void onCreate(Bundle icicle) {
        super.onCreate(icicle); 

        setContentView(R.layout.home);
        
        mDbHelper = new DbAdapter(this);
        mDbHelper.open();
        final Calendar c = Calendar.getInstance();     
        mYear = c.get(Calendar.YEAR);     
        mMonth = c.get(Calendar.MONTH);  
        mDay = c.get(Calendar.DAY_OF_MONTH);  
        this.minMonth = 0;
        this.maxMonth = this.mMonth;
           
       	format1 = new SimpleDateFormat("yyyyMM");
       	currentMonthYear = format1.format(new Date());
       	flipper = (ViewFlipper) this.findViewById(R.id.flippersss);
    	flipper.addView(loadedData(mMonth,currentMonthYear), new ViewGroup.LayoutParams(
    			ViewGroup.LayoutParams.MATCH_PARENT,
    			ViewGroup.LayoutParams.WRAP_CONTENT));
    	detector = new GestureDetector(this);
    	
    }
    

	private View loadedData(int month, String currentMonthYear){ //loads products from the db
    	View view = this.getLayoutInflater().inflate(R.layout.home_innerlist, null);
    	current_date = (TextView) view.findViewById(R.id.current_date);
    	order_total = (TextView) view.findViewById(R.id.order_total);
    	order_today = (TextView) view.findViewById(R.id.order_today);
    	order_overdue = (TextView) view.findViewById(R.id.order_overdue);
    	order_canceled = (TextView) view.findViewById(R.id.order_canceled);
    	order_amt = (TextView) view.findViewById(R.id.order_amt);

    	purchase_total = (TextView) view.findViewById(R.id.purchase_total);
    	purchase_today = (TextView) view.findViewById(R.id.purchase_today);
    	purchase_overdue = (TextView) view.findViewById(R.id.purchase_overdue);
    	purchase_canceled = (TextView) view.findViewById(R.id.purchase_canceled);
    	purchase_amt = (TextView) view.findViewById(R.id.purchase_amt);
//    	format1 = new SimpleDateFormat("yyyy-MM");
    	
    	current_date.setText(currentMonthYear.substring(0, 4)+"-"+currentMonthYear.substring(4));
    	
    	int total=0;
    	int today=0;
    	int overdue=0;
    	int canceled=0;
    	double amt=0.0;
    	String today_date = StringTools.toConvertDateFormat(new StringBuilder()
    	.append(mMonth + 1).append("/")
    	.append(mDay).append("/")
    	.append(mYear).toString(),"MM/dd/yyyy","yyyy-MM-dd"); 
//    	
   	    SharedPreferences settings = getSharedPreferences("Preference", 0);
   	    String currency = settings.getString("currency", "");
   	 
        NumberFormat nf = NumberFormat.getCurrencyInstance();
   	    if (!currency.equals("")){
       	   Currency newCurrency = Currency.getInstance(currency);
       	   nf.setCurrency(newCurrency);
   	    }    	

   	    String currentMonth = DateUtils.getMonthString(month, 0);
   	    cursor = mDbHelper.homeReport("orderHeader",currentMonthYear);
	    startManagingCursor(cursor);
	    if (cursor!=null && cursor.getCount()>0){
	    	cursor.moveToFirst(); 
	    	for (int i=0; i<cursor.getCount(); i++){
	    		if (cursor.getString(cursor.getColumnIndex("vStatus")).equals("Canceled"))	    		
	    			canceled = canceled+1;
	    		else if (cursor.getString(cursor.getColumnIndex("vStatus")).equals("Completed")){	    			
	    			amt = amt + Double.parseDouble(cursor.getString(cursor.getColumnIndex("nAmount")));
	    		}else{
	    			amt = amt + Double.parseDouble(cursor.getString(cursor.getColumnIndex("nAmount")));
	    			if (cursor.getString(cursor.getColumnIndex("dDueDate")).compareTo(today_date)<0)
	    				overdue=overdue+1;
                    else if (cursor.getString(cursor.getColumnIndex("dDueDate")).compareTo(today_date)==0)
	    				today=today+1;     
	    		}
	    			
	    		
	    	}
	    	
	    	total=cursor.getCount()-canceled;
	    }
    	order_total.setText(""+total);
    	order_today.setText(""+today);
    	order_overdue.setText(""+overdue);
    	order_canceled.setText(""+canceled);
    	order_amt.setText(""+nf.format(amt));
    	
     	total=0;
    	today=0;
    	overdue=0;
    	canceled=0;
    	amt = 0.0;
    	
    	cursor = mDbHelper.homeReport("purchaseHeader",currentMonthYear);
	    startManagingCursor(cursor);
	    if (cursor!=null && cursor.getCount()>0){
	    	cursor.moveToFirst(); 
	    	for (int i=0; i<cursor.getCount(); i++){
	    		if (cursor.getString(cursor.getColumnIndex("vStatus")).equals("Canceled"))	    		
	    			canceled = canceled+1;
	    		else if (cursor.getString(cursor.getColumnIndex("vStatus")).equals("Completed")){	    			
	    			amt = amt + Double.parseDouble(cursor.getString(cursor.getColumnIndex("nAmount")));
	    		}else{
	    			amt = amt + Double.parseDouble(cursor.getString(cursor.getColumnIndex("nAmount")));
	    			if (cursor.getString(cursor.getColumnIndex("dDueDate")).compareTo(today_date)<0)
	    				overdue=overdue+1;
                    else if (cursor.getString(cursor.getColumnIndex("dDueDate")).compareTo(today_date)==0)
	    				today=today+1;     
	    		}
	    		
	    	}
	    	
	    	total=cursor.getCount()-canceled;
	    }
	    purchase_total.setText(""+total);
	    purchase_today.setText(""+today);
	    purchase_overdue.setText(""+overdue);
	    purchase_canceled.setText(""+canceled);
	    purchase_amt.setText(""+nf.format(amt));
	   
	    return view;
   }
  
  
      private String str2Date(String dateStr){
	    	try {
				return format1.format(format1.parse(dateStr));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
			return "";
	    }

  
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	// TODO Auto-generated method stub
    	super.onActivityResult(requestCode, resultCode, data);

    	switch(requestCode) {
    	case 0:
    		loadedData(mMonth,currentMonthYear); 
    	    break;
    	case 1:
    		loadedData(mMonth,currentMonthYear); 
    	    break;    	    
    	}
    }

	@Override
	protected void onDestroy() {
		mDbHelper.close();
		super.onDestroy();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
//		Log.e("Fling", "Activity onTouchEvent!");
		return this.detector.onTouchEvent(event);
	}
@Override
public boolean onDown(MotionEvent e) {
//	Log.e("onDown", "E: "+e.getX());
	return true;
}


@Override
public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
		float velocityY) {
	if (e1.getX() - e2.getX() > 100) {
		if(this.mMonth<this.maxMonth){
			this.mMonth ++;			
			this.flipper.setInAnimation(AnimationUtils.loadAnimation(this,
					R.anim.push_left_in));
			this.flipper.setOutAnimation(AnimationUtils.loadAnimation(this,
					R.anim.push_left_out));
			this.flipper.showNext();
			
			currentMonthYear = str2Date(mYear+""+(mMonth+1));
			this.flipper.addView(loadedData(mMonth,currentMonthYear),0,
					new ViewGroup.LayoutParams(
							ViewGroup.LayoutParams.MATCH_PARENT,
							ViewGroup.LayoutParams.WRAP_CONTENT));
			this.flipper.setDisplayedChild(0);
			
		}	
	} else if (e1.getX() - e2.getX() < -100) {
		if (this.mMonth>this.minMonth){
			this.mMonth--;
			this.flipper.setInAnimation(AnimationUtils.loadAnimation(this,
					R.anim.push_right_in));
			this.flipper.setOutAnimation(AnimationUtils.loadAnimation(this,
					R.anim.push_right_out));
			this.flipper.showPrevious();
			currentMonthYear = str2Date(mYear+""+(mMonth+1));
			this.flipper.addView(loadedData(mMonth,currentMonthYear),0,
					new ViewGroup.LayoutParams(
							ViewGroup.LayoutParams.MATCH_PARENT,
							ViewGroup.LayoutParams.WRAP_CONTENT));
			this.flipper.setDisplayedChild(0);
		}
		
	}
	return true;

}
@Override
public void onLongPress(MotionEvent e) {
	// TODO Auto-generated method stub
	
}
@Override
public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
		float distanceY) {
	return false;
}
@Override
public void onShowPress(MotionEvent e) {
	// TODO Auto-generated method stub
	
}
@Override
public boolean onSingleTapUp(MotionEvent e) {
	// TODO Auto-generated method stub
	return false;
}

}



