package com.wwci.bigdeal.activity;

import com.wwci.bigdeal.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ReportActivity extends Activity {
	
	private Button btn_monthly;
	private Button btn_supplier;
	private Button btn_client;
	private Button btn_product;
	private Button btn_client_product;
	
	private Intent i;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.report_main);
        initUI();
        setListener();
	}
	
	private void initUI(){
		btn_monthly = (Button) findViewById(R.id.report_monthly);
		btn_supplier = (Button) findViewById(R.id.report_supplier);
		btn_client = (Button) findViewById(R.id.report_client);
		btn_product = (Button) findViewById(R.id.report_product);
		btn_client_product = (Button) findViewById(R.id.report_client_product);
	}
	
	private void setListener(){
		btn_monthly.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				i = new Intent(ReportActivity.this,ReportMonthly.class);
				startActivity(i);	
			}
		});
		
		btn_supplier.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				i = new Intent(ReportActivity.this,ReportSupplier.class);
				i.putExtra("table", "purchaseHeader");
				startActivity(i);
			}
		});
		

		btn_client.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				i = new Intent(ReportActivity.this,ReportSupplier.class);
				i.putExtra("table", "orderHeader");
				startActivity(i);
			}
		});
		
		btn_product.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				i = new Intent(ReportActivity.this,ReportProduct.class);
				startActivity(i);			
			}
		});
		
		btn_client_product.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				i = new Intent(ReportActivity.this,ReportClientProduct.class);
				startActivity(i);	
			}
		});

	}

}
