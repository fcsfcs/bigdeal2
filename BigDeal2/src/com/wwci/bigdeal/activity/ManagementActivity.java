package com.wwci.bigdeal.activity;


import java.util.HashMap;
import java.util.Map;

import com.wwci.bigdeal.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;



public class ManagementActivity extends Activity {
	private Map<ImageButton, Class<?>> m_actions = new HashMap<ImageButton, Class<?>>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.management);
		initUI();
		
	}
	
	private void initUI(){
		m_actions.put((ImageButton) findViewById(R.id.import_db_btn),com.wwci.bigdeal.io.input.DBView.class);
		m_actions.put((ImageButton) findViewById(R.id.export_db_btn), com.wwci.bigdeal.io.output.DBView.class);
		
		m_actions.put((ImageButton) findViewById(R.id.import_csv_btn), com.wwci.bigdeal.io.input.CSVView.class);
		
		m_actions.put((ImageButton) findViewById(R.id.export_csv_btn), com.wwci.bigdeal.io.output.PurchaseView.class);		
		m_actions.put((ImageButton) findViewById(R.id.export_example), com.wwci.bigdeal.io.output.ProductView.class);
		m_actions.put((ImageButton) findViewById(R.id.export_purchase), com.wwci.bigdeal.io.output.PurchaseView.class);
		m_actions.put((ImageButton) findViewById(R.id.export_sell), com.wwci.bigdeal.io.output.SalesView.class);
		
		m_actions.put((ImageButton) findViewById(R.id.clear_data), com.wwci.bigdeal.activity.ClearDataActivity.class);
		
		for (final ImageButton btn : m_actions.keySet()) {
			btn.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					Intent intent = new Intent();
					Class<?> cls = m_actions.get(v);
					if (cls != null) {
						intent.setClass(ManagementActivity.this, cls);
						switch (v.getId()){
                        case R.id.export_example:
                        	intent.putExtra("type", "example");
                        	break;
                        case R.id.export_csv_btn:
                        	intent.putExtra("type", "product");
                        	break;
                        }
						startActivity(intent);
					}
				}
			});
		}
	}

}
