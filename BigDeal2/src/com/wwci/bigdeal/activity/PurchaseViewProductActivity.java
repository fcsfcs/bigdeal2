package com.wwci.bigdeal.activity;

import java.util.ArrayList;
import java.util.HashMap;

import com.wwci.bigdeal.R;
import com.wwci.bigdeal.db.DbAdapter;
import com.wwci.bigdeal.uitl.StringTools;
import com.wwci.bigdeal.BigDealApplication;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class PurchaseViewProductActivity extends Activity {
	private TextView TV_ProductID;
	private TextView TV_ProductPath;
	
	private EditText EV_ProductName;
	private EditText EV_ProductDesc;
	private EditText EV_ProductUnit;
	private EditText EV_ProductCost;
	private EditText EV_ProductQty;
	private EditText EV_ProductSubtotal;
	private EditText TV_received_date;
	
	private ImageView IV_ProductPic;	
	private Button btn_viewList;

	private DbAdapter mDbHelper;
	protected Cursor cursor;
	private Bitmap resizedBitmap;
	
	protected ArrayAdapter<String> adapter;
		
	protected int position;
	protected ArrayList<HashMap<String, String>> m_orders;
	protected HashMap<String, String> productHash;
	
	private Intent i;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
		setContentView(R.layout.purchase_view_product);
	    mDbHelper = new DbAdapter(this);
	 	mDbHelper.open();
		
		initUI();
		setListener();
		
	}
	
	
    @Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		showProduct();
	}


	private void initUI(){    	
		TV_received_date = (EditText) findViewById(R.id.product_received_date);		
		
    	TV_ProductID = (TextView) findViewById(R.id.product_id);
    	TV_ProductPath = (TextView) findViewById(R.id.product_path);
    	
    	EV_ProductName = (EditText) findViewById(R.id.product_name);
    	EV_ProductDesc = (EditText) findViewById(R.id.product_desc);
    	EV_ProductUnit = (EditText) findViewById(R.id.product_unit);
    	EV_ProductCost = (EditText) findViewById(R.id.product_cost);
    	EV_ProductQty = (EditText) findViewById(R.id.product_qty);
    	EV_ProductSubtotal = (EditText) findViewById(R.id.product_subtotal);
    	
    	IV_ProductPic = (ImageView) findViewById(R.id.imageViewObj);
    	
    	btn_viewList = (Button) findViewById(R.id.viewList);  
    	
    }
    
    private void setListener(){   
    	   	
    	btn_viewList.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				i = new Intent(PurchaseViewProductActivity.this, PurchaseViewOrderActivity.class);
				startActivity(i);
				finish();
			}
		});    	
    	
    }
    
    public void showProduct(){
    	if (((BigDealApplication)getApplication()).getStatus()==1){
    		position = ((BigDealApplication)getApplication()).getPosition();
    		if (position>=0 && position<((BigDealApplication)getApplication()).getList().size()){
    			productHash = new HashMap<String, String>();
    			productHash = ((BigDealApplication)getApplication()).getList().get(position);
    			TV_ProductID.setText(productHash.get("product_id"));
    			TV_ProductPath.setText(productHash.get("product_path"));
    			EV_ProductDesc.setText(productHash.get("product_desc"));
    			EV_ProductUnit.setText(productHash.get("product_unit"));
    			EV_ProductCost.setText(productHash.get("product_cost"));
    			EV_ProductQty.setText(productHash.get("product_qty"));
    			EV_ProductSubtotal.setText(productHash.get("product_subtotal"));
    			EV_ProductName.setText(productHash.get("product_name"));
    			TV_received_date.setText(productHash.get("product_receivedDate"));    		
    		}
    	}else{
    		cleanAll();
    	}
    	
    }
    
  
    private void cleanAll(){ 
        EV_ProductName.setText("");
        TV_ProductID.setText("");
		TV_ProductPath.setText("");
		EV_ProductDesc.setText("");
		EV_ProductUnit.setText("");
		EV_ProductCost.setText("1.0");
		EV_ProductQty.setText("1");
		TV_received_date.setText("");
		
		imgView();
    	
    }
   
	private void imgView(){
		if (!StringTools.isNullOrBlank(TV_ProductPath.getText().toString())){
			if(null!=resizedBitmap&&!resizedBitmap.isRecycled())
				 resizedBitmap.recycle(); 

			 BitmapFactory.Options opts = new BitmapFactory.Options();
			     // For example, inSampleSize == 4, returns an image that is 1/4 the width/height of the original, and 1/16 the number of pixels
			 opts.inSampleSize =4;
			 resizedBitmap = BitmapFactory.decodeFile(TV_ProductPath.getText().toString(), opts);    
	        
			 IV_ProductPic.setImageBitmap(resizedBitmap); 	
		}else{
			IV_ProductPic.setImageBitmap(null); 	
		}		         
	}
	
	
	
	@Override
	protected void onDestroy() {
		mDbHelper.close();
		super.onDestroy();
	}
	
    
}
