package com.wwci.bigdeal.activity;


import java.io.File;

import com.wwci.bigdeal.R;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;

public class ProductPicViewActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_full_size_photo);
		Bundle extras = getIntent().getExtras(); 
		ImageView picture = (ImageView)findViewById(R.id.imageview);
		File imgFile = new File( extras.getString("path"));
		if(imgFile.exists()){
		    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
		    picture.setImageBitmap(myBitmap);
		}
	}

}
