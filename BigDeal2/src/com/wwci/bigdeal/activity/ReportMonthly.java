package com.wwci.bigdeal.activity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import com.wwci.bigdeal.BigDealApplication;
import com.wwci.bigdeal.R;
import com.wwci.bigdeal.db.DbAdapter;
import com.wwci.bigdeal.uitl.StringTools;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class ReportMonthly extends ListActivity {
	
	protected TextView orderdate_from; // mm/dd/yyyy
	protected TextView orderdate_to; //mm/dd/yyyy	
	
	protected Button btn_search;
	
	private ListView listView;
	
	private RadioGroup group1;
	private RadioGroup group2;
	
	private TableLayout summarypanel;
	private TextView tv_total;
	private TextView tv_amt;
	private TextView tv_canceled;
	private TextView tv_completed;
	
	private DbAdapter mDbHelper;
	protected Cursor cursor;
	protected List<HashMap<String, String>> productsList;
	protected ArrayList<HashMap<String, String>> products;
	protected HashMap<String, String> productHash;
	
	
    private int mYear;
    private int mMonth;
    private int mDay;
    
    private int iTotal;
    private double amt;
    private int icanceled;
    private int icompleted;
    
    private Intent i;
 
    static final int DATE_DIALOG_ID0 = 0;
    static final int DATE_DIALOG_ID1 = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.report_monthly);
		
        mDbHelper = new DbAdapter(this);
 	    mDbHelper.open();
 	    	    
 	    initUI();
		setUIValue();
		setListener();
	}
	
	private void initUI(){

		orderdate_from = (TextView) findViewById(R.id.from);
		orderdate_to = (TextView) findViewById(R.id.to);
		btn_search = (Button) findViewById(R.id.search);
	    summarypanel = (TableLayout) findViewById(R.id.summarypanel);
		tv_total = (TextView) findViewById(R.id.total);
		tv_amt = (TextView) findViewById(R.id.amt);
		tv_canceled = (TextView) findViewById(R.id.canceled);
		tv_completed = (TextView) findViewById(R.id.completed);
		
	    group1 = (RadioGroup) findViewById(R.id.group1);
	    group2 = (RadioGroup) findViewById(R.id.group2);
	    
	    listView = (ListView) findViewById(android.R.id.list);
	    	    		    
	}
	
	private void setUIValue(){
		  //  get the current date  
        final Calendar c = Calendar.getInstance();     
        mYear = c.get(Calendar.YEAR);     
        mMonth = c.get(Calendar.MONTH);     
        mDay = c.get(Calendar.DAY_OF_MONTH);  
        
        // display the current date (this method is below) as order date
        orderdate_from.setText(StringTools.toConvertDateFormat(new StringBuilder()
    	.append(mYear).append("-")
    	.append(mMonth-1).append("-")
        .append(mDay).toString(),"yyyy-MM-dd","MM/dd/yyyy"));
        
        orderdate_to.setText(StringTools.toConvertDateFormat(new StringBuilder()
    	.append(mYear).append("-")
    	.append(mMonth + 1).append("-")
        .append(mDay).toString(),"yyyy-MM-dd","MM/dd/yyyy"));
        
	}
	
	private void setListener(){
		
		btn_search.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				loadData();	
				summarypanel.setVisibility(View.VISIBLE);
			}
		});		
		
		orderdate_from.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 showDialog(DATE_DIALOG_ID0);
			}
		});
		
		orderdate_to.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 showDialog(DATE_DIALOG_ID1);
			}
		});
		
	}
	
	private void loadData(){
		StringBuffer where = new StringBuffer();
		
		switch (group1.getCheckedRadioButtonId()){
		case R.id.purchase:
			switch (group2.getCheckedRadioButtonId()){
			case R.id.poDate:
				where.append(DbAdapter.purchaseHeader.PURCHASEDATE).append(">='").append(StringTools.toConvertDateFormat(orderdate_from.getText().toString(), "MM/dd/yyyy","yyyy-MM-dd")).append("' and ");
				where.append(DbAdapter.purchaseHeader.PURCHASEDATE).append("<='").append(StringTools.toConvertDateFormat(orderdate_to.getText().toString(), "MM/dd/yyyy","yyyy-MM-dd")).append("' ");
				break;
			case R.id.dueDate:
				where.append(DbAdapter.purchaseHeader.DUEDATE).append(">='").append(StringTools.toConvertDateFormat(orderdate_from.getText().toString(), "MM/dd/yyyy","yyyy-MM-dd")).append("' and ");
				where.append(DbAdapter.purchaseHeader.DUEDATE).append("<='").append(StringTools.toConvertDateFormat(orderdate_to.getText().toString(), "MM/dd/yyyy","yyyy-MM-dd")).append("' ");
				break;
			}
			cursor = mDbHelper.fetchPurchases(where.toString());
			break;
		case R.id.order:
			switch (group2.getCheckedRadioButtonId()){
			case R.id.poDate:
				where.append(DbAdapter.orderHeader.PURCHASEDATE).append(">='").append(StringTools.toConvertDateFormat(orderdate_from.getText().toString(), "MM/dd/yyyy","yyyy-MM-dd")).append("' and ");
				where.append(DbAdapter.orderHeader.PURCHASEDATE).append("<='").append(StringTools.toConvertDateFormat(orderdate_to.getText().toString(), "MM/dd/yyyy","yyyy-MM-dd")).append("' ");
				break;
			case R.id.dueDate:
				where.append(DbAdapter.orderHeader.DUEDATE).append(">='").append(StringTools.toConvertDateFormat(orderdate_from.getText().toString(), "MM/dd/yyyy","yyyy-MM-dd")).append("' and ");
				where.append(DbAdapter.orderHeader.DUEDATE).append("<='").append(StringTools.toConvertDateFormat(orderdate_to.getText().toString(), "MM/dd/yyyy","yyyy-MM-dd")).append("' ");
				break;
			}
			cursor = mDbHelper.fetchOrders(where.toString());
			break;
		}
		
        productsList = new ArrayList<HashMap<String, String>>();
	    startManagingCursor(cursor);
	    iTotal=0;
	    amt=0.0;
        icanceled=0;
	    icompleted=0;
	    if (cursor!=null && cursor.getCount()>0){
	    	cursor.moveToFirst(); 
	    	for (int i=0; i<cursor.getCount(); i++){
	    		productHash = new HashMap<String,String>();
	    		productHash.put("id",cursor.getString(cursor.getColumnIndex(DbAdapter.purchaseHeader.PURCHASEID)));
	    		productHash.put("no",cursor.getString(cursor.getColumnIndex(DbAdapter.purchaseHeader.PURCHASENO)));
	    		productHash.put("orderDate",cursor.getString(cursor.getColumnIndex(DbAdapter.purchaseHeader.PURCHASEDATE)));
	    		productHash.put("dueDate",cursor.getString(cursor.getColumnIndex(DbAdapter.purchaseHeader.DUEDATE)));
	    		productHash.put("amount",cursor.getString(cursor.getColumnIndex(DbAdapter.purchaseHeader.AMOUNT)));
	    		productHash.put("paid",cursor.getString(cursor.getColumnIndex(DbAdapter.purchaseHeader.PAID)));
	    		productHash.put("client",cursor.getString(cursor.getColumnIndex(DbAdapter.purchaseHeader.CLIENT)));
	    		productHash.put("clientphone",cursor.getString(cursor.getColumnIndex(DbAdapter.purchaseHeader.CLIENTPHONE)));
	    		productHash.put("status",cursor.getString(cursor.getColumnIndex(DbAdapter.purchaseHeader.STATUS)));
	    		
	    		if (productHash.get("status").equals("Canceled")){
	    			icanceled= icanceled+1;
	    		}else{
	    			amt = amt+Double.parseDouble(productHash.get("amount").replaceAll(",", ""));
	    		}
	    		
	    		if (productHash.get("status").equals("Completed")){
	    			icompleted= icompleted+1;
	    		}
	    		
	    		
	    		
	    		productsList.add(productHash);
				cursor.moveToNext();
	    	}
	    	iTotal = cursor.getCount() - icanceled;
	    }
	    
	    listView.setOnItemClickListener(new OnItemClickListener() {
			public void onNothingSelected(AdapterView<?> arg0) {}
			public void onItemClick(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
				productHash =productsList.get((int)arg3);
				((BigDealApplication)getApplication()).setPurchase(productHash);
				loadInvovice();
				switch (group1.getCheckedRadioButtonId()){
				case R.id.purchase:
				     i = new Intent(ReportMonthly.this, PurchaseViewOrderActivity.class);
				     break;
				case R.id.order:
					i = new Intent(ReportMonthly.this, OrderViewActivity.class);
					break;
				}
				startActivity(i);	
			}});
	    		
	    updateListView();    	   
	}
	
    private void loadInvovice(){   
    	switch (group1.getCheckedRadioButtonId()){
		case R.id.purchase:
			cursor = mDbHelper.fetchPurchaseProduct(productHash.get("no"));
		     break;
		case R.id.order:
			cursor = mDbHelper.fetchOrderProduct(productHash.get("no"));
			break;
    	}
			
	    startManagingCursor(cursor);
		  
		if (cursor!=null && cursor.getCount()>0){
			HashMap<String, String> productItem;
			products = new ArrayList<HashMap<String, String>>();
			cursor.moveToFirst(); 
			for (int i=0; i<cursor.getCount(); i++){
				productItem = new HashMap<String, String>();
			    productItem.put("product_id",cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTID)));
			    productItem.put("product_path",cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTPIC)));
			    productItem.put("product_desc",cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTDESC)));
			    productItem.put("product_unit",cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTUNIT)));
			    productItem.put("product_cost",cursor.getString(cursor.getColumnIndex(DbAdapter.purchaseProduct.PRICE)));
			    productItem.put("product_qty",cursor.getString(cursor.getColumnIndex(DbAdapter.purchaseProduct.QTY)));
			    productItem.put("product_subtotal",cursor.getString(cursor.getColumnIndex(DbAdapter.purchaseProduct.SUBTOTAL)));
			    productItem.put("product_name",cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTNAME)));			  
			    productItem.put("product_receivedDate",cursor.getString(cursor.getColumnIndex(DbAdapter.purchaseProduct.RECEIVEDATE)));
			    System.out.println("load product id:"+productItem.get("product_id"));
			    products.add(productItem);
			    cursor.moveToNext();
			}
			    	
			((BigDealApplication)getApplication()).setList(products);			
	    }
	}  
    
	
    private void updateListView(){
    	MyListAdapterTwoLine adapter = new MyListAdapterTwoLine(this);
	    setListAdapter(adapter); 	   
	    tv_total.setText(String.valueOf(iTotal));
	    tv_amt.setText(String.valueOf(amt));
	    tv_completed.setText(String.valueOf(icompleted));
	    tv_canceled.setText(String.valueOf(icanceled));
    }	
	
	private class MyListAdapterTwoLine extends BaseAdapter {  
     	 
        private LayoutInflater inflater;  
        private HashMap<String,String> productHash;
    	
    	public MyListAdapterTwoLine(Context context) {  
            inflater = LayoutInflater.from(context);  
        }  
      
        @Override  
        public int getCount() {  
            return productsList.size();  
        }  
      
        @Override  
        public Object getItem(int position) {  
            return productsList.get(position);  
        }  
      
        @Override  
        public long getItemId(int position) {  
            return position;  
        }  
      
      
        @Override  
        public View getView(final int position, View convertView, ViewGroup parent) {  
        	 convertView = inflater.inflate(R.layout.purchase_list_item, null); 
             productHash = new HashMap<String, String>();
     		productHash = productsList.get(position);
     		TableLayout row = (TableLayout)convertView.findViewById(R.id.row); 
     		if (productHash.get("status").equals("Canceled"))
     		   row.setBackgroundResource(R.color.row_gary);
     		else if (productHash.get("status").equals("Completed"))
     			row.setBackgroundResource(R.color.white);
     		else{
     			if (position%2==0)
     				row.setBackgroundResource(R.color.row_lemonchiffon);
     			else
     				row.setBackgroundResource(R.color.row_honeydew);
     		}
     			

        	TextView purchaseID = (TextView)convertView.findViewById(R.id.purchaseID); 
        	TextView purchaseNo= (TextView)convertView.findViewById(R.id.no); 
        	TextView status= (TextView)convertView.findViewById(R.id.status); 
        	TextView client= (TextView)convertView.findViewById(R.id.client); 
        	TextView purchaseDate= (TextView)convertView.findViewById(R.id.purchaseDate); 
        	TextView dueDate= (TextView)convertView.findViewById(R.id.dueDate);        	    
        	    
        	purchaseID.setText(productHash.get("id"));
        	purchaseNo.setText(productHash.get("no"));
        	status.setText(productHash.get("status"));		
        	client.setText(productHash.get("client")+" ("+productHash.get("clientphone")+")");
        	purchaseDate.setText(productHash.get("orderDate"));
        	dueDate.setText(productHash.get("dueDate"));  
        	    
        	return convertView;  
        }     
    }
	
	  // updates the date in the TextView
    private void updateFromDateView() {
    	orderdate_from.setText(StringTools.toConvertDateFormat(new StringBuilder()
    	.append(mYear).append("-")
    	.append(mMonth + 1).append("-")
        .append(mDay).toString(),"yyyy-MM-dd","MM/dd/yyyy"));
    }
	  // updates the date in the TextView
    private void updateToDateView() { 	
    	orderdate_to.setText(StringTools.toConvertDateFormat(new StringBuilder()
    	.append(mYear).append("-")
    	.append(mMonth + 1).append("-")
        .append(mDay).toString(),"yyyy-MM-dd","MM/dd/yyyy"));
    }  
	 @Override
	protected Dialog onCreateDialog(int id) {
		 switch (id) {
		 case DATE_DIALOG_ID0:
		      return new DatePickerDialog(this,fromDateSetListener,mYear, mMonth, mDay);        	              
	     case DATE_DIALOG_ID1:  
	          return new DatePickerDialog(this,toDateSetListener,mYear, mMonth, mDay);    		         
		 }
		 return null;
	}	
    
	// the callback received when the user "sets" the date in the dialog
    private DatePickerDialog.OnDateSetListener fromDateSetListener = new DatePickerDialog.OnDateSetListener() {
    	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
    		mYear = year;
            mMonth = monthOfYear;
            mDay = dayOfMonth;
            updateFromDateView();
	    }
    };   
	// the callback received when the user "sets" the date in the dialog
    private DatePickerDialog.OnDateSetListener toDateSetListener = new DatePickerDialog.OnDateSetListener() {
    	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
    		mYear = year;
            mMonth = monthOfYear;
            mDay = dayOfMonth;
            updateToDateView();
        }
    };  
    
	@Override
	protected void onDestroy() {
		mDbHelper.close();
		super.onDestroy();
	}

}
