package com.wwci.bigdeal.activity;

import java.util.ArrayList;
import java.util.HashMap;

import com.wwci.bigdeal.R;
import com.wwci.bigdeal.db.DbAdapter;
import com.wwci.bigdeal.uitl.Constants;
import com.wwci.bigdeal.uitl.StringTools;
import com.wwci.bigdeal.uitl.ToastUtil;
import com.wwci.bigdeal.BigDealApplication;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class PurchaseEditProductActivity extends Activity {
	private TextView TV_ProductID;
	private TextView TV_ProductPath;
	
	private EditText EV_ProductName;
	private EditText EV_ProductDesc;
	private EditText EV_ProductUnit;
	private EditText EV_ProductCost;
	private EditText EV_ProductQty;
	private EditText EV_ProductSubtotal;
	private EditText TV_received_date;
	
	private ImageView IV_ProductPic;	
	private Spinner Spr_ProductName;
	private Button btn_addToList;
	private Button btn_viewList;
	private Button btn_removeFromList;
	
	private DbAdapter mDbHelper;
	protected Cursor cursor;
	private Bitmap resizedBitmap;
	
	private StringBuffer where;
	protected ArrayAdapter<String> adapter;
	private ArrayList<String> loadDataArray;
		
	protected int position;
	protected ArrayList<HashMap<String, String>> m_orders;
	protected HashMap<String, String> productHash;
	
	private Intent i;
	
	private int mYear;
	private int mMonth;
	private int mDay;
	 
    static final int DATE_DIALOG_ID0 = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
		setContentView(R.layout.purchase_edit_product);
	    mDbHelper = new DbAdapter(this);
	 	mDbHelper.open();
		
		initUI();
		setUIValue();
		setListener();
		
	}
	
	
    @Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		showProduct();
	}


	private void initUI(){    	
		TV_received_date = (EditText) findViewById(R.id.product_received_date);		
		
    	TV_ProductID = (TextView) findViewById(R.id.product_id);
    	TV_ProductPath = (TextView) findViewById(R.id.product_path);
    	
    	EV_ProductName = (EditText) findViewById(R.id.product_name);
    	EV_ProductDesc = (EditText) findViewById(R.id.product_desc);
    	EV_ProductUnit = (EditText) findViewById(R.id.product_unit);
    	EV_ProductCost = (EditText) findViewById(R.id.product_cost);
    	EV_ProductQty = (EditText) findViewById(R.id.product_qty);
    	EV_ProductSubtotal = (EditText) findViewById(R.id.product_subtotal);
    	
    	Spr_ProductName = (Spinner) findViewById(R.id.spr_product_name);
    	IV_ProductPic = (ImageView) findViewById(R.id.imageViewObj);
    	
    	btn_addToList = (Button) findViewById(R.id.addToList);
    	btn_viewList = (Button) findViewById(R.id.viewList);  
    	btn_removeFromList = (Button) findViewById(R.id.removeFromList);
    	
    }
    
    private void setUIValue(){    	
      	where= new StringBuffer(" where ");
    	where.append(DbAdapter.Mst_Product.STATUS).append("=1");
    	where.append(" order by "+DbAdapter.Mst_Product.PRODUCTNAME);
    	cursor = mDbHelper.fetchAllData(DbAdapter.Mst_Product.TABLE_NAME,where.toString());
    	startManagingCursor(cursor);
    	loadDataArray = new ArrayList<String>();
    	loadDataArray.add(Constants.WARNING_PRODUCT);
    	if (cursor!=null && cursor.getCount()>0){
    	  	for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
               	loadDataArray.add(cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTNAME)));            	
            }
    	        	
    	}

    	adapter = new ArrayAdapter<String>(this,R.layout.spinner_textview,loadDataArray);
    	adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	Spr_ProductName.setAdapter(adapter);
    	Spr_ProductName.setSelection(0);    	
	    
	    if (((BigDealApplication)getApplication()).getStatus()==1)
	    	btn_addToList.setText(R.string.save_changes);
	    else
	    	btn_addToList.setText(R.string.add_to_list);
    	
    }
    private void setListener(){   
    	Spr_ProductName.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				if (cursor!=null && cursor.getCount()>0 && !((String)arg0.getSelectedItem()).equals(Constants.WARNING_PRODUCT)){
					cursor.moveToPosition(arg2-1);
					EV_ProductName.setText((String)Spr_ProductName.getSelectedItem());
					TV_ProductID.setText(cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTID)));
					TV_ProductPath.setText(cursor.getString(cursor.getColumnIndex((DbAdapter.Mst_Product.PRODUCTPIC))));
					EV_ProductDesc.setText(cursor.getString(cursor.getColumnIndex((DbAdapter.Mst_Product.PRODUCTDESC))));
					EV_ProductUnit.setText(cursor.getString(cursor.getColumnIndex((DbAdapter.Mst_Product.PRODUCTUNIT))));
					EV_ProductCost.setText(cursor.getString(cursor.getColumnIndex((DbAdapter.Mst_Product.PRODUCTCOST))));
					EV_ProductQty.setText("1");
					TV_received_date.setText("");
					System.out.println(cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTID))+":"+(String)Spr_ProductName.getSelectedItem());
					calculateSubtotal();
					if (!TV_ProductPath.getText().toString().equals(""))
						imgView();							
				}
				 
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
    		
    	});    	 
    	    	
    	TV_received_date.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 showDialog(DATE_DIALOG_ID0);
			}
		});
    	
    	btn_removeFromList.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (((BigDealApplication)getApplication()).getList().size()>0){
					((BigDealApplication)getApplication()).getList().remove(((BigDealApplication)getApplication()).getPosition());
					if (((BigDealApplication)getApplication()).getList().size()>0){
						((BigDealApplication)getApplication()).setPosition(0);
				    	((BigDealApplication)getApplication()).setStatus(1);
						showProduct();
					}else{
						cleanAll();
				    	((BigDealApplication)getApplication()).setStatus(0);
					}
				}
				
			}
		});
    	
    	btn_addToList.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (vaildate()){
					addToList();
				}
				
			}
		});
    	
    	btn_viewList.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				i = new Intent(PurchaseEditProductActivity.this, PurchaseEditOrderActivity.class);
				startActivity(i);
				finish();
			}
		});
    	
    	EV_ProductCost.addTextChangedListener(new TextWatcher(){

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				String temp = s.toString();
		        int posDot = temp.indexOf(".");
		        if (posDot > 0) {
		        	if (temp.length() - posDot - 1 > 2)
		        		s.delete(posDot + 3, posDot + 4);
		        }
		        calculateSubtotal();
				
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}
    		
    	});
    	
    	EV_ProductQty.addTextChangedListener(new TextWatcher(){

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				calculateSubtotal();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}
    		
    	});
    	
    	
    	
    }
    
    public void showProduct(){
    	if (((BigDealApplication)getApplication()).getStatus()==1){
    		position = ((BigDealApplication)getApplication()).getPosition();
    		if (position>=0 && position<((BigDealApplication)getApplication()).getList().size()){
    			productHash = new HashMap<String, String>();
    			productHash = ((BigDealApplication)getApplication()).getList().get(position);
    			TV_ProductID.setText(productHash.get("product_id"));
    			TV_ProductPath.setText(productHash.get("product_path"));
    			EV_ProductDesc.setText(productHash.get("product_desc"));
    			EV_ProductUnit.setText(productHash.get("product_unit"));
    			EV_ProductCost.setText(productHash.get("product_cost"));
    			EV_ProductQty.setText(productHash.get("product_qty"));
    			EV_ProductSubtotal.setText(productHash.get("product_subtotal"));
    			EV_ProductName.setText(productHash.get("product_name"));
    			TV_received_date.setText(productHash.get("product_receivedDate"));
    			
    			if (StringTools.isNullOrBlank(productHash.get("product_receivedDate"))){
    				btn_addToList.setEnabled(true);
    				btn_removeFromList.setEnabled(true);
    				
    			}else{
    				btn_addToList.setEnabled(false);
    				btn_removeFromList.setEnabled(false);
    				
    				
    			}
    		}
    	}else{
    		cleanAll();
    	}
    	
    }
    
    private void addToList(){
    	productHash = new HashMap<String, String>();
    	System.out.print(TV_ProductID.getText().toString().trim());
    	productHash.put("product_id",TV_ProductID.getText().toString().trim());
    	productHash.put("product_path",TV_ProductPath.getText().toString().trim());
    	productHash.put("product_desc",EV_ProductDesc.getText().toString().trim());
    	productHash.put("product_unit",EV_ProductUnit.getText().toString().trim());
    	productHash.put("product_cost",EV_ProductCost.getText().toString().trim());
    	productHash.put("product_qty",EV_ProductQty.getText().toString().trim());
    	productHash.put("product_subtotal",EV_ProductSubtotal.getText().toString().trim());
    	productHash.put("product_name",EV_ProductName.getText().toString());    	
    	productHash.put("product_receivedDate",TV_received_date.getText().toString());

    	
//    	cursor = (Cursor)Spr_ProductName.getSelectedItem();
//    	productHash.put("product_name",cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTNAME)));
    	if (((BigDealApplication)getApplication()).getStatus()==0){
			((BigDealApplication)getApplication()).setPosition(-1);
			if(vaildateDuplicateProduct()) {
        		((BigDealApplication)getApplication()).getList().add(productHash);
				((BigDealApplication)getApplication()).setPosition(((BigDealApplication)getApplication()).getList().size()-1);				
				cleanAll();
				ToastUtil.getCustomToast(this,getString(R.string.info_sucess));
				
        	}
    	}else{
    		if(vaildateDuplicateProduct()) {
    			((BigDealApplication)getApplication()).getList().set(((BigDealApplication)getApplication()).getPosition(), productHash);
    			((BigDealApplication)getApplication()).setStatus(0);
    			cleanAll();
    			ToastUtil.getCustomToast(this,getString(R.string.info_sucess));
    		}
    	}
    	
    }
    private void cleanAll(){
        Spr_ProductName.setSelection(0);	 
        EV_ProductName.setText("");
        TV_ProductID.setText("");
		TV_ProductPath.setText("");
		EV_ProductDesc.setText("");
		EV_ProductUnit.setText("");
		EV_ProductCost.setText("1.0");
		EV_ProductQty.setText("1");
		TV_received_date.setText("");
		
		imgView();
    	
    }
    private boolean vaildateDuplicateProduct(){
    	m_orders = ((BigDealApplication)getApplication()).getList();
    	if (m_orders.size()>0){
    		HashMap<String, String> productitem = new HashMap<String, String>();
    		for (int i=0; i<m_orders.size();i++){
    			if (i!=((BigDealApplication)getApplication()).getPosition()){
    				productitem = m_orders.get(i);
        			if (TV_ProductID.getText().toString().equals(productitem.get("product_id"))){
        				ToastUtil.getCustomToast(this,getString(R.string.warning_duplicate_product));        				
        				return false;
        			}
    			}
    		}
    	}
    	
    	return true;
    }    

    private boolean vaildate(){
    	if (StringTools.isNullOrBlank(EV_ProductName.getText().toString())){
    		ToastUtil.getCustomToast(this, getResources().getString(R.string.warning_empty_product_name)+" " +getResources().getString(R.string.product_name));
    		return false;
    	}
    	
    	try{
    		Double.parseDouble(EV_ProductCost.getText().toString());
    	}catch(NumberFormatException e){
    		ToastUtil.getCustomToast(this, getResources().getString(R.string.product_cost)+" " +getResources().getString(R.string.warning_invaild_number));
    		return false;
    	}    	    	

      	if (StringTools.isInvaildValue(EV_ProductQty.getText().toString())){
    		ToastUtil.getCustomToast(this, getResources().getString(R.string.product_qty)+" " +getResources().getString(R.string.warning_invaild_qty));
    		return false;
    	}       	

    	
    	return true;
    	
    }
    
    private void calculateSubtotal(){    	    	
    	if (StringTools.isInvaildValue(EV_ProductQty.getText().toString())){
    		ToastUtil.getCustomToast(this, getResources().getString(R.string.product_qty)+" " +getResources().getString(R.string.warning_invaild_qty));
    	}else{
    		try{
    			double value = Double.parseDouble(EV_ProductCost.getText().toString().trim());
    			int qty = Integer.parseInt(EV_ProductQty.getText().toString().trim());
    			EV_ProductSubtotal.setText(String.valueOf(value*qty));    			
    		}catch(Exception e){
    			ToastUtil.getCustomToast(this, getResources().getString(R.string.product_cost)+" " + getResources().getString(R.string.warning_invaild_number));
    			
    		}
    		
    	}
    }
    
    
	private void imgView(){
		if (!StringTools.isNullOrBlank(TV_ProductPath.getText().toString())){
			if(null!=resizedBitmap&&!resizedBitmap.isRecycled())
				 resizedBitmap.recycle(); 

			 BitmapFactory.Options opts = new BitmapFactory.Options();
			     // For example, inSampleSize == 4, returns an image that is 1/4 the width/height of the original, and 1/16 the number of pixels
			 opts.inSampleSize =4;
			 resizedBitmap = BitmapFactory.decodeFile(TV_ProductPath.getText().toString(), opts);    
	        
			 IV_ProductPic.setImageBitmap(resizedBitmap); 	
		}else{
			IV_ProductPic.setImageBitmap(null); 	
		}		         
	}
	
	  // updates the date in the TextView
    private void updateFromDateView() {
    	TV_received_date.setText(StringTools.toConvertDateFormat(new StringBuilder()
    	.append(mYear).append("-")
    	.append(mMonth + 1).append("-")
        .append(mDay).toString(),"yyyy-MM-dd","MM/dd/yyyy"));
    }
 
	 @Override
	protected Dialog onCreateDialog(int id) {
		 switch (id) {
		 case DATE_DIALOG_ID0:
		      return new DatePickerDialog(this,fromDateSetListener,mYear, mMonth, mDay);        	              
	     }
		 return null;
	}	
    
	// the callback received when the user "sets" the date in the dialog
    private DatePickerDialog.OnDateSetListener fromDateSetListener = new DatePickerDialog.OnDateSetListener() {
    	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
    		mYear = year;
            mMonth = monthOfYear;
            mDay = dayOfMonth;
            updateFromDateView();
	    }
    };   
	
	
	@Override
	protected void onDestroy() {
		mDbHelper.close();
		super.onDestroy();
	}
	
    
}
