
package com.wwci.bigdeal.activity;
import java.util.ArrayList;

import com.wwci.bigdeal.R;
import com.wwci.bigdeal.db.DbAdapter;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;



public class SettingActivity extends Activity {
	public ArrayList<CharSequence> loadTextArray = new ArrayList<CharSequence>();
	private ArrayAdapter<CharSequence> adapter;
	
	private String selected_currency;	
	private Spinner spinner_currency; 	
	private Button btn_save;
		
	private DbAdapter mDbHelper;
	private String currency;
	
	private SharedPreferences settings;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		settings = getSharedPreferences("Preference", 0);
		currency = settings.getString("currency", "");
		setContentView(R.layout.settings);
		mDbHelper = new DbAdapter(this);
	    mDbHelper.open();
	    
	    initUI();
	    setUIValue();
	    setListener();	  	    
	}
	private void initUI(){

		btn_save = (Button) findViewById(R.id.save);
		
		spinner_currency = (Spinner) findViewById(R.id.currency); 
	}
	private void setUIValue(){
		
		loadTextArray.add("ARS");
		loadTextArray.add("AUD");
		loadTextArray.add("BRL");
		loadTextArray.add("CAD");
		loadTextArray.add("CHF");
		loadTextArray.add("CNY");
		loadTextArray.add("EUR");
		loadTextArray.add("GBP");
		   
		loadTextArray.add("HKD");
		loadTextArray.add("IDR");
		loadTextArray.add("INR");
		loadTextArray.add("JPY");
		loadTextArray.add("KRW");
		loadTextArray.add("MXN");
		loadTextArray.add("MYR");
		loadTextArray.add("NZD");
		loadTextArray.add("PHP");
		   
		loadTextArray.add("SEK");
		loadTextArray.add("SGD");
		loadTextArray.add("THB");
		loadTextArray.add("TWD");
		loadTextArray.add("USD");
		loadTextArray.add("VND");
		int selectedIndex =0;
	    for (int i=0;i<loadTextArray.size();i++){
	       if (loadTextArray.get(i).equals(currency)){
	          selectedIndex=i;
	    	  break;
	       }
	    }
	   adapter = new ArrayAdapter<CharSequence>(SettingActivity.this, R.layout.spinner_textview, loadTextArray);	        
	   adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);	          
	   spinner_currency.setAdapter(adapter); 
	   spinner_currency.setSelection(selectedIndex);
		
	}
	private void setListener(){
		spinner_currency.setOnItemSelectedListener(new OnItemSelectedListener(){
	            public void onItemSelected(AdapterView parent, View v, int position, long id) { 
	            	selected_currency = parent.getItemAtPosition(position).toString();		                	
	            }

	            public void onNothingSelected(AdapterView arg0) {}
	        });        
	    
	    btn_save.setOnClickListener(new Button.OnClickListener() {
	            public void onClick(View v) {
	            	settings.edit().putString("currency",selected_currency).commit();
	            	Intent i = new Intent(SettingActivity.this,TabView.class);
					i.putExtra("activateTab", "0");
			    	startActivity(i);
	                finish();
	            }
	        });  
	      
	  
	}

	protected void onDestroy() {
		mDbHelper.close();
		super.onDestroy();
	}	
	
}
