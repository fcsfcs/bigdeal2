package com.wwci.bigdeal.activity;

import com.wwci.bigdeal.R;
import com.wwci.bigdeal.db.DbAdapter;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TabHost;

public class TabView extends TabActivity {
	 private int activateTab;
	 private DbAdapter mDbHelper;
	     @Override  
     public void onCreate(Bundle savedInstanceState) {  
         super.onCreate(savedInstanceState);
         
//         requestWindowFeature(Window.FEATURE_NO_TITLE); 
         setContentView(R.layout.tabview); 
         
         activateTab=0;
         Bundle extras = getIntent().getExtras();
         if(extras !=null)
         {
            activateTab = Integer.valueOf(extras.getString("activateTab"));
         }
         Intent tab1 = new Intent(this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
         Intent tab2 = new Intent(this, PurchaseActivity.class);
         Intent tab3 = new Intent(this, OrderActivity.class);
         Intent tab4 = new Intent(this, ReportActivity.class);
         Intent tab5 = new Intent(this, ProductActivity.class);


         TabHost host = getTabHost();  
         host.addTab(host.newTabSpec("one").setIndicator(getResources().getString(R.string.tab_home), getResources().getDrawable(R.drawable.on_home)).setContent(tab1));
         host.addTab(host.newTabSpec("two").setIndicator(getResources().getString(R.string.tab_purchase), getResources().getDrawable(R.drawable.on_goods)).setContent(tab2));  
         host.addTab(host.newTabSpec("three").setIndicator(getResources().getString(R.string.tab_sales), getResources().getDrawable(R.drawable.on_shipping_car)).setContent(tab3));
         host.addTab(host.newTabSpec("four").setIndicator(getResources().getString(R.string.tab_report), getResources().getDrawable(R.drawable.on_report)).setContent(tab4));
         host.addTab(host.newTabSpec("five").setIndicator(getResources().getString(R.string.tab_product), getResources().getDrawable(R.drawable.on_product)).setContent(tab5));

         host.setCurrentTab(activateTab);


     }  
	     @Override
		 	public boolean onCreateOptionsMenu(Menu menu) {
		 		MenuInflater inflater = getMenuInflater();
		 		inflater.inflate(R.menu.homemenu, menu);
		 		return true;
		 	}

		     public boolean onOptionsItemSelected(MenuItem item){
		     	Intent intent = new Intent();
		     	switch(item.getItemId()){
		     	case R.id.menu_about:
		     		intent.setClass(TabView.this, AboutActivity.class);
		     		startActivity(intent);
		     		return true;
		     	case R.id.menu_help:
		     		intent.setClass(TabView.this, HelpActivity.class);
		     		startActivity(intent);
		     		return true;
		         case R.id.menu_setting:
		         	intent.setClass(TabView.this, SettingActivity.class);
		         	startActivity(intent);
		         	return true;
		         case R.id.menu_management:
		     		intent.setClass(TabView.this, ManagementActivity.class);
		     		intent.putExtra("currentTab", 1);
		     		startActivity(intent);
		     		return true;		       
		       	}
		     	return(super.onOptionsItemSelected(item));
		     }     
		     
 }
