package com.wwci.bigdeal.activity;


import java.util.ArrayList;

import java.util.Collections;

import java.util.HashMap;


import com.wwci.bigdeal.R;
import com.wwci.bigdeal.db.DbAdapter;
import com.wwci.bigdeal.uitl.OrderAdapter;
import com.wwci.bigdeal.uitl.StringTools;
import com.wwci.bigdeal.BigDealApplication;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class OrderViewActivity extends ListActivity {	
	private TextView TV_lbl_Total;
	private TextView TV_Total;
	private TextView TV_Client;
	private TextView TV_ClientPhone;
	private TextView TV_Date;
	private TextView TV_DueDate;
	private TextView TV_InvoiceNo;
	private TextView TV_InvoiceStatus;
	private TextView ET_Prepayment;
	
	private Button btn_Back;
	
	
	protected ArrayList<HashMap<String, String>> m_orders;
	private OrderAdapter m_adapter;

	private DbAdapter mDbHelper;
	protected Cursor cursor;
	private Intent i;
	protected HashMap<String, String> productHash;
	
    private SharedPreferences settings;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
        setContentView(R.layout.purchase_view_order);
        mDbHelper = new DbAdapter(this);
        mDbHelper.open();
        
//        productHash = (HashMap<String, String>)this.getIntent().getSerializableExtra("item");  
        
        initUI();
        setUIValue();
        setListener();
        
                 
    }
    
    private void initUI(){
    	TV_InvoiceNo = (TextView) findViewById(R.id.no);
    	TV_InvoiceNo.setVisibility(View.VISIBLE);
    	TV_InvoiceStatus = (TextView) findViewById(R.id.status);
    	
    	TV_lbl_Total = (TextView) findViewById(R.id.lbl_total);
    	TV_Total = (TextView) findViewById(R.id.total);
    	TV_Client = (TextView) findViewById(R.id.client);
    	TV_ClientPhone = (TextView) findViewById(R.id.clientPhone);
    	TV_Date = (TextView) findViewById(R.id.purchasedate);
    	TV_DueDate = (TextView) findViewById(R.id.duedate);
    	ET_Prepayment = (TextView) findViewById(R.id.prepayment);
    	
    	btn_Back = (Button) findViewById(R.id.back);
    	    	
    }
   
    private void setUIValue(){    	
    	//get currency symbol
    	settings = getSharedPreferences("Preference", 0);
    	if (StringTools.isNullOrBlank(settings.getString("currency", "")))
    		TV_lbl_Total.setText(getString(R.string.total)+"("+settings.getString("currency", "")+")");
    	
        productHash = (HashMap<String, String>)((BigDealApplication)getApplication()).getPurchase();        
        TV_InvoiceNo.setText(productHash.get("no"));
    	TV_Total.setText(StringTools.toFormatDouble(productHash.get("amount")));
    	TV_Client.setText(productHash.get("client"));
    	TV_ClientPhone.setText(productHash.get("clientphone"));
        TV_Date.setText(productHash.get("orderDate"));
        TV_DueDate.setText(productHash.get("dueDate"));        
        TV_InvoiceStatus.setText(productHash.get("status"));
        ET_Prepayment.setText(productHash.get("paid"));
        
		m_orders = (ArrayList<HashMap<String, String>>)((BigDealApplication)getApplication()).getList().clone();
		double total =0.0;
		if (!m_orders.isEmpty()){
			Collections.reverse(m_orders);			
			HashMap<String, String> productitem = new HashMap<String, String>();
			for (int i=0;i<m_orders.size();i++){
				productitem= m_orders.get(i);
				total+=Double.parseDouble(productitem.get("product_subtotal"));
						
			}
			this.m_adapter = new OrderAdapter(this, R.layout.purchase_order_list_item, m_orders);
	        setListAdapter(this.m_adapter);
		}
		
		TV_Total.setText(StringTools.toFormatDouble(total+""));
    }
    private void setListener(){
    	
    	
    	btn_Back.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				i = new Intent(OrderViewActivity.this,TabView.class);
//				i.putExtra("activateTab", "2");
//		    	startActivity(i);
		    	finish();
			}
		});
    	
    	
    }
    @Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);			
		((BigDealApplication)getApplication()).setPosition(m_orders.size()-position-1);
    	((BigDealApplication)getApplication()).setStatus(1);
    	i = new Intent(OrderViewActivity.this,OrderProductViewActivity.class);
    	startActivity(i);
    	finish();
    	
	}
    
	 
	@Override
	protected void onDestroy() {
		mDbHelper.close();
		super.onDestroy();
	}

}



