package com.wwci.bigdeal.activity;


import com.wwci.bigdeal.R;
import com.wwci.bigdeal.db.DbAdapter;
import com.wwci.bigdeal.uitl.ToastUtil;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class ClearDataActivity extends Activity{
	
	private Button btn_clear;
	private Button btn_cancel;
	private CheckBox chk_clear_purchase;
	private CheckBox chk_clear_sales;
	private CheckBox chk_clear_product;
	
	private DbAdapter mDbHelper;
	
//	private TextView tv_help;
	protected void onCreate(Bundle savedInstanceState){
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.custom_dialog);
		mDbHelper = new DbAdapter(this);
        mDbHelper.open();
        
		initUI();	
		setListener();
	}
    private void initUI(){
    	btn_clear = (Button) findViewById(R.id.save);		            		             
        btn_cancel = (Button) findViewById(R.id.cancel);
        
        chk_clear_purchase = (CheckBox) findViewById(R.id.clear_purchase);
        chk_clear_sales = (CheckBox) findViewById(R.id.clear_sales);
        chk_clear_product= (CheckBox) findViewById(R.id.clear_product);
    }
    
    private void setListener(){
    	chk_clear_product.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (chk_clear_product.isChecked()){
	            	 chk_clear_purchase.setChecked(true);
	            	 chk_clear_sales.setChecked(true);
	            }
			}
    		
    	});
    	
    	btn_clear.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub	
				if (!chk_clear_purchase.isChecked() && !chk_clear_sales.isChecked() && chk_clear_product.isChecked()){
					ToastUtil.getCustomToast(ClearDataActivity.this, getString(R.string.warning_clear_data));					
				}else if (mDbHelper.clearData(chk_clear_purchase.isChecked(),chk_clear_sales.isChecked(),chk_clear_product.isChecked())){
 	       			ToastUtil.getCustomToast(ClearDataActivity.this, getString(R.string.success)); 
	    	        finish();	    	       			
 	       		}
			}
        	 
         });
         btn_cancel.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
        	 
         });
    	
    }
    
	 @Override
	 protected void onDestroy() {
		mDbHelper.close();
		super.onDestroy();
	 }

}
