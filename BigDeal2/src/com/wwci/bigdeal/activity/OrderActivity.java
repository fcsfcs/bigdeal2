package com.wwci.bigdeal.activity;

import com.wwci.bigdeal.BigDealApplication;
import com.wwci.bigdeal.R;
import com.wwci.bigdeal.db.DbAdapter;
import com.wwci.bigdeal.uitl.ToastUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;

public class OrderActivity extends ListActivity {	

	protected EditText search_word;

	protected Button btn_search;
	protected Button btn_new;
	protected Button btn_view_order;
	protected Button btn_showSearchPanel;
	
	protected CheckBox chk_inprogress;
	protected CheckBox chk_received;
	protected CheckBox chk_paid;
	protected CheckBox chk_canceled;
	protected CheckBox chk_completed;
	
	private TableLayout searchPanel;

	private DbAdapter mDbHelper;
	protected Cursor cursor;

	protected Intent i;
	
	private StringBuffer where;
	protected List<HashMap<String, String>> productsList;
	protected ArrayList<HashMap<String, String>> products;
	protected HashMap<String, String> productHash;
	protected Object mActionMode;
	
	public int selectedItem = -1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	     setContentView(R.layout.purchase);    
	     mDbHelper = new DbAdapter(this);
	 	 mDbHelper.open();
	 		
	 	 initUI();
	     setUIValue();
	 	 setListener();
	 	 loadPurchase();
	 	 registerForContextMenu(getListView());
	}

	protected void onResume(){
		super.onRestart();
		setUIValue();
		loadPurchase();
	}	
	protected void onRestart(){
		super.onRestart();
		setUIValue();
		loadPurchase();
	}
	private void initUI(){	    
    	
	    search_word = (EditText) findViewById(R.id.search_word);
	    
	    btn_search = (Button) findViewById(R.id.search);
	    btn_new = (Button) findViewById(R.id.create);
	    btn_view_order = (Button)findViewById(R.id.viewList);
	    btn_showSearchPanel  = (Button)findViewById(R.id.showSearchPanel);
	    
	    chk_canceled = (CheckBox) findViewById(R.id.cancelled);
	    chk_completed = (CheckBox) findViewById(R.id.completed);	    
	    chk_inprogress  = (CheckBox) findViewById(R.id.inprogress);
		
	    chk_received  = (CheckBox) findViewById(R.id.received);
		chk_received.setText(R.string.delivered);
		
		chk_paid  = (CheckBox) findViewById(R.id.paid);
		chk_paid.setText(R.string.invoiced);
	    
	    searchPanel = (TableLayout) findViewById(R.id.searchpanel);
	}
	
	private void setUIValue(){
		btn_view_order.setText(getString(R.string.veiw_bill)+"("+((BigDealApplication)getApplication()).getList().size()+")");
                      
	}
	
	private void setListener(){
		btn_showSearchPanel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				searchPanel.setVisibility(View.VISIBLE);
			}
		});
		btn_search.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				searchPanel.setVisibility(View.GONE);
				loadPurchase();				
			}
		});
		
		btn_new.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((BigDealApplication)getApplication()).setList(new ArrayList<HashMap<String, String>>());
				((BigDealApplication)getApplication()).setPosition(0);
				((BigDealApplication)getApplication()).setStatus(0);
				i = new Intent(OrderActivity.this,OrderProductCreateActivity.class);
				startActivityForResult(i, 0);
			}
		});
		
		btn_view_order.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (((BigDealApplication)getApplication()).getList().size()>0){
					i = new Intent(OrderActivity.this,OrderCreateActivity.class);
					startActivityForResult(i, 0);					
				}else{
					ToastUtil.getCustomToast(OrderActivity.this,getString(R.string.warning_empty_list));					
				}
				
				
			}
		});

	}

	private void loadPurchase(){
    	where = new StringBuffer();
    	Boolean status_checked = false;
    	where.append(" 1=1 ");
    	if (!search_word.getText().toString().equals("")){	    		
    		where.append(" and (").append(DbAdapter.orderHeader.PURCHASENO).append(" = '").append(search_word.getText().toString().trim()).append("'").append(" COLLATE NOCASE ");
    		where.append(" or ").append(DbAdapter.orderHeader.CLIENT).append(" = '").append(search_word.getText().toString().trim()).append("'").append(" COLLATE NOCASE ");    		
    		where.append(")");
    	}
    	
    	where.append(" and (");
    	
    	if (chk_inprogress.isChecked()){	        	
         	where.append(DbAdapter.orderHeader.STATUS).append("='In Progress'");
         	status_checked = true;
    	}
    	
    	if (chk_received.isChecked()){
    		if (status_checked)
				where.append( " or ");
         	where.append(DbAdapter.orderHeader.STATUS).append("='Delivered'"); 
         	status_checked = true;
    	}	        	
         	
    	if (chk_paid.isChecked()){
    		if (status_checked)
				where.append( " or ");
         	where.append(DbAdapter.orderHeader.STATUS).append("='Invoiced'"); 
         	status_checked = true;
    	}
        if (chk_canceled.isChecked()){
    		if (status_checked)
				where.append( " or ");
         	where.append(DbAdapter.orderHeader.STATUS).append("='Canceled'"); 
         	status_checked = true;
    	}	        	
        	
        if (chk_completed.isChecked()){
    		if (status_checked)
				where.append( " or ");
         	where.append(DbAdapter.orderHeader.STATUS).append("='Completed'"); 
         	status_checked = true;
    	}		
        
        if (!status_checked)
			where.append(DbAdapter.orderHeader.STATUS).append("='NONE'");
        
        where.append(") ");
        Log.i("where string", where.toString());
        cursor = mDbHelper.fetchOrders(where.toString());
        
        productsList = new ArrayList<HashMap<String, String>>();
	    startManagingCursor(cursor);
	    if (cursor!=null && cursor.getCount()>0){
	    	cursor.moveToFirst(); 
	    	for (int i=0; i<cursor.getCount(); i++){
	    		productHash = new HashMap<String,String>();
	    		productHash.put("id",cursor.getString(cursor.getColumnIndex(DbAdapter.orderHeader.PURCHASEID)));
	    		productHash.put("no",cursor.getString(cursor.getColumnIndex(DbAdapter.orderHeader.PURCHASENO)));
	    		productHash.put("orderDate",cursor.getString(cursor.getColumnIndex(DbAdapter.orderHeader.PURCHASEDATE)));
	    		productHash.put("dueDate",cursor.getString(cursor.getColumnIndex(DbAdapter.orderHeader.DUEDATE)));
	    		productHash.put("amount",cursor.getString(cursor.getColumnIndex(DbAdapter.orderHeader.AMOUNT)));
	    		productHash.put("paid",cursor.getString(cursor.getColumnIndex(DbAdapter.orderHeader.PAID)));
	    		productHash.put("client",cursor.getString(cursor.getColumnIndex(DbAdapter.orderHeader.CLIENT)));
	    		productHash.put("clientphone",cursor.getString(cursor.getColumnIndex(DbAdapter.orderHeader.CLIENTPHONE)));
	    		productHash.put("status",cursor.getString(cursor.getColumnIndex(DbAdapter.orderHeader.STATUS)));
	    		
	    		productsList.add(productHash);
				cursor.moveToNext();
	    	}
	    }
	    		
	    updateListView(cursor);    	   
    }
    private void updateListView(Cursor cursor){
    	MyListAdapterTwoLine adapter = new MyListAdapterTwoLine(this);
	    setListAdapter(adapter); 	    
//	    if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
//	    	getListView().setOnItemLongClickListener(new OnItemLongClickListener() {
//				@Override
//				public boolean onItemLongClick(AdapterView<?> parent, View view,
//						int position, long id) {
//
//					if (mActionMode != null) {
//						return false;
//					}
//					selectedItem = position;
//					// Start the CAB using the ActionMode.Callback defined above
//					mActionMode = OrderActivity.this.startActionMode(mActionModeCallback);
//					view.setSelected(true);
//					return true;
//				}
//			});
//	      }
//	      else {
//	        getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
//	        registerForContextMenu(getListView());
//	      }

    }	
    
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		l.setItemChecked(position, true);
	} 
	
	
	  @Override
	public void onCreateContextMenu(ContextMenu menu, View v,ContextMenu.ContextMenuInfo menuInfo) {
		  AdapterView.AdapterContextMenuInfo info;
          try {
               info = (AdapterView.AdapterContextMenuInfo) menuInfo;
          } catch (ClassCastException e) {
              //Log.e(TAG, "bad menuInfo", e);
              return;
          }	                   
          selectedItem = info.position;

		  Menu m_menu = menu;
		  m_menu.clear();
		  productHash =productsList.get(selectedItem);
			
		  if (productHash.get("status").equals("In Progress")){
				m_menu.add(0, Menu.FIRST+1, 0, R.string.edit);  
				m_menu.add(0, Menu.FIRST+2, 0, R.string.cancel);
				if (loadNotYetReceivedProduct())
					m_menu.add(0, Menu.FIRST+3, 0, R.string.deliver);
				if (Double.parseDouble(productHash.get("amount"))-Double.parseDouble(productHash.get("paid"))>0)
					m_menu.add(0, Menu.FIRST+4, 0, R.string.collect_payment);
			}
			if (productHash.get("status").equals("Delivered")){
//				m_menu.add(0, Menu.FIRST+1, 0, R.string.edit);					
				if (Double.parseDouble(productHash.get("amount"))-Double.parseDouble(productHash.get("paid"))>0)
					m_menu.add(0, Menu.FIRST+4, 0, R.string.collect_payment);
				m_menu.add(0, Menu.FIRST+6, 0, R.string.view);
			}
			//received meoney
			if (productHash.get("status").equals("Invoiced")){
				m_menu.add(0, Menu.FIRST+1, 0, R.string.edit);  				
				if (loadNotYetReceivedProduct())
					m_menu.add(0, Menu.FIRST+3, 0, R.string.deliver);
			}
			
			if (productHash.get("status").equals("Canceled")){
				m_menu.add(0, Menu.FIRST+5, 0, R.string.resume);  
				m_menu.add(0, Menu.FIRST+6, 0, R.string.view);										
			}
			
			if (productHash.get("status").equals("Completed")){
				m_menu.add(0, Menu.FIRST+6, 0, R.string.view);	  										
			}
	}
	  
	  @Override
	public boolean onContextItemSelected(MenuItem item) {
	    boolean result=performActions(item);

	    if (!result) {
	      result=super.onContextItemSelected(item);
	    }
	    
	    return(result);
	}
	  @SuppressWarnings("unchecked")
	  public boolean performActions(MenuItem item) {
		productHash =productsList.get(selectedItem);
		switch (item.getItemId()) {
		case Menu.FIRST+1: //edit	
			((BigDealApplication)getApplication()).setPurchase(productHash);
			loadInvovice();										
			i = new Intent(OrderActivity.this, OrderEditActivity.class);
		  	startActivity(i);											       
			return true;
			
		case Menu.FIRST+2: // cancel
			cancelAlert();
			return true;			
		case Menu.FIRST+3: // delivered
			i = new Intent(OrderActivity.this, OrderDeliverActivity.class);
		    i.putExtra("no", productHash.get("no"));
		    i.putExtra("status", productHash.get("status"));
	  	    startActivity(i);			  	    
			return true;
		case Menu.FIRST+4: // settle
			LayoutInflater factory=LayoutInflater.from(OrderActivity.this);
			final View v1=factory.inflate(R.layout.purchase_pay,null);
			AlertDialog.Builder dialog=new AlertDialog.Builder(OrderActivity.this);
			dialog.setTitle(getResources().getString(R.string.collect_payment));
			dialog.setView(v1);
			final TextView TV_toPay = (TextView)v1.findViewById(R.id.lbl_toPay);
			final EditText EV_total = (EditText)v1.findViewById(R.id.total);
			final EditText EV_paid = (EditText)v1.findViewById(R.id.paid); 
			final EditText EV_topay = (EditText)v1.findViewById(R.id.toPay);			
			TV_toPay.setText(R.string.to_collect);
			EV_total.setText(productHash.get("amount"));
			EV_paid.setText(productHash.get("paid"));
			EV_topay.setText(String.valueOf(Double.parseDouble(productHash.get("amount"))-Double.parseDouble(productHash.get("paid"))));
	        dialog.setPositiveButton(getResources().getString(R.string.settle), new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int whichButton) {			            	
	            	if (EV_topay.getText().toString().trim().equals("") || EV_topay.getText().toString().trim().equals("0") ){
	            		ToastUtil.getCustomToast(OrderActivity.this, getResources().getString(R.string.warning_paid_null));
	            	}else if ((Double.parseDouble(EV_paid.getText().toString().trim())+Double.parseDouble(EV_topay.getText().toString().trim()))>Double.parseDouble(EV_total.getText().toString())){
	            		ToastUtil.getCustomToast(OrderActivity.this, getResources().getString(R.string.warning_paid_big_totalCost));
	            	}else{
	            		String status = productHash.get("status");
	            		Double paid = Double.parseDouble(EV_paid.getText().toString().trim())+Double.parseDouble(EV_topay.getText().toString().trim());
	            		if (paid==Double.parseDouble(EV_total.getText().toString())){
	            			if (status.equals("Delivered"))
	        		    		status = "Completed";
	        		    	else
	        		    		status="Invoiced";
	            		}
	    	            mDbHelper.updateSettleAmount(productHash.get("id"), paid, status);
	    	            loadPurchase();
	    	            ToastUtil.getCustomToast(OrderActivity.this, getResources().getString(R.string.info_sucess));
	            	}
	            	
	            }
	        });
	        dialog.setNegativeButton(getResources().getString(R.string.cancel),new DialogInterface.OnClickListener() {
	 

				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
	 
				}
			});
	        dialog.show();
	        
			return true;
		case Menu.FIRST+5: //resume
			resumePurchase();
			return true;
		case Menu.FIRST+6: //view
			((BigDealApplication)getApplication()).setPurchase(productHash);
			loadInvovice();										
		    i = new Intent(OrderActivity.this, PurchaseViewOrderActivity.class);
	  	    startActivity(i);	
			return true;					
		default:
			return false;
	    }

	  }	 
//	  private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
//
//			public boolean onCreateActionMode(ActionMode mode, Menu menu) {
//
//				Menu m_menu = menu;
//				productHash =productsList.get(selectedItem);
//				
//				if (productHash.get("status").equals("In Progress")){
//					m_menu.add(0, Menu.FIRST+1, 0, R.string.edit);  
//					m_menu.add(0, Menu.FIRST+2, 0, R.string.cancel);
//					if (loadNotYetReceivedProduct())
//						m_menu.add(0, Menu.FIRST+3, 0, R.string.receive);
//					if (Double.parseDouble(productHash.get("amount"))-Double.parseDouble(productHash.get("paid"))>0)
//						m_menu.add(0, Menu.FIRST+4, 0, R.string.pay);
//				}
//				if (productHash.get("status").equals("Received")){
//					m_menu.add(0, Menu.FIRST+1, 0, R.string.edit);					
//					if (Double.parseDouble(productHash.get("amount"))-Double.parseDouble(productHash.get("paid"))>0)
//						m_menu.add(0, Menu.FIRST+4, 0, R.string.pay);
//				}
//				
//				if (productHash.get("status").equals("Paid")){
//					m_menu.add(0, Menu.FIRST+1, 0, R.string.edit);  
//					if (loadNotYetReceivedProduct())
//						m_menu.add(0, Menu.FIRST+3, 0, R.string.receive);
//				}
//				
//				if (productHash.get("status").equals("Canceled")){
//					m_menu.add(0, Menu.FIRST+5, 0, R.string.resume);  
//					m_menu.add(0, Menu.FIRST+6, 0, R.string.view);										
//				}
//				
//				if (productHash.get("status").equals("Completed")){
//					m_menu.add(0, Menu.FIRST+6, 0, R.string.view);	  										
//				}
//				
//				
//				return true;
//			}
//
//
//			public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
//				return false; // Return false if nothing is done
//			}
//
//			// Called when the user selects a contextual menu item
//			public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
//				productHash =productsList.get(selectedItem);
//				switch (item.getItemId()) {
//				case Menu.FIRST+1: //edit				
//					loadInvovice();										
//					i = new Intent(OrderActivity.this, PurchaseEditOrderActivity.class);
//				  	i.putExtra("item", productHash);
//				  	startActivity(i);											       
//					mode.finish();
//					return true;
//					
//				case Menu.FIRST+2: // cancel
//					cancelAlert();
//					mode.finish();
//					return true;
//					
//				case Menu.FIRST+3: // received
//					i = new Intent(OrderActivity.this, PurchaseReceiveActivity.class);
//				    i.putExtra("no", productHash.get("no"));
//			  	    startActivity(i);			  	    
//					mode.finish();
//					return true;
//				case Menu.FIRST+4: // paid
//					LayoutInflater factory=LayoutInflater.from(OrderActivity.this);
//					final View v1=factory.inflate(R.layout.purchase_pay,null);
//					AlertDialog.Builder dialog=new AlertDialog.Builder(OrderActivity.this);
//					dialog.setTitle(getResources().getString(R.string.pay));
//					dialog.setView(v1);
//					final EditText EV_total = (EditText)v1.findViewById(R.id.total);
//					final EditText EV_paid = (EditText)v1.findViewById(R.id.paid); 
//					final EditText EV_topay = (EditText)v1.findViewById(R.id.toPay); 
//					EV_total.setText(productHash.get("amount"));
//					EV_paid.setText(productHash.get("paid"));
//					EV_topay.setText(String.valueOf(Double.parseDouble(productHash.get("amount"))-Double.parseDouble(productHash.get("paid"))));
//			        dialog.setPositiveButton(getResources().getString(R.string.save), new DialogInterface.OnClickListener() {
//			            public void onClick(DialogInterface dialog, int whichButton) {			            	
//			            	if (EV_topay.getText().toString().trim().equals("") || EV_topay.getText().toString().trim().equals("0") ){
//			            		Toast.makeText(OrderActivity.this, getResources().getString(R.string.warning_paid_null), Toast.LENGTH_SHORT).show();
//			            	}else if ((Double.parseDouble(EV_paid.getText().toString().trim())+Double.parseDouble(EV_topay.getText().toString().trim()))>Double.parseDouble(EV_total.getText().toString())){
//			            		Toast.makeText(OrderActivity.this, getResources().getString(R.string.warning_paid_big_totalCost), Toast.LENGTH_SHORT).show();
//			            	}else{
//			            		String status = productHash.get("status");
//			            		Double paid = Double.parseDouble(EV_paid.getText().toString().trim())+Double.parseDouble(EV_topay.getText().toString().trim());
//			            		if (paid==Double.parseDouble(EV_total.getText().toString()))
//			            			status = "Paid";
//			    	            mDbHelper.updatePaidAmount(productHash.get("id"), paid, status);
//			    	            loadPurchase();
//			    	            Toast.makeText(OrderActivity.this, getResources().getString(R.string.info_sucess), Toast.LENGTH_SHORT).show();
//			            	}
//			            	
//			            }
//			        });
//			        dialog.setNegativeButton(getResources().getString(R.string.close),new DialogInterface.OnClickListener() {
//			 
//
//						public void onClick(DialogInterface dialog, int which) {
//							// TODO Auto-generated method stub
//			 
//						}
//					});
//			        dialog.show();
//			        
//					mode.finish();
//					return true;
//				case Menu.FIRST+5: //resume
//					resumePurchase();
//					mode.finish();
//					return true;
//				case Menu.FIRST+6: //view
//					loadInvovice();										
//				    i = new Intent(OrderActivity.this, PurchaseViewOrderActivity.class);
//			  	    i.putExtra("item", productHash);
//			  	    startActivity(i);	
//					mode.finish();
//					return true;					
//				default:
//					return false;
//				}
//			}
//
//			// Called when the user exits the action mode
//			public void onDestroyActionMode(ActionMode mode) {
//				mActionMode = null;
//				selectedItem = -1;
//			}
//		};
	private void cancelAlert() {
		AlertDialog.Builder dialog=new AlertDialog.Builder(OrderActivity.this);
		dialog.setMessage("Are you sure to cancel?");
		dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				cancelPurchase(); 
		    }
		});
		dialog.setNegativeButton("No",new DialogInterface.OnClickListener() {		 
			public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
		 
			}
		});
		dialog.show();
	}
    private void cancelPurchase(){
    	mDbHelper.cancelResumeOrder(productHash.get("no"),"Canceled");
    	loadPurchase();		    	
    }
    private void resumePurchase(){
    	mDbHelper.cancelResumeOrder(productHash.get("no"),"In Progress");
    	loadPurchase();		    	
    }
    private boolean loadNotYetReceivedProduct(){
    	cursor = mDbHelper.fetchNotYetDeliverProduct(productHash.get("no"));
	    startManagingCursor(cursor);	    
		if (cursor!=null && cursor.getCount()>0){
			return true;
		}
		return false;
    }
    private void loadInvovice(){   
    	cursor = mDbHelper.fetchOrderProduct(productHash.get("no"));
	    startManagingCursor(cursor);
		  
		if (cursor!=null && cursor.getCount()>0){
			HashMap<String, String> productItem;
			products = new ArrayList<HashMap<String, String>>();
			cursor.moveToFirst(); 
			for (int i=0; i<cursor.getCount(); i++){
				productItem = new HashMap<String, String>();
			    productItem.put("product_id",cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTID)));
			    productItem.put("product_path",cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTPIC)));
			    productItem.put("product_desc",cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTDESC)));
			    productItem.put("product_unit",cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTUNIT)));
			    productItem.put("product_cost",cursor.getString(cursor.getColumnIndex(DbAdapter.purchaseProduct.PRICE)));
			    productItem.put("product_qty",cursor.getString(cursor.getColumnIndex(DbAdapter.purchaseProduct.QTY)));
			    productItem.put("product_subtotal",cursor.getString(cursor.getColumnIndex(DbAdapter.purchaseProduct.SUBTOTAL)));
			    productItem.put("product_name",cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTNAME)));			  
			    productItem.put("product_receivedDate",cursor.getString(cursor.getColumnIndex(DbAdapter.purchaseProduct.RECEIVEDATE)));
			        	
			    products.add(productItem);
			    cursor.moveToNext();
			}
			    	
			((BigDealApplication)getApplication()).setList(products);
			
	    }
	}  
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
	    	super.onActivityResult(requestCode, resultCode, data);
	    	switch(resultCode) {
	    	case RESULT_OK:
	    		loadPurchase();
	    	    break;
	    	}
	   
	 }
	
    private class MyListAdapterTwoLine extends BaseAdapter {  
      	 
        private LayoutInflater inflater;  
        private HashMap<String,String> productHash;
    	
    	public MyListAdapterTwoLine(Context context) {  
            inflater = LayoutInflater.from(context);  
        }  
      
        @Override  
        public int getCount() {  
            return productsList.size();  
        }  
      
        @Override  
        public Object getItem(int position) {  
            return productsList.get(position);  
        }  
      
        @Override  
        public long getItemId(int position) {  
            return position;  
        }  
      
      
        @Override  
        public View getView(final int position, View convertView, ViewGroup parent) {  
            convertView = inflater.inflate(R.layout.purchase_list_item, null); 
            productHash = new HashMap<String, String>();
    		productHash = productsList.get(position);
    		TableLayout row = (TableLayout)convertView.findViewById(R.id.row); 
    		if (productHash.get("status").equals("Canceled"))
    		   row.setBackgroundResource(R.color.row_gary);
    		else if (productHash.get("status").equals("Completed"))
    			row.setBackgroundResource(R.color.white);
    		else{
    			if (position%2==0)
    				row.setBackgroundResource(R.color.row_lemonchiffon);
    			else
    				row.setBackgroundResource(R.color.row_honeydew);
    		}
    			

       	    TextView purchaseID = (TextView)convertView.findViewById(R.id.purchaseID); 
       	    TextView purchaseNo= (TextView)convertView.findViewById(R.id.no); 
       	    TextView status= (TextView)convertView.findViewById(R.id.status); 
       	    TextView client= (TextView)convertView.findViewById(R.id.client); 
       	    TextView purchaseDate= (TextView)convertView.findViewById(R.id.purchaseDate); 
       	    TextView dueDate= (TextView)convertView.findViewById(R.id.dueDate);        	    
       	    
       	    purchaseID.setText(productHash.get("id"));
       	    purchaseNo.setText(productHash.get("no"));
       	    status.setText(productHash.get("status"));		
       	    client.setText(productHash.get("client")+" ("+productHash.get("clientphone")+")");
       	    purchaseDate.setText(productHash.get("orderDate"));
       	    dueDate.setText(productHash.get("dueDate"));

    	    return convertView;        
        }     
    }
	 @Override
	 protected void onDestroy() {
		mDbHelper.close();
		super.onDestroy();
	 }

	
}