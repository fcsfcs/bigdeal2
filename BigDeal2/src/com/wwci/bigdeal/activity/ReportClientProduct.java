package com.wwci.bigdeal.activity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import com.wwci.bigdeal.R;
import com.wwci.bigdeal.db.DbAdapter;
import com.wwci.bigdeal.uitl.StringTools;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class ReportClientProduct extends ListActivity {
	
	protected TextView orderdate_from; // mm/dd/yyyy
	protected TextView orderdate_to; //mm/dd/yyyy
	protected TextView TV_paidamt;
	protected TextView TV_amt;
	protected CheckBox chk_check;
	
	private DbAdapter mDbHelper;
	protected Cursor cursor;
	protected List<HashMap<String, String>> productsList;
	protected ArrayList<HashMap<String, String>> products;
	protected HashMap<String, String> productHash;
	
	
    private int mYear;
    private int mMonth;
    private int mDay;
 
    static final int DATE_DIALOG_ID0 = 0;
    static final int DATE_DIALOG_ID1 = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.report_supplier);
		
        mDbHelper = new DbAdapter(this);
 	    mDbHelper.open();
 	   
 	    
 	    initUI();
		setUIValue();
		setListener();
		loadData();
	}
	
	private void initUI(){
		chk_check = (CheckBox) findViewById(R.id.check);
		orderdate_from = (TextView) findViewById(R.id.from);
		orderdate_to = (TextView) findViewById(R.id.to);
		TV_amt  = (TextView) findViewById(R.id.amt);
		TV_amt.setVisibility(View.INVISIBLE);
		TV_paidamt  = (TextView) findViewById(R.id.paidamt);
		TV_paidamt.setText(R.string.report_totalAmt);
	}
	
	private void setUIValue(){
		  //  get the current date  
        final Calendar c = Calendar.getInstance();     
        mYear = c.get(Calendar.YEAR);     
        mMonth = c.get(Calendar.MONTH);     
        mDay = c.get(Calendar.DAY_OF_MONTH);  
        
        // display the current date (this method is below) as order date
        orderdate_from.setText(StringTools.toConvertDateFormat(new StringBuilder()
    	.append(mYear).append("-")
    	.append(mMonth - 1).append("-")
        .append(mDay).toString(),"yyyy-MM-dd","MM/dd/yyyy"));
        
        orderdate_to.setText(StringTools.toConvertDateFormat(new StringBuilder()
    	.append(mYear).append("-")
    	.append(mMonth + 1).append("-")
        .append(mDay).toString(),"yyyy-MM-dd","MM/dd/yyyy"));
        
	}
	
	private void setListener(){
		chk_check.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				 loadData();
			}
			
		});
		
		orderdate_from.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 showDialog(DATE_DIALOG_ID0);
			}
		});
		
		orderdate_to.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 showDialog(DATE_DIALOG_ID1);
			}
		});
		
	}
	
	private void loadData(){
		StringBuffer where = new StringBuffer();
		where.append("");
		if (chk_check.isChecked())
			where.append( " and b.dPurchaseDate>= '"+ StringTools.toConvertDateFormat(orderdate_from.getText().toString(), "MM/dd/yyyy","yyyy-MM-dd") +"' and b.dPurchaseDate<='"+ StringTools.toConvertDateFormat(orderdate_to.getText().toString(), "MM/dd/yyyy","yyyy-MM-dd")+"'");						

		cursor = mDbHelper.clientProductReport(where.toString());		
		productsList = new ArrayList<HashMap<String, String>>();
		
		startManagingCursor(cursor);
		if (cursor!=null && cursor.getCount()>0){
			cursor.moveToFirst(); 
//		    	headerrow.setVisibility(View.VISIBLE);
		    for (int i=0; i<cursor.getCount(); i++){
		    	productHash = new HashMap<String,String>();
		    	productHash.put("client",cursor.getString(cursor.getColumnIndex("client")));
		    	productHash.put("clientphone",cursor.getString(cursor.getColumnIndex("clientPhone")));
		    	productHash.put("product",cursor.getString(cursor.getColumnIndex("vName")));
		    	productHash.put("total",cursor.getString(cursor.getColumnIndex("qty")));
		    	productHash.put("amount",cursor.getString(cursor.getColumnIndex("amt")));
	    		
	    		productsList.add(productHash);
				cursor.moveToNext();
		    }
		}
		
		updateListView();   
	}
	
    private void updateListView(){
    	MyListAdapterTwoLine adapter = new MyListAdapterTwoLine(this);
	    setListAdapter(adapter); 	    
    }	
	
	private class MyListAdapterTwoLine extends BaseAdapter {  
     	 
        private LayoutInflater inflater;  
        private HashMap<String,String> productHash;
    	
    	public MyListAdapterTwoLine(Context context) {  
            inflater = LayoutInflater.from(context);  
        }  
      
        @Override  
        public int getCount() {  
            return productsList.size();  
        }  
      
        @Override  
        public Object getItem(int position) {  
            return productsList.get(position);  
        }  
      
        @Override  
        public long getItemId(int position) {  
            return position;  
        }  
      
      
        @Override  
        public View getView(final int position, View convertView, ViewGroup parent) {  
            convertView = inflater.inflate(R.layout.report_client_product_list_item, null); 
            productHash = new HashMap<String, String>();
    		productHash = productsList.get(position);
    		TableLayout row = (TableLayout)convertView.findViewById(R.id.row); 
    		if (position%2==0)
    			row.setBackgroundResource(R.color.row_lemonchiffon);
    		else
    			row.setBackgroundResource(R.color.row_honeydew);
        			
       	    TextView total= (TextView)convertView.findViewById(R.id.total); 
       	    TextView client= (TextView)convertView.findViewById(R.id.client); 
       	    TextView product= (TextView)convertView.findViewById(R.id.product); 
       	    TextView amt= (TextView)convertView.findViewById(R.id.amt);      	    
       	    
       	    		
       	    client.setText(productHash.get("client")+" ("+productHash.get("clientphone")+")");
       	    product.setText(productHash.get("product"));
       	    total.setText(productHash.get("total"));
       	    amt.setText(productHash.get("amount"));


    	    return convertView;        
        }     
    }
	
	  // updates the date in the TextView
    private void updateFromDateView() {
    	orderdate_from.setText(StringTools.toConvertDateFormat(new StringBuilder()
    	.append(mYear).append("-")
    	.append(mMonth + 1).append("-")
        .append(mDay).toString(),"yyyy-MM-dd","MM/dd/yyyy"));
    }
	  // updates the date in the TextView
    private void updateToDateView() { 	
    	orderdate_to.setText(StringTools.toConvertDateFormat(new StringBuilder()
    	.append(mYear).append("-")
    	.append(mMonth + 1).append("-")
        .append(mDay).toString(),"yyyy-MM-dd","MM/dd/yyyy"));
    }  
	 @Override
	protected Dialog onCreateDialog(int id) {
		 switch (id) {
		 case DATE_DIALOG_ID0:
		      return new DatePickerDialog(this,fromDateSetListener,mYear, mMonth, mDay);        	              
	     case DATE_DIALOG_ID1:  
	          return new DatePickerDialog(this,toDateSetListener,mYear, mMonth, mDay);    		         
		 }
		 return null;
	}	
    
	// the callback received when the user "sets" the date in the dialog
    private DatePickerDialog.OnDateSetListener fromDateSetListener = new DatePickerDialog.OnDateSetListener() {
    	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
    		mYear = year;
            mMonth = monthOfYear;
            mDay = dayOfMonth;
            updateFromDateView();
            loadData();
	    }
    };   
	// the callback received when the user "sets" the date in the dialog
    private DatePickerDialog.OnDateSetListener toDateSetListener = new DatePickerDialog.OnDateSetListener() {
    	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
    		mYear = year;
            mMonth = monthOfYear;
            mDay = dayOfMonth;
            updateToDateView();
            loadData();
        }
    };  
    
	@Override
	protected void onDestroy() {
		mDbHelper.close();
		super.onDestroy();
	}

}
