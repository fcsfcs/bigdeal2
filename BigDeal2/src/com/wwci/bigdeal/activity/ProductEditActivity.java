package com.wwci.bigdeal.activity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import com.wwci.bigdeal.R;
import com.wwci.bigdeal.db.DbAdapter;
import com.wwci.bigdeal.model.Product;
import com.wwci.bigdeal.uitl.Constants;
import com.wwci.bigdeal.uitl.FileUtil;
import com.wwci.bigdeal.uitl.SpannableStringBuilderUtil;
import com.wwci.bigdeal.uitl.StringTools;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;

import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class ProductEditActivity extends Activity {   
	protected ImageView show_pic;
	
	protected EditText product_id;
	protected EditText product_name;
	protected EditText product_desc;
	protected EditText product_cost;
	protected EditText product_price;
	protected EditText product_unit;
	protected EditText product_path;
	protected EditText product_tag;
	protected EditText product_status;
	
	private TextView tv_alltags;
	
	private Button btn_save;
	private Button btn_activeInactive;
	private Button btn_close;
	private Button btn_takePic;
	private Button btn_pickPic;
	private Button btn_clearPic;
	
	protected Uri imageCaptureUri;
	private Bitmap resizedBitmap;
	protected File photo;
	private static final int TAKE_PHOTO_CODE = 1;
	private Bundle datas;
	private DbAdapter mDbHelper;
	
	private String alltags;
	private ArrayList<String> loadDataArray;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.product_detail);;
//		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.provider_titlebar);  
        mDbHelper = new DbAdapter(this);
        mDbHelper.open();
	    datas = this.getIntent().getExtras();  	    
		initUI();
		setUIValue();	
		setListener();
	}
	

	private void initUI(){	    	    
		product_id = (EditText) findViewById(R.id.product_id);
		product_status = (EditText) findViewById(R.id.product_status);
    	product_name = (EditText) findViewById(R.id.product_name);
    	product_desc = (EditText) findViewById(R.id.product_desc);
    	product_price = (EditText) findViewById(R.id.product_price);
    	product_cost= (EditText) findViewById(R.id.product_cost);
    	product_unit = (EditText) findViewById(R.id.product_unit);    	
    	product_path = (EditText) findViewById(R.id.product_picPath);    	
    	product_tag = (EditText) findViewById(R.id.product_tag);  
    	tv_alltags = (TextView) findViewById(R.id.alltags);  
    	
    	show_pic = (ImageView) findViewById(R.id.imageViewObj);
    	
    	btn_save = (Button) findViewById(R.id.save);
    	btn_activeInactive = (Button) findViewById(R.id.button);
    	btn_close = (Button) findViewById(R.id.close);
    	
    	btn_takePic = (Button) findViewById(R.id.takepic);
    	btn_pickPic = (Button) findViewById(R.id.pickpic);
    	btn_clearPic = (Button) findViewById(R.id.clear);
    	
    	
    }
    private void setUIValue(){
    	alltags="";
    	loadDataArray = datas.getStringArrayList("all_tag");
    	for (String s : loadDataArray)
    		if (!s.equals(Constants.ALL))
    			alltags += s + ",";
    	
    	tv_alltags.setText(SpannableStringBuilderUtil.setClickspans(alltags,product_tag));   
    	MovementMethod m = tv_alltags.getMovementMethod();
		if ((m == null) || !(m instanceof LinkMovementMethod))
		{
			tv_alltags.setMovementMethod(LinkMovementMethod.getInstance());
		}

        product_id.setText(datas.getString("product_id"));
        product_status.setText(datas.getString("product_status"));
        product_name.setText(datas.getString("product_name"));
        product_desc.setText(datas.getString("product_desc"));
        product_cost.setText(datas.getString("product_cost"));
        product_price.setText(datas.getString("product_price"));
        product_unit.setText(datas.getString("product_unit"));
        product_path.setText(datas.getString("product_path"));
        product_tag.setText(datas.getString("product_tag"));
        	
        if (datas.getString("product_status").equals("1"))
        	btn_activeInactive.setText(R.string.inactive);
        else
        	btn_activeInactive.setText(R.string.active);

    	
		if (!StringTools.isNullOrBlank(product_path.getText().toString()))
			imgView();			
 	    
    }
	private void setListener(){
		
		btn_save.setOnClickListener(new Button.OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (vaildate())
					save();
				
			}
			
		});
		
		btn_activeInactive.setOnClickListener(new Button.OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
					update();
				
			}
			
		});
		
		btn_close.setOnClickListener(new Button.OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
					finish();
				
			}
			
		});

		btn_pickPic.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {            	
            	Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            	photoPickerIntent.setType("image/*");
                startActivityForResult( photoPickerIntent, 0 );
            }
         });
       
        
		btn_takePic.setOnClickListener(new Button.OnClickListener() {
        	public void onClick(View v) {
				// TODO Auto-generated method stub
			    try
			    {
			        // place where to store camera taken picture
			        photo = FileUtil.createTemporaryFile("picture", ".jpg");
			        photo.delete();
			    }
			    catch(Exception e)
			    {
			        Log.v("testing", "Can't create file to take picture!");
			    }
			    imageCaptureUri = Uri.fromFile(photo);

				Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
				cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageCaptureUri); 
				startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
			}
         });
              
           
        
		btn_clearPic.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
            	show_pic.setImageBitmap(null);
            }
        });   
	 
	 }
	private boolean vaildate(){
		boolean returnValue = true;
		
		if (StringTools.isInvaildValue(product_name.getText().toString())){
			returnValue=false;
			Toast.makeText(ProductEditActivity.this, getResources().getString(R.string.warning_invaild_field)+" " + R.string.product_name, Toast.LENGTH_SHORT).show();
		}
		if (StringTools.isInvaildValue(product_cost.getText().toString())){
			returnValue=false;
			Toast.makeText(ProductEditActivity.this, getResources().getString(R.string.warning_invaild_field)+" " + R.string.product_cost, Toast.LENGTH_SHORT).show();
		}
		if (StringTools.isInvaildValue(product_price.getText().toString())){
			returnValue=false;
			Toast.makeText(ProductEditActivity.this, getResources().getString(R.string.warning_invaild_field)+" " + R.string.product_price, Toast.LENGTH_SHORT).show();
		}
		if (!StringTools.isInvaildValue(product_tag.getText().toString())){
			String[] arr = product_tag.getText().toString().trim().split(",");
			if (arr.length>5){
				returnValue=false;
				Toast.makeText(ProductEditActivity.this, R.string.remark_tag, Toast.LENGTH_SHORT).show();
			}
		}
		
		return returnValue;
	}
	private void update(){
		if (datas.getString("product_status").equals("1"))
			mDbHelper.updateProduct(product_id.getText().toString(), "0");
		else
			mDbHelper.updateProduct(product_id.getText().toString(), "1");	
		finish();
	}
	private void save(){
		Product product = new Product();
		product.setId(product_id.getText().toString());
		product.setName(product_name.getText().toString());
		product.setDesc(product_desc.getText().toString());
		product.setSaleCost(product_cost.getText().toString());
		product.setSalePrice(product_price.getText().toString());
		product.setUnit(product_unit.getText().toString());
		product.setTag(product_tag.getText().toString());
		product.setPicPath(product_path.getText().toString());
		product.setStatus(product_status.getText().toString());
		
		String[] arr = product.getTag().split(",");
		for (String s:arr){
			if (!loadDataArray.contains(s)){
			    loadDataArray.add(s);
			    alltags += s + ",";
			}
		}
		
		if (!mDbHelper.checkProduct(product.getName(),product.getId())){
			mDbHelper.updateProduct(product,alltags);
			finish();
		}else
			Toast.makeText(this, R.string.warning_duplicate_product, Toast.LENGTH_LONG).show();
				
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (data != null)
			datas = data.getExtras();
    		switch(requestCode) {
        	case TAKE_PHOTO_CODE:    	
           		if ( resultCode == RESULT_OK ){
//            		   ContentResolver cr = getContentResolver();
            		   product_path.setText(imageCaptureUri.getPath());  
    				   imgView();            		 
                 }
        		break;
	    	case 0:
	    		if ( resultCode == RESULT_OK ){
	    			Uri uri = data.getData();
                    if( uri != null ){
                        // Form an array specifying which columns to return. 
                        String[] projection = new String[] {Images.Thumbnails.DATA};
                       // Make the query.   
    			        Cursor cur = managedQuery(uri,
    			    	   projection, // Which columns to return 
    					   null,   // WHERE clause; which rows to return (all rows)
    					   null,   // WHERE clause selection arguments (none)
    					   null    // Order-by clause (ascending by name)
    					 );
    			
    			        product_path.setText("");
    	     	 
    	     	         if (cur.moveToFirst()) {    	     		  
    	     		        int dataColumn = cur.getColumnIndex(Images.Media.DATA);
              	     	    //  Get the field values
    	     		       product_path.setText(cur.getString(dataColumn));
    	     	          }
    	     	          cur.close();   
    	     	          imgView();   	         
    	                  break;
                    }
                }
            }
        
	}
	
	private void imgView(){
		if (!StringTools.isNullOrBlank(product_path.getText().toString())){
			if(null!=resizedBitmap&&!resizedBitmap.isRecycled())
				 resizedBitmap.recycle(); 

			 BitmapFactory.Options opts = new BitmapFactory.Options();
			     // For example, inSampleSize == 4, returns an image that is 1/4 the width/height of the original, and 1/16 the number of pixels
			 opts.inSampleSize =4;
			 resizedBitmap = BitmapFactory.decodeFile(product_path.getText().toString(), opts);    
	        
			 show_pic.setImageBitmap(resizedBitmap); 	
		}else{
			show_pic.setImageBitmap(null); 	
		}
		         

	}
	

	 @Override
	 protected void onDestroy() {
		mDbHelper.close();
		super.onDestroy();
	 }
}
