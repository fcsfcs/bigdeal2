package com.wwci.bigdeal.activity;

import com.wwci.bigdeal.R;
import com.wwci.bigdeal.db.DbAdapter;

import com.wwci.bigdeal.uitl.StringTools;
import com.wwci.bigdeal.uitl.ToastUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;

import android.widget.TextView;

public class OrderDeliverActivity extends ListActivity {	
	protected Button btn_receive;
	protected Button btn_close;
	protected Button btn_receivedate;
	private ListView lView;
	private TextView TV_number;
	
	private DbAdapter mDbHelper;
	protected Cursor cursor;
	protected List<HashMap<String, String>> productsList;
	private MyListAdapterTwoLine adapter;
	private StringBuffer selectedItem = new StringBuffer();
	
	protected Intent i;
	private Bundle datas;
	
	private int mYear;
    private int mMonth;
    private int mDay;
    private boolean receiveAll = true;
 
    static final int DATE_DIALOG_ID = 0;  
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
        setContentView(R.layout.purchase_receive);
      
	    mDbHelper = new DbAdapter(this);
	    mDbHelper.open();
	    this.datas = this.getIntent().getExtras();  	
	 	initUI();
	    setUIValue();
	 	setListener();
	 	loadProduct();
	}
	
	protected void onRestart(){
		super.onRestart();
		setUIValue();
		loadProduct();
	}
	private void initUI(){	    		
		TV_number = (TextView) findViewById(R.id.number);
		btn_receivedate = (Button) findViewById(R.id.receivedate);
	    btn_receive = (Button) findViewById(R.id.receive);
	    btn_receive.setText(R.string.deliver);
	    btn_close = (Button) findViewById(R.id.close);
	}
	
	private void setUIValue(){
		
		TV_number.setText(datas.getString("no"));
		//listview
		lView = getListView();
		lView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		
		// get the current date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);	
        
        btn_receivedate.setText((mMonth+1) + "/"+mDay+"/"+mYear);
        // display the current date (this method is below) as order date
        updateDisplay();      
	}
	
	private void setListener(){
		btn_close.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();				
			}
		});
		btn_receive.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (vaildate()){
					updateReceiveDate();
					finish();
				}							
			}
		});
		
		btn_receivedate.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 showDialog(DATE_DIALOG_ID);
			}
		});
	}
	private boolean vaildate(){
		HashMap<Integer, Boolean> state =adapter.state;
		for(int j=0;j<state.size();j++){  			 
			if (state.get(j)) {
				if (!selectedItem.toString().equals(""))
					selectedItem.append(",");				
				selectedItem.append(productsList.get(j).get("purchase_product_id"));
			}else{
				if (receiveAll) receiveAll=false;
			}
		}
        if (selectedItem.toString().equals("")){
        	ToastUtil.getCustomToast(this, getString(R.string.warning_empty_selection));
        	return false;
        } 
                		
         return true;
	}
	  // updates the date in the TextView
    private void updateDisplay() {
    	btn_receivedate.setText(StringTools.toConvertDateFormat(new StringBuilder()
    	.append(mYear).append("-")
    	.append(mMonth + 1).append("-")
        .append(mDay).toString(),"yyyy-MM-dd","MM/dd/yyyy"));
    	
    }
    @Override
   	protected Dialog onCreateDialog(int id) {
   		 switch (id) {
   	     case DATE_DIALOG_ID:
   	         return new DatePickerDialog(this,mDateSetListener,mYear, mMonth, mDay);
   	     }
   	     return null;
   	}	 
   	 
   	// the callback received when the user "sets" the date in the dialog
   	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
   		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
   			mYear = year;
   	        mMonth = monthOfYear;
   	        mDay = dayOfMonth;
   	        updateDisplay();
   	    }
   	}; 
   	private void updateReceiveDate(){
   		mDbHelper.updateDeliverDate(StringTools.toConvertDateFormat(btn_receivedate.getText().toString(), "MM/dd/yyyy","yyyy-MM-dd"),selectedItem.toString(),receiveAll, datas.getString("status"),datas.getString("no"));
   	}

	private void loadProduct(){
        cursor = mDbHelper.fetchNotYetDeliverProduct(datas.getString("no"));
        startManagingCursor(cursor);
        productsList = new ArrayList<HashMap<String, String>>();    
      	if (cursor!=null && cursor.getCount()>0){
      		HashMap<String, String> productItem;			
      		cursor.moveToFirst(); 
      		for (int i=0; i<cursor.getCount(); i++){
      			productItem = new HashMap<String, String>();
      			productItem.put("purchase_product_id",cursor.getString(cursor.getColumnIndex(DbAdapter.orderProduct.PURCHASEPID)));
     		    productItem.put("product_path",cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTPIC)));
     		    productItem.put("product_desc",cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTDESC)));
     		    productItem.put("product_unit",cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTUNIT)));     			    
     		    productItem.put("product_qty",cursor.getString(cursor.getColumnIndex(DbAdapter.orderProduct.QTY)));     			    
     		    productItem.put("product_name",cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTNAME)));			  			   			        	
     		    productsList.add(productItem);
     		    cursor.moveToNext();
      		}
      	}
      	updateListView(cursor); 
    }
	
	private void updateListView(Cursor cursor){		
		adapter = new MyListAdapterTwoLine(this);
		lView.setAdapter(adapter); 
		
	}
	private class MyListAdapterTwoLine extends BaseAdapter {  
     	 
        private LayoutInflater inflater;  
        private HashMap<String,String> productHash;
        private HashMap<Integer, Boolean> state = new HashMap<Integer, Boolean>();
    	
    	public MyListAdapterTwoLine(Context context) {  
            inflater = LayoutInflater.from(context);  
            initData();
        }  
    	private void initData(){
    		for(int i=0; i<getCount();i++) {
    			state.put(i,false);
    	    }
    	}

        @Override  
        public int getCount() {  
            return productsList.size();  
        }  
      
        @Override  
        public Object getItem(int position) {  
            return productsList.get(position);  
        }  
      
        @Override  
        public long getItemId(int position) {  
            return position;  
        }  
      
      
        @SuppressWarnings("unchecked")
		@Override  
        public View getView(final int position, View convertView, ViewGroup parent) { 
        	View v = convertView;
    		if (v == null) {
    			v = inflater.inflate(R.layout.purchase_list_item_with_checkbox, null);
    		}
    		
//            convertView = inflater.inflate(R.layout.purchase_list_item_with_checkbox, null); 
            productHash = new HashMap<String, String>();
    		productHash =(HashMap<String, String>) getItem(position);
//    		ImageView image = (ImageView) v.findViewById(R.id.image);      		  
       	    TextView purchaseID = (TextView)v.findViewById(R.id.purchaseID); 
       	    TextView productName= (TextView)v.findViewById(R.id.name); 
       	    TextView productQty= (TextView)v.findViewById(R.id.qty); 
       	    TextView productDesc= (TextView)v.findViewById(R.id.desc);  
       	    CheckBox check = (CheckBox) v.findViewById(R.id.list_checkbox); 
       	           	    
//       	    image.setBackgroundResource((Integer) listData.get(position).get("friend_image"));
       	    purchaseID.setText(productHash.get("purchase_product_id"));
       	    productName.setText(productHash.get("product_name"));
       	    productDesc.setText(productHash.get("product_desc"));		
       	    productQty.setText(productHash.get("product_qty")+" "+productHash.get("product_unit"));
       	    
       	    check.setOnCheckedChangeListener(new OnCheckedChangeListener() {  
       	    	public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {  
       	    		// TODO Auto-generated method stub  
       		        if (isChecked) {
       		        	state.put(position, true);  
       		        } else {
       		        	state.put(position,false);  
       		        }  
       		     }  
       		});  
       	    
       	    return v;        
        }     
    }	
	 @Override
	 protected void onDestroy() {
		mDbHelper.close();
		super.onDestroy();
	 }

	
}