package com.wwci.bigdeal.activity;

import com.wwci.bigdeal.R;
import com.wwci.bigdeal.db.DbAdapter;
import com.wwci.bigdeal.uitl.Constants;
import com.wwci.bigdeal.uitl.MySimpleCursorAdapter;

import java.util.ArrayList;
import java.util.Arrays;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;

public class ProductActivity extends ListActivity {	
	protected Spinner spr_tag;
	protected Button btn_search;
	protected Button btn_new;
	protected EditText search_word;

	private RadioButton radio_active;
	
	private DbAdapter mDbHelper;
	protected Cursor cursor;
	protected ListAdapter products;
	protected ArrayAdapter<String> adapter;
	
	protected Intent i;
	
	private StringBuffer where;
	private ArrayList<String> loadDataArray;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
//		 requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
	     setContentView(R.layout.product);
//	     getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.product_title);        
	     mDbHelper = new DbAdapter(this);
	 	 mDbHelper.open();
	 		
	 	 initUI();
	     setUIValue();
	 	 setListener();
	 	 loadProduct();
	}
	
	protected void onRestart(){
		super.onRestart();
		setUIValue();
		loadProduct();
	}
	private void initUI(){	    
		spr_tag = (Spinner) findViewById(R.id.spr_tag);
    	
	    search_word = (EditText) findViewById(R.id.search_word);
	    btn_search = (Button) findViewById(R.id.search);
	    btn_new = (Button) findViewById(R.id.create);
	    
	    radio_active = (RadioButton) findViewById(R.id.active);
	    
	}
	
	private void setUIValue(){
	    //prepare tag
		where= new StringBuffer();
		where.append(" where vParamType='Application' and vParamCode='TAG'") ;
	    cursor = mDbHelper.fetchData(where.toString());
	    startManagingCursor(cursor);
	    loadDataArray = new ArrayList<String>();
	    loadDataArray.add(Constants.ALL);
	    if (cursor!=null && cursor.getCount()>0){
            cursor.moveToFirst();
	    	String[] arr = cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Parameter.PARAMVALUE)).split(",");	    	
		    Arrays.sort(arr,String.CASE_INSENSITIVE_ORDER);	    	
		    for(String s:arr)
		    	loadDataArray.add(s);    	
	    }	    
	    adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,loadDataArray);
	    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    spr_tag.setAdapter(adapter);
	    spr_tag.setSelection(0);	    
              
	}
	
	private void setListener(){
		spr_tag.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				loadProduct();		
			}
		});
		btn_search.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				loadProduct();				
			}
		});
		
		btn_new.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				createProduct();
			}
		});

	}
	private void createProduct(){
		i = new Intent(ProductActivity.this, ProductCreateActivity.class);
		i.putExtra("all_tag", loadDataArray);
		startActivityForResult(i, 0);
	}

	private void loadProduct(){
    	where = new StringBuffer();
    	where.append(" where 1=1 ");
    	if (!search_word.getText().toString().equals("")){	    		
    		where.append(" and (").append(DbAdapter.Mst_Product.PRODUCTNAME).append(" like '%").append(search_word.getText().toString().trim()).append("%'").append(" COLLATE NOCASE ");
    		where.append(" or ").append(DbAdapter.Mst_Product.PRODUCTDESC).append(" like '%").append(search_word.getText().toString().trim()).append("%'").append(" COLLATE NOCASE ");
    		where.append(" or ").append(DbAdapter.Mst_Product.PRODUCTTAG).append(" like '%").append(search_word.getText().toString().trim()).append("%'").append(" COLLATE NOCASE ");
    		where.append(")");
    	}
        if (radio_active.isChecked()){	        	
        	where.append(" and ").append(DbAdapter.Mst_Product.STATUS).append("=1");
        }else{	        	
        	where.append(" and ").append(DbAdapter.Mst_Product.STATUS).append("=0");
        }
        
        if (!spr_tag.getSelectedItem().toString().equals(Constants.ALL))
        	where.append(" and ").append(DbAdapter.Mst_Product.PRODUCTTAG).append(" like '%").append(spr_tag.getSelectedItem().toString()).append("%'");
     	
        where.append(" order by "+DbAdapter.Mst_Product.PRODUCTNAME);
        cursor = mDbHelper.fetchAllData(DbAdapter.Mst_Product.TABLE_NAME,where.toString());
        startManagingCursor(cursor);
        updateListView(cursor);
    }

    private void updateListView(Cursor cursor){
    	// the desired columns to be bound
    	String[] columns = new String[] { DbAdapter.Mst_Product.PRODUCTID, DbAdapter.Mst_Product.PRODUCTNAME, DbAdapter.Mst_Product.PRODUCTTAG};
    	// the XML defined views which the data will be bound to
    	int[] to = new int[] { R.id.productid, R.id.name, R.id.tag};
    	products = new MySimpleCursorAdapter(this,R.layout.product_list_item, cursor, columns, to);
		setListAdapter(products);
    }
    
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		cursor = (Cursor)products.getItem(position);
		i = new Intent(ProductActivity.this, ProductEditActivity.class);
		i.putExtra("product_id", cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTID)));
		i.putExtra("product_status", cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.STATUS)));
		i.putExtra("product_name", cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTNAME)));
		i.putExtra("product_desc", cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTDESC)));
		i.putExtra("product_cost", cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTCOST)));
		i.putExtra("product_price", cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTPRICE)));
		i.putExtra("product_unit", cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTUNIT)));
		i.putExtra("product_path", cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTPIC)));
		i.putExtra("product_tag", cursor.getString(cursor.getColumnIndex(DbAdapter.Mst_Product.PRODUCTTAG)));
		i.putExtra("all_tag", loadDataArray);
	    startActivityForResult(i, 0);
	} 
	 protected void onActivityResult(int requestCode, int resultCode, Intent data){
	    	super.onActivityResult(requestCode, resultCode, data);
	    	switch(resultCode) {
	    	case RESULT_OK:
	    		loadProduct();
	    	    break;
	    	}
	   
	    }	 
	 @Override
	 protected void onDestroy() {
		mDbHelper.close();
		super.onDestroy();
	 }

	
}